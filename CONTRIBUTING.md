Before your Pull Requests may be accepted we require you to sign our [contributor license agreement][cla].

We are soon switching to full blown [Google style][gsg] and will require contributors to use this.

[cla]: https://www.clahub.com/agreements/PocketServerMP/PocketServer
[gsg]: https://google.github.io/styleguide/javaguide.html

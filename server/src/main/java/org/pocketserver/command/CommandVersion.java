package org.pocketserver.command;

import org.pocketserver.api.ChatColor;
import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;

public class CommandVersion extends Command {

    private static final String VERSION = CommandVersion.class.getPackage()
        .getImplementationVersion();

    public CommandVersion() {
        super("version", "pocket.command.version", "ver", "versions", "pocketserver");
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        ctx.getExecutor().sendMessage(ChatColor.GREEN + "The server is currently running: {0}",
            VERSION);
    }
}

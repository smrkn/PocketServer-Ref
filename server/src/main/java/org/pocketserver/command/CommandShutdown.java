package org.pocketserver.command;

import org.pocketserver.api.Server;
import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;

public final class CommandShutdown extends Command {

    private final Server server;

    public CommandShutdown(Server server) {
        super("stop", "pocket.command.stop", "end", "shutdown", "quit");
        this.server = server;
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        server.shutdown();
    }
}

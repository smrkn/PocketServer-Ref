package org.pocketserver.command;

import org.pocketserver.api.ChatColor;
import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;
import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PluginManager;

import java.util.stream.Collectors;

public final class CommandPlugins extends Command {

    private final PluginManager manager;

    public CommandPlugins(PluginManager manager) {
        super("plugins", "pocket.command.plugins", "pl");
        this.manager = manager;
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        String plugins = manager.getPlugins().stream()
            .map(this::formatName)
            .collect(Collectors.joining(ChatColor.RESET + ", "));
        ctx.getExecutor().sendMessage("Plugins [{0}]", plugins);
    }

    private String formatName(Plugin plugin) {
        return (plugin.isEnabled() ? ChatColor.GREEN : ChatColor.RED) + plugin.getName();
    }
}

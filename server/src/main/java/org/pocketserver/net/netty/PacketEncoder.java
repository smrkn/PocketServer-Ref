package org.pocketserver.net.netty;

import com.google.common.base.Preconditions;
import org.pocketserver.api.Server;
import org.pocketserver.api.util.Pipeline;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.net.encapsulation.Encapsulated;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.pocketserver.net.encapsulation.Encapsulation;
import org.pocketserver.net.encapsulation.EncapsulationStrategy;
import org.pocketserver.player.Session;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class PacketEncoder extends MessageToMessageEncoder<Packet> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet msg, List<Object> out) throws Exception {
        InetSocketAddress recipient = ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE).get();
        ByteBuf buf = ctx.alloc().buffer();
        try {
            byte id = PacketRegistry.getId(msg);
            buf.writeByte(id);
            msg.write(buf);
            if (msg instanceof Encapsulated) {
                buf.markWriterIndex();
                Session session = null;
                if (ctx.hasAttr(PipelineUtils.SESSION_ATTRIBUTE)) {
                    session = ctx.attr(PipelineUtils.SESSION_ATTRIBUTE).getAndRemove();
                }
                try {
                    buf = getStrategy(msg, session).encode(ctx, buf, session);
                } catch (Exception ex) {
                    Server.getServer().getLogger().error(PocketLogging.Server.NETWORK, "Failed to encode " +
                        "packet!", ex);
                    buf.resetWriterIndex();
                }
            }
            out.add(new DatagramPacket(buf, recipient));
        } finally {
            msg.close();
        }
    }

    private EncapsulationStrategy getStrategy(Packet packet, Session session) {
        Preconditions.checkNotNull(packet);
        if (session == null) {
            return Encapsulation.BARE;
        }

        if (packet.isImmediate()) {
            return Encapsulation.COUNT;
        }

        if (session.hasSplit()) {
            return Encapsulation.COUNT_UNKNOWN;
        }

        if (session.hasMultiple()) {
            return Encapsulation.COUNT;
        }
        return Encapsulation.BARE;
    }
}

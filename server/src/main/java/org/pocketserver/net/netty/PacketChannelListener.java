package org.pocketserver.net.netty;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.pocketserver.api.Server;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import java.net.InetSocketAddress;

public class PacketChannelListener implements ChannelFutureListener {
    private final InetSocketAddress address;
    private final Packet packet;

    public PacketChannelListener(InetSocketAddress address, Packet packet) {
        this.address = address;
        this.packet = packet;
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isSuccess()) {
            Object[] params = new Object[]{null, address};

            if (Server.getServer().getLogger().isTraceEnabled()) {
                params[0] = packet.toString();
            } else {
                params[0] = String.format("0x%02X", PacketRegistry.getId(packet));
            }

            Server.getServer().getLogger().debug(PocketLogging.Server.NETWORK, "Sent {} to {}",
                    params);
        } else {
            Server.getServer().getLogger().error("Failed to send packet", future.cause());
        }
    }
}

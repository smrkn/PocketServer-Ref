package org.pocketserver.net.netty;

import org.pocketserver.api.Server;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.PipelineUtils;

import com.google.common.collect.Lists;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.Attribute;

import java.net.InetSocketAddress;
import java.util.List;

public class PocketServerHandler extends SimpleChannelInboundHandler<Packet> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, Packet packet) throws Exception {
        Attribute<InetSocketAddress> addressAttribute = ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE);
        InetSocketAddress address = addressAttribute.get();
        try {
            List<Packet> out = Lists.newLinkedList();
            packet.record(this);
            packet.handle(ctx, out);
            packet.close();
            for (Packet outbound : out) {
                ctx.write(outbound).addListener(new PacketChannelListener(address, packet));
            }
            ctx.flush();
        } finally {
            addressAttribute.remove();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Server.getServer().getLogger().error("", cause);
    }
}

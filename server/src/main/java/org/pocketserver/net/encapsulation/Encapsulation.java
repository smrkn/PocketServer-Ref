package org.pocketserver.net.encapsulation;

import org.pocketserver.exception.BadPacketException;
import org.pocketserver.net.NetworkChannel;
import org.pocketserver.net.Packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.player.Session;

import java.nio.ByteOrder;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Encapsulation {
    private static final AtomicInteger counter = new AtomicInteger();

    public static final EncapsulationStrategy BARE = new EncapsulationStrategy() {

        @Override
        public void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Packet> out, short length) throws Exception {
            assertLength(length / 8, buf);
        }

        @Override
        public ByteBuf encode(ChannelHandlerContext ctx, ByteBuf buf, Session session) throws Exception {
            ByteBuf out = ctx.alloc().buffer(buf.readableBytes() + 7);
            {
                out.writeByte(0x80);
                out.order(ByteOrder.LITTLE_ENDIAN).writeMedium(counter.getAndIncrement());
                out.writeByte(0x00);
                out.writeShort(buf.readableBytes() * 8);
                out.writeBytes(buf);
            }
            return out;
        }
    };

    public static final EncapsulationStrategy COUNT = new EncapsulationStrategy() {
        @Override
        public void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Packet> out, short length) throws Exception {
            assertLength(length / 8, buf);
            buf.skipBytes(3);
            BARE.decode(ctx, buf, out, length);
        }

        @Override
        public ByteBuf encode(ChannelHandlerContext ctx, ByteBuf buf, Session session) throws Exception {
            ByteBuf out = ctx.alloc().buffer(buf.readableBytes() + 7);
            {
                out.writeByte(0x80);
                out.order(ByteOrder.LITTLE_ENDIAN).writeMedium(counter.getAndIncrement());
                out.writeByte(0x40);
                out.writeShort(buf.readableBytes() * 8);
                out.order(ByteOrder.LITTLE_ENDIAN).writeMedium(session.getOrderIndex());
                out.writeBytes(buf);
            }
            return out;
        }
    };

    public static final EncapsulationStrategy COUNT_UNKNOWN = new EncapsulationStrategy() {
        @Override
        public void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Packet> out, short length)
                throws Exception {
            assertLength(length / 8, buf);
            buf.skipBytes(4);
            COUNT.decode(ctx, buf, out, length);
        }

        @Override
        public ByteBuf encode(ChannelHandlerContext ctx, ByteBuf buf, Session session) throws Exception {
            ByteBuf out = ctx.alloc().buffer(buf.readableBytes() + 7);
            {
                out.writeByte(0x80);
                out.order(ByteOrder.LITTLE_ENDIAN).writeMedium(counter.getAndIncrement());
                out.writeByte(0x60);
                out.writeShort(buf.readableBytes() * 8);
                out.writeBytes(buf);
            }
            return out;
        }
    };

    private static void assertLength(int length, ByteBuf buf) {
        if (length > buf.readableBytes()) {
            throw new BadPacketException("The packets length can not be longer than the readable.");
        }
    }
}

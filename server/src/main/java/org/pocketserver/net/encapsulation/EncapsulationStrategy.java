package org.pocketserver.net.encapsulation;

import org.pocketserver.net.Packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.player.Session;

import java.util.List;

public interface EncapsulationStrategy {

    void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Packet> out, short length) throws Exception;

    ByteBuf encode(ChannelHandlerContext ctx, ByteBuf buf, Session session) throws Exception;
}

package org.pocketserver.net;

import org.pocketserver.net.packet.raknet.PacketSplitPacket;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import io.netty.buffer.ByteBuf;

import java.util.Iterator;
import java.util.TreeSet;

public class SplitPacketContainer {
    private final TreeSet<PacketSplitPacket> packets;
    private final int count;

    public SplitPacketContainer(int count) {
        this.packets = Sets.newTreeSet();
        this.count = count;
    }

    public ByteBuf concat() throws Exception {
        Preconditions.checkState(isComplete(), "packet is not complete");
        ByteBuf buf = packets.first().buf().alloc().buffer(); // Unpooled is evil!
        for (Iterator<PacketSplitPacket> iterator = packets.iterator(); iterator.hasNext(); ) {
            PacketSplitPacket packet = iterator.next();
            try {
                // this is going to get confusing real fast
                packet.read(buf);
            } finally {
                packet.release();
                iterator.remove();
            }
        }
        return buf;
    }

    public boolean isComplete() {
        return packets.size() == count;
    }

    public void add(PacketSplitPacket packet) {
        if (!packets.add(packet)) {
            throw new IllegalArgumentException("a packet with this index has already been registered!");
        }
    }
}

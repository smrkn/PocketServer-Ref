package org.pocketserver.net;

public enum NetworkChannel {
    NONE(0),
    PRIORITY(1),
    CHUNK(2),
    MOVEMENT(3),
    BLOCK(4),
    WORLD(5),
    SPAWN_ENTITY(6),
    TEXT(7),
    END(31);

    private byte channel;

    NetworkChannel(byte channel) {
        this.channel = channel;
    }

    NetworkChannel(int channel) {
        this((byte) channel);
    }

    public byte getByte(){
        return channel;
    }
}
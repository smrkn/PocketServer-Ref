package org.pocketserver.net;

import org.pocketserver.net.netty.PacketDecoder;
import org.pocketserver.net.netty.PacketEncoder;
import org.pocketserver.net.netty.PocketServerHandler;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import org.pocketserver.player.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollDatagramChannel;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.AttributeKey;
import io.netty.util.internal.PlatformDependent;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Connor Spencer Harries
 */
public final class PipelineUtils {

    public static final AttributeKey<InetSocketAddress> ADDRESS_ATTRIBUTE = AttributeKey.valueOf
        ("recipient");
    public static final AttributeKey<Session> SESSION_ATTRIBUTE = AttributeKey.valueOf
            ("session");
    public static final ChannelInitializer<DatagramChannel> INITIALIZER = new
        ChannelInitializer<DatagramChannel>() {
            @Override
            protected void initChannel(DatagramChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast("encoder", new PacketEncoder());
                pipeline.addLast("decoder", new PacketDecoder());
                pipeline.addLast("handler", new PocketServerHandler());
            }
        };
    private static final boolean useEpoll;

    static {
        Logger logger = LoggerFactory.getLogger("PocketServer");
        boolean tempUseEpoll = false;
        if (!PlatformDependent.isWindows() && Boolean.getBoolean("pocket.epoll")) {
            if (Epoll.isAvailable()) {
                tempUseEpoll = true;
                logger.info("Epoll is supported by your system!");
            } else {
                logger.info("Epoll is not supported by your system!");
            }
        }
        useEpoll = tempUseEpoll;
    }

    public static Class<? extends Channel> getChannelClass() {
        if (useEpoll) {
            return EpollDatagramChannel.class;
        } else {
            return NioDatagramChannel.class;
        }
    }

    public static EventLoopGroup newEventLoop(int numThreads) {
        ThreadFactoryBuilder builder = new ThreadFactoryBuilder().setNameFormat("Netty IO Thread " +
                "#%1$d");
        ExecutorService executor = Executors.newFixedThreadPool(numThreads, builder.build());
        if (useEpoll) {
            return new EpollEventLoopGroup(0, executor);
        } else {
            return new NioEventLoopGroup(0, executor);
        }
    }

    private PipelineUtils() {
        throw new UnsupportedOperationException("PipelineUtils cannot be instantiated!");
    }
}

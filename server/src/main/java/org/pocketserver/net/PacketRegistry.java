package org.pocketserver.net;

import org.pocketserver.net.packet.block.PacketPlayUpdateBlock;
import org.pocketserver.net.packet.connect.PacketConnectOpenNewConnection;
import org.pocketserver.net.packet.connect.PacketConnectOpenRequest;
import org.pocketserver.net.packet.connect.PacketConnectOpenResponse;
import org.pocketserver.net.packet.handshake.PacketHandshakeLogin;
import org.pocketserver.net.packet.handshake.PacketHandshakePlayerStatus;
import org.pocketserver.net.packet.notify.PacketNotifyBanned;
import org.pocketserver.net.packet.notify.PacketNotifyDisconnect;
import org.pocketserver.net.packet.notify.PacketNotifyNoFreeConnections;
import org.pocketserver.net.packet.ping.PacketPingConnectedPing;
import org.pocketserver.net.packet.ping.PacketPingConnectedPong;
import org.pocketserver.net.packet.ping.PacketPingUnconnectedPing;
import org.pocketserver.net.packet.ping.PacketPingUnconnectedPong;
import org.pocketserver.net.packet.play.PacketPlayAddPainting;
import org.pocketserver.net.packet.play.PacketPlayAdventureSettings;
import org.pocketserver.net.packet.play.PacketPlayAnimate;
import org.pocketserver.net.packet.play.PacketPlayBatch;
import org.pocketserver.net.packet.block.PacketPlayChunkData;
import org.pocketserver.net.packet.play.PacketPlayContainerClose;
import org.pocketserver.net.packet.play.PacketPlayContainerOpen;
import org.pocketserver.net.packet.play.PacketPlayContainerSetData;
import org.pocketserver.net.packet.play.PacketPlayDisconnect;
import org.pocketserver.net.packet.play.PacketPlayEntityEvent;
import org.pocketserver.net.packet.play.PacketPlayHurtArmor;
import org.pocketserver.net.packet.play.PacketPlayInteract;
import org.pocketserver.net.packet.play.PacketPlayLevelEvent;
import org.pocketserver.net.packet.play.PacketPlayMobEffect;
import org.pocketserver.net.packet.play.PacketPlayMovePlayer;
import org.pocketserver.net.packet.play.PacketPlayPlayerAction;
import org.pocketserver.net.packet.play.PacketPlayRemoveBlock;
import org.pocketserver.net.packet.play.PacketPlayRemoveEntity;
import org.pocketserver.net.packet.play.PacketPlayRemovePlayer;
import org.pocketserver.net.packet.play.PacketPlayRespawn;
import org.pocketserver.net.packet.play.PacketPlaySetDifficulty;
import org.pocketserver.net.packet.play.PacketPlaySetEntityLink;
import org.pocketserver.net.packet.play.PacketPlaySetHealth;
import org.pocketserver.net.packet.play.PacketPlaySetSpawnLocation;
import org.pocketserver.net.packet.play.PacketPlaySetTime;
import org.pocketserver.net.packet.play.PacketPlaySpawnExperience;
import org.pocketserver.net.packet.play.PacketPlayStartGame;
import org.pocketserver.net.packet.play.PacketPlayTakeItemEntity;
import org.pocketserver.net.packet.play.PacketPlayText;
import org.pocketserver.net.packet.play.PacketPlayTileEvent;
import org.pocketserver.net.packet.raknet.PacketRaknetAck;
import org.pocketserver.net.packet.raknet.PacketRaknetCustom;
import org.pocketserver.net.packet.raknet.PacketRaknetIncompatibleProtocol;
import org.pocketserver.net.packet.raknet.PacketRaknetNack;
import org.pocketserver.net.packet.raknet.PacketRaknetOpenConnectionReplyA;
import org.pocketserver.net.packet.raknet.PacketRaknetOpenConnectionReplyB;
import org.pocketserver.net.packet.raknet.PacketRaknetOpenConnectionRequestA;
import org.pocketserver.net.packet.raknet.PacketRaknetOpenConnectionRequestB;
import org.pocketserver.net.packet.raknet.PacketSplitPacket;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.function.Supplier;

public final class PacketRegistry {

    private static final Map<Byte, PacketType> packetById;

    static {
        packetById = Maps.newConcurrentMap();

        for (PacketType packetType : DefaultPacketType.values()) {
            register(packetType);
        }
    }

    public static Packet construct(byte id) throws ReflectiveOperationException {
        PacketType type = packetById.getOrDefault(id, DefaultPacketType.UNKNOWN);
        if (type != DefaultPacketType.UNKNOWN) {
            return type.createPacket();
        }
        throw new IllegalArgumentException("A packet with the ID \'" + String.format("0x%02x", id) +
            "\' does not exist!");
    }

    public static byte getId(Packet packet) {
        return packet.getType().getId();
    }

    public static void register(PacketType type) {
        Preconditions.checkNotNull(type.getPacketClass(), "clazz should not be null!");
        packetById.putIfAbsent(type.getId(), type);
        if (type.hasExtraIds()) {
            for (byte eid : type.getExtraIds()) {
                packetById.putIfAbsent(eid, type);
            }
        }
    }

    private PacketRegistry() {
        throw new UnsupportedOperationException("PacketRegistry cannot be instantiated!");
    }

    public enum DefaultPacketType implements PacketType {
        UNCONNECTED_PING(0x01, PacketPingUnconnectedPing.class, PacketPingUnconnectedPing::new),
        UNCONNECTED_PONG(0x1C, PacketPingUnconnectedPong.class),
        CONNECTED_PING(0x00, PacketPingConnectedPing.class, PacketPingConnectedPing::new),
        CONNECTED_PONG(0x03, PacketPingConnectedPong.class),
        OPEN_REQUEST(0x09, PacketConnectOpenRequest.class, PacketConnectOpenRequest::new),
        OPEN_RESPONSE(0x10, PacketConnectOpenResponse.class),
        OPEN_NEW_CONNECTION(0x13, PacketConnectOpenNewConnection.class, PacketConnectOpenNewConnection::new),
        NO_FREE_CONNECTIONS(0x14, PacketNotifyNoFreeConnections.class, PacketNotifyNoFreeConnections::new),
        NOTIFY_DISCONNECT(0x15, PacketNotifyDisconnect.class, PacketNotifyDisconnect::new),
        SPLIT_PACKET(0x00, PacketSplitPacket.class),
        // TODO: create proper class, incoming packets are practically indistinguishable
        CONNECTION_BANNED(0x17, PacketNotifyBanned.class, PacketNotifyBanned::new),
        LOGIN(0x8F, PacketHandshakeLogin.class, PacketHandshakeLogin::new),
        PLAYER_STATUS(0x90, PacketHandshakePlayerStatus.class, PacketHandshakePlayerStatus::new),
        PLAY_DISCONNECT(0x91, PacketPlayDisconnect.class),
        BATCH(0x92, PacketPlayBatch.class, PacketPlayBatch::new),
        TEXT(0x93, PacketPlayText.class, PacketPlayText::new),
        SET_TIME(0x94, PacketPlaySetTime.class),
        START_GAME(0x95, PacketPlayStartGame.class),
        REMOVE_PLAYER(0x97, PacketPlayRemovePlayer.class),
        REMOVE_ENTITY(0x99, PacketPlayRemoveEntity.class),
        REMOVE_BLOCK(0x9e, PacketPlayRemoveBlock.class, PacketPlayRemoveBlock::new),
        UPDATE_BLOCK(0x9f, PacketPlayUpdateBlock.class, PacketPlayUpdateBlock::new),
        SPAWN_EXPERIENCE(0xC5, PacketPlaySpawnExperience.class),
        OPEN_CONNECTION_REQUEST_A(0x05, PacketRaknetOpenConnectionRequestA.class,
            PacketRaknetOpenConnectionRequestA::new),
        OPEN_CONNECTION_REPLY_A(0x06, PacketRaknetOpenConnectionReplyA.class),
        OPEN_CONNECTION_REQUEST_B(0x07, PacketRaknetOpenConnectionRequestB.class,
            PacketRaknetOpenConnectionRequestB::new),
        OPEN_CONNECTION_REPLY_B(0x08, PacketRaknetOpenConnectionReplyB.class),
        INCOMPATIBLE_PROTOCOL(0x1A, PacketRaknetIncompatibleProtocol.class, PacketRaknetIncompatibleProtocol::new),
        NACK(0xA0, PacketRaknetNack.class, PacketRaknetNack::new),
        ACK(0xC0, PacketRaknetAck.class, PacketRaknetAck::new),
        CUSTOM(0x80, PacketRaknetCustom.class, PacketRaknetCustom::new, 0x8F),
        ADVENTURE_SETTINGS(0xBC, PacketPlayAdventureSettings.class),
        SET_HEALTH(0xB0, PacketPlaySetHealth.class),
        SET_DIFFICULTY(0xC0, PacketPlaySetDifficulty.class),
        SET_SPAWN_LOCATION(0xB1, PacketPlaySetSpawnLocation.class),
        CONTAINER_SET_DATA(0xB8, PacketPlayContainerSetData.class, PacketPlayContainerSetData::new),
        ANIMATE(0xB2, PacketPlayAnimate.class, PacketPlayAnimate::new),
        CHUNK_DATA(0xbf, PacketPlayChunkData.class),
        HURT_ARMOR(0xAC, PacketPlayHurtArmor.class, PacketPlayHurtArmor::new),
        MOB_EFFECT(0xA5, PacketPlayMobEffect.class, PacketPlayMobEffect::new),
        TILE_EVENT(0xA3, PacketPlayTileEvent.class, PacketPlayTileEvent::new),
        ENTITY_EVENT(0xA4, PacketPlayEntityEvent.class, PacketPlayEntityEvent::new),
        ADD_PAINTING(0xA0, PacketPlayAddPainting.class, PacketPlayAddPainting::new),
        MOVE_PLAYER(0x9D, PacketPlayMovePlayer.class, PacketPlayMovePlayer::new),
        LEVEL_EVENT(0xA2, PacketPlayLevelEvent.class, PacketPlayLevelEvent::new),
        INTERACT(0xA9, PacketPlayInteract.class, PacketPlayInteract::new),
        TAKE_ITEM_ENTITY(0x9B, PacketPlayTakeItemEntity.class, PacketPlayTakeItemEntity::new),
        RESPAWN(0xB3, PacketPlayRespawn.class, PacketPlayRespawn::new),
        CONTAINER_OPEN(0xB5, PacketPlayContainerOpen.class, PacketPlayContainerOpen::new),
        CONTAINER_CLOSE(0xB6, PacketPlayContainerClose.class, PacketPlayContainerClose::new),
        PLAYER_ACTION(0xAB, PacketPlayPlayerAction.class, PacketPlayPlayerAction::new),
        SET_ENTITY_LINK(0xAF, PacketPlaySetEntityLink.class, PacketPlaySetEntityLink::new),
        UNKNOWN(-1, Packet.class); // Prevent Preconditions from going off

        private final byte id;
        private final byte[] extraIds;
        private final Class<? extends Packet> cls;
        private final Supplier<? extends Packet> constructor;

        <T extends Packet> DefaultPacketType(int id, Class<T> cls) {
            this(id, cls, null);
        }

        <T extends Packet> DefaultPacketType(int id, Class<T> cls, Supplier<T> constructor) {
            this(id, cls, constructor, -1);
        }

        <T extends Packet> DefaultPacketType(int id, Class<T> cls, Supplier<T> constructor, int range) {
            this.id = (byte) id;
            this.cls = cls;
            this.constructor = constructor;
            int extra = range - id;
            if (extra > 0) {
                this.extraIds = new byte[extra];
                for (int i = 0; i < extra; i++) {
                    this.extraIds[i] = (byte) (id + i + 1);
                }
            } else {
                this.extraIds = new byte[0];
            }
        }

        @Override
        public byte getId() {
            return this.id;
        }

        @Override
        public boolean hasExtraIds() {
            return this.extraIds.length > 0;
        }

        @Override
        public byte[] getExtraIds() {
            return this.extraIds;
        }

        @Override
        public Class<? extends Packet> getPacketClass() {
            return this.cls;
        }

        @Override
        public Packet createPacket() {
            Preconditions.checkNotNull(this.constructor,
                "Client tried to send a server packet of type, " + String
                    .format("0x%02x", id));
            return this.constructor.get();
        }

        @Override
        public boolean isClientPacket() {
            return this.constructor != null;
        }
    }
}
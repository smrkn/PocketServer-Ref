package org.pocketserver.net;

public enum Reliability {
    UNRELIABLE(0),
    UNRELIABLE_SEQUENCED(1),
    RELIABLE(2),
    RELIABLE_ORDERED(3),
    RELIABLE_SEQUENCED(4),
    UNRELIABLE_WITH_ACK_RECEIPT(5),
    RELIABLE_WITH_ACK_RECEIPT(6),
    RELIABLE_ORDERED_WITH_ACK_RECEIPT(7);

    public static Reliability getReliability(int id) {
        return values()[id];
    }

    private final int id;

    Reliability(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

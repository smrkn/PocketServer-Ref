package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketSplitPacket extends Packet implements Comparable<PacketSplitPacket> {
    private final ByteBuf buf;
    private final int packetId;
    private final int index;

    public PacketSplitPacket(int packetId, int index, ByteBuf buf) {
        super(PacketRegistry.DefaultPacketType.SPLIT_PACKET);
        this.packetId = packetId;
        this.buf = buf;
        this.index = index;
    }

    @Override
    public int compareTo(PacketSplitPacket o) {
        return index - o.index;
    }

    public void release() {
        buf.release();
    }

    public ByteBuf buf() {
        return buf;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        throw new UnsupportedOperationException("split packet must be concatenated");
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        buf.readBytes(buf);
    }
}

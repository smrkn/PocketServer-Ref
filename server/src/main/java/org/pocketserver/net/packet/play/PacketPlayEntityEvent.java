package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayEntityEvent extends Packet {
    private long entityId;
    private byte eventId;

    public PacketPlayEntityEvent() {
        super(PacketRegistry.DefaultPacketType.ENTITY_EVENT);
    }

    public PacketPlayEntityEvent(long entityId, byte eventId) {
        super(PacketRegistry.DefaultPacketType.ENTITY_EVENT);
        this.entityId = entityId;
        this.eventId = eventId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeByte(eventId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        eventId = buf.readByte();
    }
}

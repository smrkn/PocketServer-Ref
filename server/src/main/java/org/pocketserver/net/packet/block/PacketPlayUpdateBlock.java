package org.pocketserver.net.packet.block;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

public class PacketPlayUpdateBlock extends Packet {
    public PacketPlayUpdateBlock() {
        super(PacketRegistry.DefaultPacketType.UPDATE_BLOCK);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {

    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}

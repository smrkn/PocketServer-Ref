package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

public class PacketPlayTakeItemEntity extends Packet {
    private long entityId;
    private long target;

    public PacketPlayTakeItemEntity() {
        super(PacketRegistry.DefaultPacketType.TAKE_ITEM_ENTITY);
    }

    public PacketPlayTakeItemEntity(long entityId, long target) {
        this();
        this.entityId = entityId;
        this.target = target;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(target);
        buf.writeLong(entityId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        target = buf.readLong();
        entityId = buf.readLong();
    }
}

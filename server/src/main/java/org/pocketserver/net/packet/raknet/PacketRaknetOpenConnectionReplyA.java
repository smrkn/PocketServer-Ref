package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Protocol;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOpenConnectionReplyA extends Packet {

    private final short mtu;

    public PacketRaknetOpenConnectionReplyA(short mtu) {
        super(PacketRegistry.DefaultPacketType.OPEN_CONNECTION_REPLY_A);
        this.mtu = mtu;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        writeMagic(buf);
        buf.writeLong(Protocol.SERVER_ID);
        buf.writeByte(0);
        buf.writeShort(mtu);
    }
}

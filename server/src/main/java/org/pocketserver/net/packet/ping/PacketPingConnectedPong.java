package org.pocketserver.net.packet.ping;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPingConnectedPong extends Packet {

    private final long timestamp;

    public PacketPingConnectedPong(long timestamp) {
        super(PacketRegistry.DefaultPacketType.CONNECTED_PONG);
        this.timestamp = timestamp;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(timestamp);
    }
}

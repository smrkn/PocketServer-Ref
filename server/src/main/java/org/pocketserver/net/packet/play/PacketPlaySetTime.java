package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.encapsulation.Encapsulated;

public class PacketPlaySetTime extends Packet implements Encapsulated {

    private final int time;

    public PacketPlaySetTime(int time) {
        super(PacketRegistry.DefaultPacketType.SET_TIME);
        this.time = time;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(time);
    }
}

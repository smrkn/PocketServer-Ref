package org.pocketserver.net.packet.ping;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketPingUnconnectedPing extends Packet {

    private long timestamp;

    public PacketPingUnconnectedPing() {
        super(PacketRegistry.DefaultPacketType.UNCONNECTED_PING);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketPingUnconnectedPong(timestamp));
    }

    @Override
    public void read(ByteBuf buf) {
        timestamp = buf.readLong();
    }
}

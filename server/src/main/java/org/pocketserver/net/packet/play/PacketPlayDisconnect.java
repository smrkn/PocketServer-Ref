package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayDisconnect extends Packet {

    private final String reason;

    public PacketPlayDisconnect(String reason) {
        super(PacketRegistry.DefaultPacketType.PLAY_DISCONNECT);
        this.reason = reason;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        writeString(buf, reason);
    }
}

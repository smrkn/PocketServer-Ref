package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.api.player.GameMode;
import org.pocketserver.api.world.Dimension;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.encapsulation.Encapsulated;
import org.pocketserver.player.PocketPlayer;
import org.pocketserver.world.generator.GeneratorType;

public class PacketPlayStartGame extends Packet implements Encapsulated {
    private final int seed;
    private final Dimension dimension;
    private final GeneratorType generatorType;
    private final GameMode gameMode;
    private final long entityId;
    private final int spawnX;
    private final int spawnY;
    private final int spawnZ;
    private final float x;
    private final float y;
    private final float z;
    private final byte unknown;

    public PacketPlayStartGame(PocketPlayer player) {
        super(PacketRegistry.DefaultPacketType.START_GAME);
        this.seed = -1; //TODO: Configure all of this + world settings
        this.dimension = Dimension.OVERWORLD;
        this.generatorType = GeneratorType.FLAT;
        this.gameMode = GameMode.SURVIVAL;
        this.entityId = player.getEntityId();
        this.spawnX = 0;
        this.spawnY = 5;
        this.spawnZ = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.unknown = 0;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        /*
        buf.writeInt(seed);
        buf.writeByte(dimension.getId());
        buf.writeInt(generatorType.getId());
        buf.writeInt(gameMode.getId());
        buf.writeLong(entityId);
        buf.writeInt(spawnX);
        buf.writeInt(spawnY);
        buf.writeInt(spawnZ);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeByte(unknown);
        */
        buf.writeInt(seed);
        buf.writeByte(this.dimension.getId());
        buf.writeInt(generatorType.getId());
        buf.writeInt(0);
        buf.writeLong(0);
        buf.writeInt(this.spawnX);
        buf.writeInt(this.spawnY);
        buf.writeInt(this.spawnZ);
        buf.writeFloat(this.x);
        buf.writeFloat(this.y + 1.62f);
        buf.writeFloat(this.z);
        buf.writeByte((byte)0);
    }
}

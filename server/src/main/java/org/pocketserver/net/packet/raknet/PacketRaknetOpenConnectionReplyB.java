package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Protocol;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOpenConnectionReplyB extends Packet {

    private final short port;
    private final short mtu;

    public PacketRaknetOpenConnectionReplyB(short mtu, short port) {
        super(PacketRegistry.DefaultPacketType.OPEN_CONNECTION_REPLY_B);
        this.port = port;
        this.mtu = mtu;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        writeMagic(buf);
        buf.writeLong(Protocol.SERVER_ID);
        buf.writeShort(port);
        buf.writeShort(mtu);
        buf.writeByte(0);
    }
}

package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.PacketRegistry;

public class PacketRaknetNack extends PacketRaknetAck {

    public PacketRaknetNack() {
        super(PacketRegistry.DefaultPacketType.NACK, new int[1]);
    }
}

package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayAnimate extends Packet {
    private long entityId;
    private byte actionId;

    public PacketPlayAnimate() {
        super(PacketRegistry.DefaultPacketType.ANIMATE);
    }

    public PacketPlayAnimate(long entityId, byte actionId) {
        super(PacketRegistry.DefaultPacketType.ANIMATE);
        this.entityId = entityId;
        this.actionId = actionId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(actionId);
        buf.writeLong(entityId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        actionId = buf.readByte();
        entityId = buf.readLong();
    }
}

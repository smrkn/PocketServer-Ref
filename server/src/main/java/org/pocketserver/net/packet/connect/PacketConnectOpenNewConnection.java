package org.pocketserver.net.packet.connect;

import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.api.Server;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.player.Session;

import java.util.List;

public class PacketConnectOpenNewConnection extends Packet {

    public PacketConnectOpenNewConnection() {
        super(PacketRegistry.DefaultPacketType.OPEN_NEW_CONNECTION);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        Server.getServer().getLogger().info(PocketLogging.Server.NETWORK, "A client is logging in!");
        buf.skipBytes(93);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Session session = new Session(ctx.channel(), ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE).get());

    }
}

package org.pocketserver.net.packet.play;

import io.netty.buffer.Unpooled;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.net.encapsulation.Encapsulated;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class PacketPlayBatch extends Packet implements Encapsulated {
    private byte[] payload;

    public PacketPlayBatch() {
        super(PacketRegistry.DefaultPacketType.BATCH);
    }

    public PacketPlayBatch(Packet payload) {
        super(PacketRegistry.DefaultPacketType.BATCH);
        try {
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeByte(payload.getType().getId());
            payload.write(buffer); //I regret the system we use! :(
            ByteBuf outside = Unpooled.buffer(); //TODO: Un-Aids this
            outside.writeInt(buffer.readableBytes());
            outside.writeBytes(buffer);
            this.payload = outside.array();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        Deflater deflater = new Deflater();
        deflater.setInput(payload);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(payload.length);
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer); // returns the generated code... index
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        buf.writeInt(output.length);
        buf.writeBytes(output);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.payload = new byte[buf.readInt()];
        buf.readBytes(payload);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Inflater inflater = new Inflater();
        inflater.setInput(payload);
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream(payload.length)){
            byte[] buffer = new byte[1024];
            do {
                stream.write(buffer, 0, inflater.inflate(buffer));
            } while (inflater.getRemaining() > 0);

            buffer = stream.toByteArray();
            ByteBuf buf = ctx.alloc().buffer(buffer.length, buffer.length);
            try {
                buf.writeBytes(buffer);
                // int length = buf.readInt();
                for (int i = 0; i < 4; i++) {
                    System.out.println(String.format("0x%02x", buf.readByte()));
                }
                Packet packet = PacketRegistry.construct(buf.readByte());
                System.out.println(packet + " was in batch.");
                packet.read(buf);
                packet.handle(ctx, out);
            } finally {
                buf.release();
            }
        }
    }
}

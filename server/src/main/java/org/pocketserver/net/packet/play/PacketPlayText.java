package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.PocketServer;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.exception.BadPacketException;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.encapsulation.Encapsulated;
import org.pocketserver.player.PocketPlayer;

import java.util.List;
import java.util.Optional;

public class PacketPlayText extends Packet implements Encapsulated {
    private TextType type;
    private String source;
    private String message;

    public PacketPlayText(TextType type, String message, Optional<String> source) {
        this();
        this.type = type;
        this.message = message;
        if (type.requiresSource() && !source.isPresent()) {
            throw new BadPacketException("Creating a bad packet", getClass());
        }
        this.source = source.orElseGet(() -> null);
    }

    public PacketPlayText() {
        super(PacketRegistry.DefaultPacketType.TEXT);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(type.id);
        if (type.requiresSource()) {
            writeString(buf, source);
        }
        writeString(buf, message);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.type = TextType.parseText(buf.readByte());
        if (type == null) {
            throw new BadPacketException("Has the text packet changed data values??", this.getClass());
        }
        switch (type) {
            case CHAT:
            case POPUP:
                this.source = readString(buf);
            case RAW:
            case TIP:
            case SYSTEM:
                this.message = readString(buf);
                break;
            case TRANSLATION:
                this.message = readString(buf);
                int ctr = buf.readByte();
                for (int i = 0; i < ctr; i++) {
                    throw new Exception("I have actually no idea what this does");
                }
        }
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        PocketServer server = PocketServer.getServer();
        if (type == TextType.CHAT) {
            Optional<PocketPlayer> player = server.getPlayer(source).stream().findAny();
            if (!player.isPresent()) {
                return;
            }
            player.ifPresent(p -> p.chat(message));
        } else {
            server.getLogger().debug(PocketLogging.Server.NETWORK, "Received a " + type + " in text.");
        }
    }

    public enum TextType {
        RAW(0),
        CHAT(1),
        TRANSLATION(2),
        POPUP(3),
        TIP(4),
        SYSTEM(5);

        private final byte id;

        TextType(byte id) {
            this.id = id;
        }

        TextType(int i) {
            this((byte) i);
        }

        private static TextType parseText(byte id) {
            for (TextType textType : values()) {
                if (textType.id == id) {
                    return textType;
                }
            }
            return null;
        }

        public boolean requiresSource() {
            return this == CHAT || this == POPUP;
        }
    }
}

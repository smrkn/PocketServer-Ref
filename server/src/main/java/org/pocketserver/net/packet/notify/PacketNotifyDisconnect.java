package org.pocketserver.net.packet.notify;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.PocketServer;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.player.PocketPlayer;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Optional;

public class PacketNotifyDisconnect extends Packet {

    public PacketNotifyDisconnect() {
        super(PacketRegistry.DefaultPacketType.NOTIFY_DISCONNECT);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress address = ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE).get();
        PocketServer server = PocketServer.getServer();
        Optional<PocketPlayer> player = server.getPlayer(address);
        if (player.isPresent()) {
            PocketPlayer pocketPlayer = player.get();
            server.removePlayer(pocketPlayer);
            server.getLogger().info(PocketLogging.Server.NETWORK, pocketPlayer.getName()
                    .concat(" has disconnected from the server."));
        }
    }
}

package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PacketPlayPlayerAction extends Packet {
    private long entityId;
    private Action action;
    private int x;
    private int y;
    private int z;
    private int face;

    public PacketPlayPlayerAction() {
        super(PacketRegistry.DefaultPacketType.PLAYER_ACTION);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        action = Action.getAction(buf.readInt());
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        face = buf.readInt();
    }

    public enum Action {
        UNKNOWN(-1),
        START_BREAK(0),
        ABORT_BREAK(1),
        STOP_BREAK(2),
        RELEASE_ITEM(5),
        STOP_SLEEPING(6),
        RESPAWN(7),
        JUMP(8),
        START_SPRINT(9),
        STOP_SPRINT(10),
        START_SNEAK(11),
        STOP_SNEAK(12),
        DIMENSION_CHANGE(13);

        private static final Map<Integer, Action> ID_MAP = Collections.unmodifiableMap(new HashMap<Integer, Action>() {{
            Arrays.asList(PacketPlayPlayerAction.Action.values()).forEach(type -> put(type.getId(), type));
        }});

        private final int ID;

        Action(int id) {
            this.ID = id;
        }

        public int getId() {
            return ID;
        }

        public static Action getAction(int id) {
            return ID_MAP.getOrDefault(id, Action.UNKNOWN);
        }
    }
}

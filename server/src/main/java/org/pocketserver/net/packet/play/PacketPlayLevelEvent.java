package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayLevelEvent extends Packet {
    private short entityId;
    private float x;
    private float y;
    private float z;
    private int data;

    public PacketPlayLevelEvent() {
        super(PacketRegistry.DefaultPacketType.LEVEL_EVENT);
    }

    public PacketPlayLevelEvent(short entityId, float x, float y, float z, int data) {
        super(PacketRegistry.DefaultPacketType.LEVEL_EVENT);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.data = data;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeShort(entityId);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeInt(data);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readShort();
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
        data = buf.readInt();
    }
}

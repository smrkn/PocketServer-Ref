package org.pocketserver.net.packet.block;

import io.netty.buffer.ByteBuf;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.net.Batched;
import org.pocketserver.net.NetworkChannel;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import java.util.Arrays;

public class PacketPlayChunkData extends Packet implements Batched {
    private final Chunk chunk;
    private byte order = 0; //ORDER COLUMNS

    public PacketPlayChunkData(Chunk chunk) {
        super(PacketRegistry.DefaultPacketType.CHUNK_DATA);
        this.chunk = chunk;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(chunk.getX());
        buf.writeInt(chunk.getZ());
        buf.writeByte(order);
        byte[] bytes = chunk.getBytes();
        buf.writeInt(bytes.length);
        buf.writeBytes(bytes);
        System.out.println(Arrays.toString(buf.array()));
    }

    @Override
    public NetworkChannel getChannel() {
        return NetworkChannel.CHUNK;
    }
}

package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayInteract extends Packet {
    private long targetEntityId;
    private byte actionId;

    public PacketPlayInteract() {
        super(PacketRegistry.DefaultPacketType.INTERACT);
    }

    public PacketPlayInteract(long targetEntityId, byte actionId) {
        super(PacketRegistry.DefaultPacketType.INTERACT);
        this.targetEntityId = targetEntityId;
        this.actionId = actionId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(actionId);
        buf.writeLong(targetEntityId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        actionId = buf.readByte();
        targetEntityId = buf.readLong();
    }
}

package org.pocketserver.net.packet.handshake;

import org.pocketserver.PocketServer;
import org.pocketserver.api.Server;
import org.pocketserver.api.event.player.PlayerLoginEvent;
import org.pocketserver.api.util.Callback;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.net.encapsulation.Encapsulated;
import org.pocketserver.net.packet.play.PacketPlayDisconnect;
import org.pocketserver.net.packet.play.PacketPlaySetSpawnLocation;
import org.pocketserver.net.packet.play.PacketPlaySetTime;
import org.pocketserver.net.packet.play.PacketPlayStartGame;
import org.pocketserver.player.PocketPlayer;
import org.pocketserver.player.Session;
import org.pocketserver.player.PocketSkin;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.world.PocketWorld;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class PacketHandshakeLogin extends Packet implements Encapsulated {

    private String name;
    private int protocolOne;
    private int protocolTwo;
    private long clientId;
    private UUID clientUniqueId;
    private String serverAddress;
    private String skinName;
    private PocketSkin skin;

    public PacketHandshakeLogin() {
        super(PacketRegistry.DefaultPacketType.LOGIN);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.name = readString(buf);
        this.protocolOne = buf.readInt();
        this.protocolTwo = buf.readInt();
        this.clientId = buf.readLong();
        this.clientUniqueId = readUniqueId(buf); //Read UUID
        this.serverAddress = readString(buf);
        this.skinName = readString(buf);

        String skinString = readString(buf);
        this.skin = new PocketSkin();
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress address = ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE).get();
        PocketServer server = PocketServer.getServer();
        if (server.getPlayer(address).isPresent()) {
            out.add(new PacketPlayDisconnect("Somebody is already logged in from that address."));
        } else {
            PocketPlayer player = new PocketPlayer(server, new Session(ctx.channel(), address), name);
            Callback<PlayerLoginEvent> callback = (event, err) -> { //TODO: Make Session cached for all pckts
                if (event.isCancelled()) {
                    out.add(new PacketPlayDisconnect(event.getKickMessage()));
                } else {
                    Server.getServer().getLogger().info(PocketLogging.Server.NETWORK, "{} is attempting to " +
                            "login", new Object[]{
                            name
                    });

                    player.unsafe().send(new PacketHandshakePlayerStatus(0), true);
                    player.unsafe().send(new PacketPlayStartGame(player), true);
                    server.addPlayer(player);
                }
            };
            PlayerLoginEvent event = new PlayerLoginEvent(callback);
            server.getPluginManager().post(event);
        }
    }
}

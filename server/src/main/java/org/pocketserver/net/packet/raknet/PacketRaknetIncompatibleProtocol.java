package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Protocol;

import io.netty.buffer.ByteBuf;

public class PacketRaknetIncompatibleProtocol extends Packet {

    public PacketRaknetIncompatibleProtocol() {
        super(PacketRegistry.DefaultPacketType.INCOMPATIBLE_PROTOCOL);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(Protocol.RAKNET_VERSION);
        writeMagic(buf);
        buf.writeLong(Protocol.SERVER_ID);
    }
}

package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayHurtArmor extends Packet {
    private byte health;

    public PacketPlayHurtArmor() {
        super(PacketRegistry.DefaultPacketType.HURT_ARMOR);
    }

    public PacketPlayHurtArmor(byte health) {
        super(PacketRegistry.DefaultPacketType.HURT_ARMOR);
        this.health = health;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(health);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        health = buf.readByte();
    }
}

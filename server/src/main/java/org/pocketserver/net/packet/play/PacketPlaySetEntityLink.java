package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

public class PacketPlaySetEntityLink extends Packet {
    private long riderId;
    private long riddenId;
    private byte linkType;

    public PacketPlaySetEntityLink() {
        super(PacketRegistry.DefaultPacketType.SET_ENTITY_LINK);
    }

    public PacketPlaySetEntityLink(long riderId, long riddenId, byte linkType) {
        this();
        this.riderId = riderId;
        this.riddenId = riddenId;
        this.linkType = linkType;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(riderId);
        buf.writeLong(riddenId);
        buf.writeByte(linkType);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        riderId = buf.readLong();
        riddenId = buf.readLong();
        linkType = buf.readByte();
    }
}

package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayRemoveBlock extends Packet {
    private long entityId;
    private int x;
    private byte y;
    private int z;

    public PacketPlayRemoveBlock() {
        super(PacketRegistry.DefaultPacketType.REMOVE_BLOCK);
    }

    public PacketPlayRemoveBlock(long entityId, int x, byte y, int z) {
        super(PacketRegistry.DefaultPacketType.REMOVE_BLOCK);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeInt(x);
        buf.writeInt(z);
        buf.writeByte(y);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        x = buf.readInt();
        z = buf.readInt();
        y = buf.readByte();
    }
}

package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayContainerSetData extends Packet {
    private byte windowId;
    private short property;
    private short value;

    public PacketPlayContainerSetData() {
        super(PacketRegistry.DefaultPacketType.CONTAINER_SET_DATA);
    }

    public PacketPlayContainerSetData(byte windowId, short property, short value) {
        super(PacketRegistry.DefaultPacketType.CONTAINER_SET_DATA);
        this.windowId = windowId;
        this.property = property;
        this.value = value;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(windowId);
        buf.writeShort(property);
        buf.writeShort(value);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        windowId = buf.readByte();
        property = buf.readShort();
        value = buf.readShort();
    }
}

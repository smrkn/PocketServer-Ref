package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayTileEvent extends Packet {
    private int x;
    private int y;
    private int z;
    private int case1;
    private int case2;

    public PacketPlayTileEvent() {
        super(PacketRegistry.DefaultPacketType.TILE_EVENT);
    }

    public PacketPlayTileEvent(int x, int y, int z, int case1, int case2) {
        super(PacketRegistry.DefaultPacketType.TILE_EVENT);
        this.x = x;
        this.y = y;
        this.z = z;
        this.case1 = case1;
        this.case2 = case2;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(case1);
        buf.writeInt(case2);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        case1 = buf.readInt();
        case2 = buf.readInt();
    }
}

package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Protocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketRaknetOpenConnectionRequestA extends Packet {

    private byte raknetVersion;
    private int mtu;

    public PacketRaknetOpenConnectionRequestA() {
        super(PacketRegistry.DefaultPacketType.OPEN_CONNECTION_REQUEST_A);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        if (raknetVersion == Protocol.RAKNET_VERSION) {
            out.add(new PacketRaknetOpenConnectionReplyA((short) mtu));
        } else {
            out.add(new PacketRaknetIncompatibleProtocol());
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        long magicOne = buf.readLong();
        long magicTwo = buf.readLong();

        if (magicOne == Protocol.MAGIC_1 && magicTwo == Protocol.MAGIC_2) {
            raknetVersion = buf.readByte();
            mtu = buf.readableBytes();
        }
    }
}

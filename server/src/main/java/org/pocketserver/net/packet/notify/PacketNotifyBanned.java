package org.pocketserver.net.packet.notify;

import org.pocketserver.api.Server;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketNotifyBanned extends Packet {

    public PacketNotifyBanned() {
        super(PacketRegistry.DefaultPacketType.CONNECTION_BANNED);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {

    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        Server.getServer().getLogger().trace(PocketLogging.Server.NETWORK, "Connection has been " +
            "banned");
    }
}

package org.pocketserver.net.packet.connect;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.PipelineUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketConnectOpenRequest extends Packet {

    private long timestamp;
    private long clientId;
    private byte sec;

    public PacketConnectOpenRequest() {
        super(PacketRegistry.DefaultPacketType.OPEN_REQUEST);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketConnectOpenResponse(timestamp, ctx.attr(PipelineUtils.ADDRESS_ATTRIBUTE)
            .get()));
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        clientId = buf.readLong();
        timestamp = buf.readLong();
        sec = buf.readByte();
    }
}

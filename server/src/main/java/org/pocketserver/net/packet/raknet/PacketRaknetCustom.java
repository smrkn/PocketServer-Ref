package org.pocketserver.net.packet.raknet;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Reliability;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.net.packet.handshake.PacketHandshakeLogin;
import org.pocketserver.net.packet.play.PacketPlayBatch;

import java.util.LinkedList;
import java.util.List;

public class PacketRaknetCustom extends Packet {

    private final List<Packet> packets;
    private int datagramSequenceNumber;

    public PacketRaknetCustom() {
        super(PacketRegistry.DefaultPacketType.CUSTOM);
        this.packets = new LinkedList<>();
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketRaknetAck(datagramSequenceNumber));
        for (Packet packet : packets) {
            packet.handle(ctx, out);
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        datagramSequenceNumber = buf.readMedium();

        int reliableMessageNumber;
        byte orderingChannel;
        int sequenceIndex;
        int orderingIndex;

        int splitPackets;
        int splitPacketId;
        int splitPacketIndex;

        while (buf.isReadable()) {
            byte header = buf.readByte();
            short bitLength = buf.readShort();
            int length = (int) Math.ceil(bitLength / 8); //TODO: Handle most of this in encapsulation/session

            Reliability reliability = Reliability.getReliability((header & 0xE0) >> 5);
            boolean split = (header & 0x10) > 0;

            if (reliability == Reliability.RELIABLE
                    || reliability == Reliability.RELIABLE_ORDERED
                    || reliability == Reliability.RELIABLE_SEQUENCED) {
                reliableMessageNumber = buf.readMedium();
            } else {
                reliableMessageNumber = -1;
            }

            if (reliability == Reliability.UNRELIABLE_SEQUENCED || reliability == Reliability.RELIABLE_SEQUENCED) {
                sequenceIndex = buf.readMedium();
            }

            if (reliability == Reliability.UNRELIABLE_SEQUENCED
                    || reliability == Reliability.RELIABLE_SEQUENCED
                    || reliability == Reliability.RELIABLE_ORDERED
                    || reliability == Reliability.RELIABLE_ORDERED_WITH_ACK_RECEIPT) {
                orderingIndex = buf.readMedium();
                orderingChannel = buf.readByte(); // flags
            } else {
                orderingChannel = 0;
                orderingIndex = 0;
            }

            ByteBuf buffer = buf.alloc().buffer(length, length);
            byte id = buf.readByte();
            if (id == (byte)0x8e) {
                System.out.println("Debug?");
                id = buf.readByte();
            }
            buffer.writerIndex(length);
            buf.readBytes(buffer, 0, length-1);

            if (split) {
                PacketSplitPacket packet = new PacketSplitPacket(buffer.getByte(0), orderingIndex, buffer);
                packets.add(packet);
            }

            try {
                Packet packet = PacketRegistry.construct(id);
                System.out.printf("Id: 0x%02x, Packet: %s\n", id, packet);
                packet.read(buffer);
                packets.add(packet);
                if (packet instanceof PacketHandshakeLogin) {
                    return;
                }
            } catch (IllegalArgumentException ex) {
                buf.skipBytes(buf.readableBytes());
            } finally {
                buffer.release();
            }
        }
    }
}
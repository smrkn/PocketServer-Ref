package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.encapsulation.Encapsulated;

public class PacketPlaySetDifficulty extends Packet implements Encapsulated {
    private final int difficulty;

    public PacketPlaySetDifficulty(int difficulty) {
        super(PacketRegistry.DefaultPacketType.ADVENTURE_SETTINGS);
        this.difficulty = difficulty;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(difficulty);
    }
}

package org.pocketserver.net.packet.ping;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketPingConnectedPing extends Packet {

    private long timestamp;

    public PacketPingConnectedPing() {
        super(PacketRegistry.DefaultPacketType.CONNECTED_PING);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketPingConnectedPong(timestamp));
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        timestamp = buf.readLong();
    }
}

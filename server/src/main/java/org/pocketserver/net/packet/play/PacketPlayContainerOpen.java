package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

public class PacketPlayContainerOpen extends Packet {
    private byte windowId;
    private byte type;
    private short slots;
    private int x;
    private int y;
    private int z;

    public PacketPlayContainerOpen() {
        super(PacketRegistry.DefaultPacketType.CONTAINER_OPEN);
    }

    public PacketPlayContainerOpen(byte windowId, byte type, short slots, int x, int y, int z) {
        this();
        this.windowId = windowId;
        this.type = type;
        this.slots = slots;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(windowId);
        buf.writeByte(type);
        buf.writeShort(slots);
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        windowId = buf.readByte();
        type = buf.readByte();
        slots = buf.readShort();
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
    }
}

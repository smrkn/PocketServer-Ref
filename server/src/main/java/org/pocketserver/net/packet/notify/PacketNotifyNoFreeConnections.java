package org.pocketserver.net.packet.notify;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketNotifyNoFreeConnections extends Packet {

    public PacketNotifyNoFreeConnections() {
        super(PacketRegistry.DefaultPacketType.NO_FREE_CONNECTIONS);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        // NOP
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        // NOP
    }
}

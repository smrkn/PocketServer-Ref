package org.pocketserver.net.packet.play;

import org.pocketserver.api.world.locale.Location;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.encapsulation.Encapsulated;

public class PacketPlaySetSpawnLocation extends Packet implements Encapsulated {

    private final Location location;

    public PacketPlaySetSpawnLocation(Location location) {
        super(PacketRegistry.DefaultPacketType.SET_SPAWN_LOCATION);
        this.location = location;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(location.getBlockX());
        buf.writeInt(location.getBlockY());
        buf.writeInt(location.getBlockZ());
    }
}

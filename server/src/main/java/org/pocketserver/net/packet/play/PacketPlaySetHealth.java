package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlaySetHealth extends Packet {
    private final int health;

    protected PacketPlaySetHealth(int health) {
        super(PacketRegistry.DefaultPacketType.SET_HEALTH);
        this.health = health;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(health);
    }
}

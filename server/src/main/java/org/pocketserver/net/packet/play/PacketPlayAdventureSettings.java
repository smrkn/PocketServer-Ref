package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayAdventureSettings extends Packet {
    private final int flags;

    protected PacketPlayAdventureSettings(int flags) {
        super(PacketRegistry.DefaultPacketType.ADVENTURE_SETTINGS);
        this.flags = flags;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(flags);
    }
}

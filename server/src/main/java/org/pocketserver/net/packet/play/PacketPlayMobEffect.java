package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayMobEffect extends Packet {
    private long entityId;
    private byte eventId;
    private byte effectId;
    private byte amplifier;
    private byte particles;
    private int duration;

    public PacketPlayMobEffect() {
        super(PacketRegistry.DefaultPacketType.MOB_EFFECT);
    }

    public PacketPlayMobEffect(long entityId, byte eventId, byte effectId, byte amplifier, byte particles, int
        duration) {
        super(PacketRegistry.DefaultPacketType.MOB_EFFECT);
        this.entityId = entityId;
        this.eventId = eventId;
        this.effectId = effectId;
        this.amplifier = amplifier;
        this.particles = particles;
        this.duration = duration;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeByte(eventId);
        buf.writeByte(effectId);
        buf.writeByte(amplifier);
        buf.writeByte(particles);
        buf.writeInt(duration);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        eventId = buf.readByte();
        effectId = buf.readByte();
        amplifier = buf.readByte();
        particles = buf.readByte();
        duration = buf.readInt();
    }
}

package org.pocketserver.net.packet.handshake;

import org.pocketserver.api.Server;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.encapsulation.Encapsulated;

public class PacketHandshakePlayerStatus extends Packet implements Encapsulated {

    private final int status;

    public PacketHandshakePlayerStatus() {
        this(0);
    }

    public PacketHandshakePlayerStatus(int status) {
        super(PacketRegistry.DefaultPacketType.PLAYER_STATUS);
        this.status = status;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(status);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        int status = buf.readInt();
        switch (status) {
            case 0:
                Server.getServer().getLogger().debug("Everything is working!");
                break;
            case 1:
                Server.getServer().getLogger().debug("Client is disconnecting - server outdated");
                break;
            case 2:
                Server.getServer().getLogger().debug("Player has spawned!");
                break;
        }
    }
}

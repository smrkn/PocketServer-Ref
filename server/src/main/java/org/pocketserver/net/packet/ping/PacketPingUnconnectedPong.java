package org.pocketserver.net.packet.ping;

import org.pocketserver.api.Server;
import org.pocketserver.api.event.server.ServerPingEvent;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.Protocol;

import io.netty.buffer.ByteBuf;

public class PacketPingUnconnectedPong extends Packet {
    private final StringBuilder builder;
    private final long timestamp;

    public PacketPingUnconnectedPong(long timestamp) {
        super(PacketRegistry.DefaultPacketType.UNCONNECTED_PONG);
        this.builder = new StringBuilder();
        this.timestamp = timestamp;
    }

    @Override
    public void write(ByteBuf buf) {
        ServerPingEvent event = new ServerPingEvent("Yo MOTD", "0.13.0",
                Protocol.PROTOCOL_VERSION, Server.getServer().getOnlinePlayers().size(), 100); //TODO Config
        Server.getServer().getPluginManager().post(event);

        buf.writeLong(timestamp);
        buf.writeLong(Protocol.SERVER_ID);
        writeMagic(buf);

        StringBuilder builder = new StringBuilder("MCPE;");
        {
            builder.append(event.getMotd()).append(";");
            builder.append(event.getProtocolVersion()).append(";");
            builder.append(event.getVersionNumber()).append(";");
            builder.append(event.getPlayerCount()).append(";");
            builder.append(event.getMaxPlayers()).append(";");
        }
        writeString(buf, builder.toString());
    }
}

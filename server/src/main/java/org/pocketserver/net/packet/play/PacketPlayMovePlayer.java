package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PacketPlayMovePlayer extends Packet {
    private long entityId;
    private float x;
    private float y;
    private float z;
    private float yaw;
    private float bodyYaw;
    private float pitch;
    private Mode mode;
    private byte onGround;

    public PacketPlayMovePlayer() {
        super(PacketRegistry.DefaultPacketType.MOVE_PLAYER);
    }

    public PacketPlayMovePlayer(long entityId, float x, float y, float z, float yaw, float bodyYaw, float pitch, Mode
        mode, byte onGround) {
        super(PacketRegistry.DefaultPacketType.MOVE_PLAYER);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.bodyYaw = bodyYaw;
        this.pitch = pitch;
        this.mode = mode;
        this.onGround = onGround;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeFloat(yaw);
        buf.writeFloat(bodyYaw);
        buf.writeFloat(pitch);
        buf.writeByte(mode.getId());
        buf.writeByte(onGround);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
        yaw = buf.readFloat();
        bodyYaw = buf.readFloat();
        pitch = buf.readFloat();
        mode = Mode.getById(buf.readByte());
        onGround = buf.readByte();
    }

    public enum Mode {
        NORMAL(0),
        RESET(1),
        ROTATION(2);

        private static final Map<Integer, Mode> ID_MAP = Collections.unmodifiableMap(new HashMap<Integer, Mode>() {{
            Arrays.asList(PacketPlayMovePlayer.Mode.values()).forEach(mode -> put(mode.getId(), mode));
        }});

        public static Mode getById(int id) {
            return ID_MAP.get(id);
        }

        private final int ID;

        Mode(int id) {
            this.ID = id;
        }

        public int getId() {
            return ID;
        }
    }
}

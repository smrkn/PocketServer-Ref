package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

public class PacketPlayRemovePlayer extends Packet {

    private final long entityId;
    private final UUID uniqueId;

    public PacketPlayRemovePlayer(long entityId, UUID uniqueId) {
        super(PacketRegistry.DefaultPacketType.REMOVE_PLAYER);
        this.entityId = entityId;
        this.uniqueId = uniqueId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeLong(uniqueId.getMostSignificantBits());
        buf.writeLong(uniqueId.getLeastSignificantBits());
    }
}

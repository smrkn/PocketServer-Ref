package org.pocketserver.net.packet.block;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import java.util.List;

public class PacketPlayRemoveBlock extends Packet {
    protected PacketPlayRemoveBlock() {
        super(PacketRegistry.DefaultPacketType.REMOVE_BLOCK);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {

    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        //TODO: Implement this
    }
}

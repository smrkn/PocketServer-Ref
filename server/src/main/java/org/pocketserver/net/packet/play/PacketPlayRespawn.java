package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;
import org.pocketserver.net.encapsulation.Encapsulated;

public class PacketPlayRespawn extends Packet implements Encapsulated {
    private float x;
    private float y;
    private float z;

    public PacketPlayRespawn() {
        super(PacketRegistry.DefaultPacketType.RESPAWN);
    }

    public PacketPlayRespawn(float x, float y, float z) {
        this();
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
    }
}

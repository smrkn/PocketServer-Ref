package org.pocketserver.net.packet.play;

import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

import io.netty.buffer.ByteBuf;

public class PacketPlayAddPainting extends Packet {
    private long entityId;
    private int x;
    private int y;
    private int z;
    private int direction;
    private String title;

    public PacketPlayAddPainting() {
        super(PacketRegistry.DefaultPacketType.ADD_PAINTING);
    }

    public PacketPlayAddPainting(long entityId, int x, int y, int z, int direction, String title) {
        super(PacketRegistry.DefaultPacketType.ADD_PAINTING);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.direction = direction;
        this.title = title;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(direction);
        writeString(buf, title);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        direction = buf.readInt();
        title = readString(buf);
    }
}

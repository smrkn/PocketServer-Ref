package org.pocketserver.net.packet.play;

import io.netty.buffer.ByteBuf;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PacketRegistry;

public class PacketPlayContainerClose extends Packet {
    private byte windowId;

    public PacketPlayContainerClose() {
        super(PacketRegistry.DefaultPacketType.CONTAINER_CLOSE);
    }

    public PacketPlayContainerClose(byte windowId) {
        this();
        this.windowId = windowId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(windowId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        windowId = buf.readByte();
    }
}

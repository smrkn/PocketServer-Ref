package org.pocketserver.player;

public final class PocketSkin {

    private boolean slim;
    private byte[] data;
    private byte alpha;

    public PocketSkin() {
        data = new byte[0];
    }

    public boolean isSlim() {
        return slim;
    }

    public void setSlim(boolean slim) {
        this.slim = slim;
    }

    public byte getAlpha() {
        return alpha;
    }

    public void setAlpha(byte alpha) {
        this.alpha = alpha;
    }

    public byte[] getData() {
        return data.clone();
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}

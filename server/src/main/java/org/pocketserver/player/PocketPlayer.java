package org.pocketserver.player;

import org.pocketserver.PocketServer;
import org.pocketserver.api.Server;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.EntityEquipment;
import org.pocketserver.api.event.player.PlayerChatEvent;
import org.pocketserver.api.event.player.PlayerGameModeChangeEvent;
import org.pocketserver.api.inventory.Inventory;
import org.pocketserver.api.inventory.InventoryType;
import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.permissions.PermissionResolver.Result;
import org.pocketserver.api.player.GameMode;
import org.pocketserver.api.player.Player;
import org.pocketserver.entity.living.PocketLivingEntity;
import org.pocketserver.inventory.PocketInventory;
import org.pocketserver.net.Packet;
import org.pocketserver.net.packet.handshake.PacketHandshakePlayerStatus;
import org.pocketserver.net.packet.play.PacketPlayText;

import com.google.common.base.Objects;
import org.pocketserver.world.PocketWorld;

import java.net.InetSocketAddress;
import java.util.Optional;

public class PocketPlayer extends PocketLivingEntity implements Player {

    private final PocketServer server;
    private final Session session;
    private final Unsafe unsafe;
    private final String name;
    private final Inventory inventory;

    private GameMode gamemode;
    private boolean op;
    private EntityEquipment equipment;
    private int fireTicks;
    private boolean flying;

    public PocketPlayer(PocketServer server, Session session, String
        name) {
        super(EntityType.PLAYER);
        this.gamemode = GameMode.SURVIVAL;
        this.server = server;
        this.session = session;
        this.name = name;
        //TODO: Some way to load the data
        this.equipment = new EntityEquipment(null, null);
        this.fireTicks = 0;
        this.inventory = new PocketInventory(InventoryType.PLAYER);

        // Don't convert to lambda as Unsafe is definitely going to change.
        // new Unsafe() => send(Packet packet) => channel.writeAndFlush(packet)
        // noinspection Convert2Lambda,Anonymous2MethodRef
        this.unsafe = new Unsafe() {
            @Override
            public void send(Packet packet) {
                session.sendPacket(packet);
            }

            @Override
            public void send(Packet packet, boolean immediate) {
                session.sendPacket(packet, immediate);
            }
        };
    }

    @Override
    public void chat(String message) {
        if (message == null || message.isEmpty()) {
            return;
        }

        PlayerChatEvent event = new PlayerChatEvent(this, message);
        server.getPluginManager().post(event);
        if (event.isCancelled()) {
            return;
        }
        Packet packet = new PacketPlayText(PacketPlayText.TextType.CHAT, message, Optional.of(getName()));
        server.broadcast(packet, p -> event.getRecipients().contains(p));
    }

    @Override
    public GameMode getGamemode() {
        return this.gamemode;
    }

    @Override
    public void setGamemode(GameMode gamemode) {
        PlayerGameModeChangeEvent event = new PlayerGameModeChangeEvent(this, gamemode);
        server.getPluginManager().post(event);

        if (event.isCancelled()) {
            return;
        }

        this.gamemode = event.getGameMode();
    }

    @Override
    public InetSocketAddress getAddress() {
        return this.session.getAddress();
    }

    @Override
    public void sendTip(String message) {
        if (message == null) {
            return;
        }
        unsafe().send(new PacketPlayText(PacketPlayText.TextType.TIP, message, Optional.empty()));
    }

    @Override
    public void sendPopup(String message) {
        if (message == null) {
            return;
        }
        unsafe().send(new PacketPlayText(PacketPlayText.TextType.POPUP, message, Optional.of(getName())));
    }

    public Unsafe unsafe() {
        return unsafe;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void sendMessage(String message) {
        if (message == null) {
            return;
        }
        unsafe().send(new PacketPlayText(PacketPlayText.TextType.SYSTEM, message, Optional.empty()));
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public boolean hasPermission(String permission) {
        if (permission.isEmpty()) {
            return true;
        }

        for (PermissionResolver resolver : server.getPermissionPipeline()) {
            Result result = resolver.checkPermission(this, permission);
            if (result.isBlocking()) {
                return result == Result.ALLOW;
            }
        }
        return false;
    }

    @Override
    public boolean isOp() {
        return op;
    }

    @Override
    public void setOp(boolean op) {
        this.op = op;
    }

    @Override
    public EntityEquipment getEquipment() {
        return equipment;
    }

    @Override
    public void setEquipment(EntityEquipment equipment) {
        this.equipment = equipment;
    }

    @Override
    public int getFireTicks() {
        return fireTicks;
    }

    @Override
    public void setFireTicks(int fireTicks) {
        this.fireTicks = fireTicks;
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj instanceof PocketPlayer) {
            PocketPlayer that = (PocketPlayer) obj;
            return name.equals(that.name) && that.getAddress().equals(getAddress());
        }
        return false;
    }

    @Override
    public boolean isFlying() {
        return flying;
    }

    @Override
    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public void spawn(PocketWorld world) {
        world.sendChunks(this);
    }

    public interface Unsafe {
        void send(Packet packet);

        void send(Packet packet, boolean immediate);
    }
}

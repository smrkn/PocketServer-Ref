package org.pocketserver.player;

import com.google.common.collect.Lists;
import io.netty.channel.Channel;
import org.pocketserver.net.Batched;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.net.SplitPacketContainer;
import org.pocketserver.net.netty.PacketChannelListener;
import org.pocketserver.net.packet.play.PacketPlayBatch;
import org.pocketserver.net.packet.raknet.PacketSplitPacket;

import com.google.common.base.Objects;
import com.google.common.collect.Maps;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

// TODO: Implement sane instantiation
public final class Session {
    private final Map<Integer, SplitPacketContainer> splitMap;
    private final LinkedList<Packet> queue;
    private final Channel channel;
    private final InetSocketAddress address;
    private final AtomicInteger order;

    public Session(Channel channel, InetSocketAddress address) {
        this.splitMap = Maps.newHashMap();
        this.queue = Lists.newLinkedList();
        this.channel = channel;
        this.address = address;
        this.order = new AtomicInteger(0);
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                queue.forEach(p -> sendPacket(p, true));
            }
        }, 100, 100);
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void sendPacket(Packet packet, boolean immediate) {
        if (immediate) {
            if (packet instanceof Batched) {
                packet = new PacketPlayBatch(packet);
            }
            channel.attr(PipelineUtils.ADDRESS_ATTRIBUTE).set(address);
            channel.attr(PipelineUtils.SESSION_ATTRIBUTE).set(this);
            packet.setImmediate(true);
            this.channel.writeAndFlush(packet).addListener(new PacketChannelListener(address, packet));
            return;
        }
        queue.add(packet);
    }

    public void sendPacket(Packet packet) {
        this.sendPacket(packet, false);
    }

    public boolean hasSplit() {
        return splitMap.size() > 0;
    }

    public boolean hasMultiple() {
        return queue.size() > 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(address);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Session)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        Session that = (Session) o;
        return Objects.equal(address, that.address);
    }

    public int getOrderIndex() {
        return order.getAndIncrement();
    }
}

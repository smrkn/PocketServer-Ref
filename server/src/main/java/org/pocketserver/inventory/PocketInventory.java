package org.pocketserver.inventory;

import org.pocketserver.api.inventory.Inventory;
import org.pocketserver.api.inventory.InventoryType;
import org.pocketserver.api.inventory.ItemStack;

public class PocketInventory implements Inventory {

    private final InventoryType type;
    private final ItemStack[] contents;

    public PocketInventory(InventoryType type) {
        this(type, new ItemStack[type.getSize()]);
    }

    public PocketInventory(InventoryType type, ItemStack[] contents) {
        this.type = type;
        if (contents.length > type.getSize()) {
            this.contents = new ItemStack[type.getSize()];
            System.arraycopy(contents, 0, this.contents, 0, type.getSize());
        } else {
            this.contents = contents;
        }
    }

    @Override
    public InventoryType getType() {
        return type;
    }

    @Override
    public int getSize() {
        return contents.length;
    }

    @Override
    public ItemStack[] getContents() {
        return contents;
    }

    @Override
    public void setContents(ItemStack... contents) {
        if (contents.length > type.getSize()) {
            System.arraycopy(contents, 0, this.contents, 0, type.getSize());
        } else {
            System.arraycopy(contents, 0, this.contents, 0, contents.length);
        }
    }

    @Override
    public ItemStack getItem(int position) {
        return contents[position];
    }

    @Override
    public void setItem(int position, ItemStack item) {
        contents[position] = item;
    }
}

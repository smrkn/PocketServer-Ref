package org.pocketserver.inventory.meta;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.inventory.meta.Enchantment;
import org.pocketserver.api.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PocketItemMeta implements ItemMeta {

    private final Material parent;
    private String name;
    private List<String> lore;
    private Map<Enchantment, Integer> enchantments;

    public PocketItemMeta(Material parent) {
        this(parent, new HashMap<>());
    }

    public PocketItemMeta(Material parent, Map<Enchantment, Integer> enchantments) {
        this.parent = parent;
        this.enchantments = enchantments;
    }

    @Override
    public Optional<String> getDisplayName() {
        return Optional.ofNullable(name);
    }

    @Override
    public void setDisplayName(String name) {
        this.name = name;
    }

    @Override
    public Optional<List<String>> getLore() {
        return Optional.ofNullable(lore);
    }

    @Override
    public void setLore(List<String> lore) {
        this.lore = lore;
    }

    @Override
    public void addEnchantment(Enchantment enchantment, int level) {
        if (!enchantment.getTargets().contains(parent)) {
            return;
        }
        if (level > enchantment.getMaxLevel()) {
            level = enchantment.getMaxLevel();
        }
        addUnsafeEnchantment(enchantment, level);
    }

    @Override
    public void setEnchantmentLevel(Enchantment enchantment, int level) {
        if (!enchantments.containsKey(enchantment)) {
            return;
        }
        if (level > enchantment.getMaxLevel()) {
            level = enchantment.getMaxLevel();
        }
        setUnsafeEnchantmentLevel(enchantment, level);
    }

    @Override
    public int getEnchantmentLevel(Enchantment enchantment) {
        return enchantments.get(enchantment);
    }

    @Override
    public void removeEnchantment(Enchantment enchantment) {
        enchantments.remove(enchantment);
    }

    @Override
    public boolean hasEnchantment(Enchantment enchantment) {
        return enchantments.containsKey(enchantment);
    }

    @Override
    public void addUnsafeEnchantment(Enchantment enchantment, int level) {
        enchantments.put(enchantment, level);
    }

    @Override
    public void setUnsafeEnchantmentLevel(Enchantment enchantment, int level) {
        if (!enchantments.containsKey(enchantment)) {
            return;
        }
        enchantments.put(enchantment, level);
    }
}

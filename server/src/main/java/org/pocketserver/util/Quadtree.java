package org.pocketserver.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Quadtree<E> {

    private static final int MAX_ITEMS = 10;

    @SuppressWarnings("unchecked")
    private final Quadtree<E>[] nodes = new Quadtree[4];
    private final List<E> items = new ArrayList<>();
    private final int x;
    private final int y;
    private final int width;
    private final int height;

    public Quadtree(int x, int y, int width, int height) {
        this(x, y, width, height, null);
    }

    public Quadtree(int x, int y, int width, int height, Collection<? extends E> items) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        if (items != null) {
            this.items.addAll(items);
        }
    }

    public void insertItem(E e, int x, int y) {
        if (items.size() >= MAX_ITEMS) {
            if (nodes[0] == null) {
                split();
            }
            nodes[getIndex(x, y)].insertItem(e, x, y);
        }
        items.add(e);
    }

    public void split() {
        int width = this.width / 2;
        int height = this.height / 2;
        nodes[0] = new Quadtree<>(x, y, width, height);
        nodes[1] = new Quadtree<>(x + width, y, width, height);
        nodes[2] = new Quadtree<>(x, y + height, width, height);
        nodes[3] = new Quadtree<>(x + width, y + height, width, height);
    }

    public List<E> retrieve(E e) {
        if (nodes[0] == null) {
            if (this.items.contains(e)) {
                return this.items;
            }
        }
        for (Quadtree<E> node : nodes) {
            List<E> retrieve = node.retrieve(e);
            if (retrieve != null) {
                return retrieve;
            }
        }
        return null;
    }

    public List<E> retrieve(int x, int y) {
        return ImmutableList.copyOf(nodes[0] == null ? items : nodes[getIndex(x, y)].items);
    }

    private int getIndex(int x, int y) {
        Preconditions.checkNotNull(nodes[0]);
        for (int i = 0; i < nodes.length; i++) {
            Quadtree<E> quadtree = nodes[i];
            if (quadtree.containsPoint(x, y)) {
                return i;
            }
        }
        return -1;
    }

    public boolean containsPoint(int x, int y) {
        return this.x < x && this.x + width > x && this.y < y && this.y + height > y;
    }

    public List<E> retrieveValues() {
        List<E> list = new ArrayList<>(this.items);
        if (nodes[0] != null) {
            for (Quadtree<E> node : nodes) {
                list.addAll(node.retrieveValues());
            }
        }
        return list;
    }
}

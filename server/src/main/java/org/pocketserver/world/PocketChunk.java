package org.pocketserver.world;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.pocketserver.api.block.Block;
import org.pocketserver.api.block.Material;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.api.world.World;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.block.PocketBlock;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

public class PocketChunk implements Chunk {

    private final int x;
    private final int z;
    private final PocketWorld world;
    private final PocketBlock[] blocks = new PocketBlock[128 * 16 * 16];
    private final AtomicBoolean updated = new AtomicBoolean(true);
    private byte[] cache;
    private boolean dirty;

    public PocketChunk(Location loc) {
        this((PocketWorld) loc.getWorld(), loc.getBlockX() >> 4, loc.getBlockZ() >> 4);
    }

    public PocketChunk(PocketWorld world, int x, int z) {
        this.world = world;
        this.x = x;
        this.z = z;

        for (int dx = 0; dx < 16; dx++) {
            for (int dy = 0; dy < 128; dy++) {
                for (int dz = 0; dz < 16; dz++) {
                    Location loc = new Location(world, x * 16 + dx, dy, z * 16 + dz);
                    PocketBlock b = new PocketBlock(Material.AIR, loc);
                    if (dy < 2) {
                        b.setType(Material.BEDROCK);
                    }
                    blocks[getBlockIndex(dx, dy, dz)] = b;
                }
            }
        }
    }

    private int getBlockIndex(int x, int y, int z) {
        return ((x << 11) | (z << 7) | y);
    }

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getZ() {
        return z;
    }

    @Override
    public Block getBlock(Location loc) {
        int dx = loc.getBlockX(), dz = loc.getBlockZ();
        if (dx >> 4 != x || dz >> 4 != z) {
            throw new IllegalArgumentException("Location is not inside this Chunk.");
        }
        return getBlock(dx & 0xF, loc.getBlockY(), dz & 0xF);
    }

    @Override
    public Block getBlock(int x, int y, int z) {
        if (0 <= x && x < 16 && 0 <= y && y < 256 && 0 <= z && z < 16) {
            return blocks[getBlockIndex(x, y, z)];
        }
        return null;
    }

    @Override
    public boolean isInChunk(Location location) {
        int dx = location.getBlockX(), dz = location.getBlockZ();
        return dx >> 4 == x && dz >> 4 == z;
    }

    @Override
    public boolean isInChunk(int x, int y, int z) {
        return isInChunk(new Location(world, x, y, z));
    }

    @Override
    public void queueUpdate(int x, int y, int z, Material material) {
        this.updated.set(true);

        blocks[getBlockIndex(x, y, z)].setType(material);
    }

    @Override
    public void proposeUpdates() {
        //TODO: Send all the new blocks to client.
    }

    @Override
    public byte[] getBytes() throws Exception {
        if (cache == null || dirty) {
            ByteArrayDataOutput output = ByteStreams.newDataOutput();
            output.write(toByteArray(blocks, pocketBlock -> pocketBlock.getType().getByte())); //Block IDs
            output.write(toByteArray(new Object[16*16*64], block -> (byte) 0)); //Data
            output.write(toByteArray(new Object[16*16*64], block -> (byte) -1)); //TODO
            output.write(toByteArray(new Object[16*16*64], block -> (byte) 0));
            for (int i = 0; i < 16 * 16; i++) {
                //Height map TODO
                output.writeByte(2 & 0xff);
            }


            int[] biomeColors = new int[256];
            Arrays.fill(biomeColors, readInt(new byte[]{(byte) 0xff, (byte) 0x00, (byte) 0x00, (byte) 0x00}));
            for (int color : biomeColors) {
                System.out.println(color);
                output.writeInt(color);
            }

            output.write(writeLInt(0));
            output.write(new byte[0]);
            cache = output.toByteArray();
            dirty = false;
        }
        return cache;
    }

    //Copied from nukkit to test
    public static int readInt(byte[] bytes) {
        return ((bytes[0] & 0xff) << 24) +
                ((bytes[1] & 0xff) << 16) +
                ((bytes[2] & 0xff) << 8) +
                (bytes[3] & 0xff);
    }

    //^
    private byte[] writeLInt(int i) {
        return new byte[]{
                (byte) (i & 0xFF),
                (byte) ((i >>> 8) & 0xFF),
                (byte) ((i >>> 16) & 0xFF),
                (byte) ((i >>> 24) & 0xFF)
        };
    }

    private <T> byte[] toByteArray(T[] array, Function<? super T, ? extends Byte> mapper) {
        byte[] bytes = new byte[array.length];
        for (int i = 0; i < array.length; i++) {
            bytes[i] = mapper.apply(array[i]);
        }
        return bytes;
    }

    public void setBlock() {

    }
}

package org.pocketserver.world.storage;

import com.google.common.collect.ImmutableList;
import org.pocketserver.api.world.provider.WorldProvider;
import org.pocketserver.world.storage.anvil.AnvilWorldProvider;
import org.pocketserver.world.storage.testing.TestWorldProvider;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class Providers {
    private static Providers ourInstance = new Providers();

    public static Providers getInstance() {
        return ourInstance;
    }

    private final List<WorldProvider> providers = new CopyOnWriteArrayList<>(new WorldProvider[]{
            new TestWorldProvider(), //TODO: Remove this
            new AnvilWorldProvider(),
    });

    private Providers() {
    }

    public void addProvider(WorldProvider provider) {
        this.providers.add(provider);
    }

    public List<WorldProvider> getProviders() {
        return ImmutableList.copyOf(providers);
    }

    public WorldProvider getDefaultProvider() {
        return providers.get(0);
    }
}

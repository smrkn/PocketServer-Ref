package org.pocketserver.world.storage.anvil;

import com.google.common.base.Preconditions;
import org.pocketserver.api.world.World;
import org.pocketserver.world.PocketChunk;
import org.pocketserver.world.PocketWorld;
import org.pocketserver.world.storage.AsyncWorldProvider;
import org.pocketserver.world.storage.WorldSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class AnvilWorldProvider extends AsyncWorldProvider {

    @Override
    protected World loadWorld(String name) {
        File directory = new File(name);
        if (!directory.isDirectory()) {
            return null;
        }

        File levelData = new File(directory, "level.dat");
        WorldSettings settings = loadLevelData(levelData);
        return new PocketWorld(this, name, settings);
    }

    @Override
    protected void saveWorld(World world) {

    }

    @Override
    protected void loadChunk(World world, int x, int z) {
        Preconditions.checkArgument(world instanceof PocketWorld); //TODO: Support custom worlds maybe..? Doubtful.
        PocketWorld pocketWorld = (PocketWorld) world;
        PocketChunk chunk = new PocketChunk(pocketWorld, x, z);
        File regionFile = new File(world.getName() + File.separator + "regions/r." + x + "." + z + ".mca");
        loadRegions(chunk, regionFile);
        pocketWorld.addLoadedChunk(chunk);
    }

    private WorldSettings loadLevelData(File file) {
        /*
        try {
            FileInputStream stream = new FileInputStream(file);
            NbtReader reader = new NbtReader(stream, true);
            NbtCompoundTag compound = reader.readCompound();
            WorldSettings settings = new WorldSettings();
            for (NbtTag tag : compound.getTags()) {
                settings.getClass().getField(tag.getName()).set(settings, tag.getValue()); //YOLO
            }
        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        */
        return new WorldSettings();
    }

    private void loadRegions(PocketChunk chunk, File file) {
        /*try {

            NbtReader reader = new NbtReader(new FileInputStream(file));
            NbtCompoundTag root = reader.readCompound();
            NbtCompoundTag level = root.getTag("Level");
            NbtListTag sections = level.getTag("Sections");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        */
    }
}

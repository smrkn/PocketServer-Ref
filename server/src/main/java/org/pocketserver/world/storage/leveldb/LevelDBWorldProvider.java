package org.pocketserver.world.storage.leveldb;

import org.pocketserver.api.world.World;
import org.pocketserver.world.storage.AsyncWorldProvider;

public class LevelDBWorldProvider extends AsyncWorldProvider {

    @Override
    protected World loadWorld(String name) {
        return null;
    }

    @Override
    protected void saveWorld(World world) {

    }

    @Override
    protected void loadChunk(World world, int x, int z) {

    }
}
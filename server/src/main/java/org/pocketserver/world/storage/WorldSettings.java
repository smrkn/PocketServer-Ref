package org.pocketserver.world.storage;

import org.pocketserver.api.player.GameMode;
import org.pocketserver.api.world.settings.Difficulty;
import org.pocketserver.api.world.settings.GameRule;
import org.pocketserver.world.generator.GeneratorType;

import java.util.Set;
import java.util.HashSet;

@SuppressWarnings("unused")
public class WorldSettings {
    private final Set<GameRule> gameRules = new HashSet<>();
    private int level;
    private boolean enabled;
    private String levelName;
    private GeneratorType generator;
    private String generatorOptions;
    private long seed;
    private boolean mapFeatures;
    private long lastLoaded;
    private boolean allowCommands;
    private boolean hardcore;
    private GameMode defaultGametype;
    private Difficulty difficulty;
    private boolean difficultyLocked;
    private long time;
    private long dayTime;
    private int spawnX;
    private int spawnY;
    private int spawnZ;
    private boolean raining;
    private long rainTime;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public GeneratorType getGenerator() {
        return generator;
    }

    public void setGenerator(GeneratorType generator) {
        this.generator = generator;
    }

    public String getGeneratorOptions() {
        return generatorOptions;
    }

    public void setGeneratorOptions(String generatorOptions) {
        this.generatorOptions = generatorOptions;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public boolean isMapFeatures() {
        return mapFeatures;
    }

    public void setMapFeatures(boolean mapFeatures) {
        this.mapFeatures = mapFeatures;
    }

    public long getLastLoaded() {
        return lastLoaded;
    }

    public void setLastLoaded(long lastLoaded) {
        this.lastLoaded = lastLoaded;
    }

    public boolean isAllowCommands() {
        return allowCommands;
    }

    public void setAllowCommands(boolean allowCommands) {
        this.allowCommands = allowCommands;
    }

    public boolean isHardcore() {
        return hardcore;
    }

    public void setHardcore(boolean hardcore) {
        this.hardcore = hardcore;
    }

    public GameMode getDefaultGametype() {
        return defaultGametype;
    }

    public void setDefaultGametype(GameMode defaultGametype) {
        this.defaultGametype = defaultGametype;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isDifficultyLocked() {
        return difficultyLocked;
    }

    public void setDifficultyLocked(boolean difficultyLocked) {
        this.difficultyLocked = difficultyLocked;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getDayTime() {
        return dayTime;
    }

    public void setDayTime(long dayTime) {
        this.dayTime = dayTime;
    }

    public int getSpawnX() {
        return spawnX;
    }

    public void setSpawnX(int spawnX) {
        this.spawnX = spawnX;
    }

    public int getSpawnY() {
        return spawnY;
    }

    public void setSpawnY(int spawnY) {
        this.spawnY = spawnY;
    }

    public int getSpawnZ() {
        return spawnZ;
    }

    public void setSpawnZ(int spawnZ) {
        this.spawnZ = spawnZ;
    }

    public boolean isRaining() {
        return raining;
    }

    public void setRaining(boolean raining) {
        this.raining = raining;
    }

    public long getRainTime() {
        return rainTime;
    }

    public void setRainTime(long rainTime) {
        this.rainTime = rainTime;
    }

    public Set<GameRule> getGameRules() {
        return gameRules;
    }
}

package org.pocketserver.world.storage;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import org.pocketserver.api.world.World;
import org.pocketserver.api.world.provider.WorldProvider;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public abstract class AsyncWorldProvider implements WorldProvider {
    protected final ListeningExecutorService service = MoreExecutors.listeningDecorator(
            Executors.newSingleThreadExecutor()
    );

    @Override
    public Future<World> loadWorld(String name, boolean async) {
        return async ? service.submit(() -> loadWorld(name)) : Futures.immediateFuture(loadWorld(name));
    }

    protected abstract World loadWorld(String name);

    @Override
    public void saveWorld(World world, boolean async) {
        if (async) {
            service.submit(() -> saveWorld(world));
            return;
        }
        saveWorld(world);
    }

    protected abstract void saveWorld(World world);

    @Override
    public void loadChunk(World world, int x, int z, boolean async) {
        if (async) {
            service.submit(() -> loadChunk(world, x, z)).addListener(world::updateWorld, service);
            return;
        }
        loadChunk(world, x, z);
    }

    protected abstract void loadChunk(World world, int x, int z);
}

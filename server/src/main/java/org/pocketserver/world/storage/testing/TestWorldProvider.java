package org.pocketserver.world.storage.testing;

import com.google.common.base.Preconditions;
import org.pocketserver.api.block.Material;
import org.pocketserver.api.world.World;
import org.pocketserver.world.PocketChunk;
import org.pocketserver.world.PocketWorld;
import org.pocketserver.world.storage.AsyncWorldProvider;
import org.pocketserver.world.storage.WorldSettings;

public class TestWorldProvider extends AsyncWorldProvider {
    @Override
    protected World loadWorld(String name) {
        return new PocketWorld(this, name, new WorldSettings());
    }

    @Override
    protected void saveWorld(World world) {
        //NOP
    }

    @Override
    protected void loadChunk(World world, int x, int z) {
        Preconditions.checkArgument(world instanceof PocketWorld);
        PocketChunk chunk = new PocketChunk((PocketWorld) world, x, z);
        for (int bX = 0; bX < 16; bX++) {
            for (int bZ = 0; bZ < 16; bZ++) {
                chunk.getBlock(bX, 1, bZ).setType(Material.STONE);
            }
        }
    }
}

package org.pocketserver.world.biome;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class Biomes {
    private static final Set<Biome> biomes = new CopyOnWriteArraySet<>();

    public static void registerBiome(Biome biome) {
        biomes.add(biome);
    }

    public static Set<Biome> getBiomes() {
        return biomes;
    }
}

package org.pocketserver.world.biome;

import org.pocketserver.world.generator.ChunkPopulator;

import java.util.function.Supplier;

public interface Biome {
    String getName();

    Supplier<ChunkPopulator> getPopulator();
}

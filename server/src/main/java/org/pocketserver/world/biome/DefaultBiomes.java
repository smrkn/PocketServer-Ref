package org.pocketserver.world.biome;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.world.generator.ChunkPopulator;

import java.util.function.Supplier;

public enum DefaultBiomes implements Biome {
    PLAINS,
    DESERT,
    EXTREME_HILLS,
    FOREST,
    TAIGA,
    SWAMP,
    RIVER,
    ICE_PLAINS,
    MUSHROOM_ISLAND,
    BEACH,
    JUNGLE,
    BIRCH_FOREST,
    ROOFED_FOREST,
    COLD_TAIGA,
    SAVANNA,
    MESA,
    ICE_SPIKES,
    MEGA_SPRUCE_TAIGA;

    DefaultBiomes() {
        Biomes.registerBiome(this);
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public Supplier<ChunkPopulator> getPopulator() {
        return () -> new ChunkPopulator(this) {
            @Override
            public void generateChunk(Chunk chunk) {
                for (int x = 0; x < 16; x++) {
                    for (int z = 0; z < 16; z++) {
                        for (int y = 0; y < 20; y++) {
                            chunk.queueUpdate(x, y, z, Material.AIR);
                        }
                    }
                }
            }
        };
    }
}

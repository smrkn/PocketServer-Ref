package org.pocketserver.world;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.pocketserver.api.block.Block;
import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.api.world.World;
import org.pocketserver.api.world.locale.Bounds;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.provider.WorldProvider;
import org.pocketserver.net.packet.block.PacketPlayChunkData;
import org.pocketserver.net.packet.handshake.PacketHandshakePlayerStatus;
import org.pocketserver.net.packet.play.PacketPlayRespawn;
import org.pocketserver.net.packet.play.PacketPlaySetDifficulty;
import org.pocketserver.net.packet.play.PacketPlaySetSpawnLocation;
import org.pocketserver.net.packet.play.PacketPlaySetTime;
import org.pocketserver.player.PocketPlayer;
import org.pocketserver.util.Quadtree;
import org.pocketserver.world.storage.WorldSettings;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

public class PocketWorld implements World {

    private final WorldProvider provider;
    private final String name;
    private final Table<Integer, Integer, PocketChunk> chunks = HashBasedTable.create();
    private final Quadtree<Entity> entities;
    private final Bounds bounds;
    private final WorldSettings settings;
    private long time;

    public PocketWorld(WorldProvider provider, String name, WorldSettings settings) {
        this.provider = provider;
        this.name = name;
        this.settings = settings;
        this.bounds = new Bounds(1000, 1000);
        this.entities = new Quadtree<>(0, 0, bounds.getWidth() * 2, bounds.getHeight() * 2);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void updateWorld() {

    }

    @Override
    public PocketChunk getChunk(int x, int z) {
        return getChunk(x, z, false);
    }

    @Override
    public PocketChunk getChunk(int cx, int cz, boolean load) {
        if (!load || chunks.contains(cx, cz)) {
            return chunks.get(cx, cz);
        }
        PocketChunk c = new PocketChunk(this, cx, cz);
        chunks.put(cx, cz, c);
        return c;
    }

    @Override
    public Block getBlockAt(int x, int y, int z) {
        int cx = x >> 4;
        int cz = z >> 4;
        Chunk c = getChunk(cx, cz);
        return c != null ? c.getBlock(x % 16, y, z % 16) : null;
    }

    @Override
    public Block getBlockAt(Location location) {
        return getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    @Override
    public void spawnPlayer(Player player) {
        PocketPlayer pocketPlayer = (PocketPlayer) player;
        pocketPlayer.setLocation(getSpawn());
        sendChunks(pocketPlayer);
    }

    @Override
    public Entity spawnEntity(EntityType entityType, Location location) {
        Entity entity = entityType.bound().spawn(location);

  /*
   * TODO: Send "Add entity" packets and begin tracking
   * PocketServer server = (PocketServer) Server.getServer();
   * server.broadcast(null);
   */

        return entity;
    }

    @Override
    public Bounds getDimensions() {
        return bounds;
    }

    @Override
    public List<Entity> getEntities() {
        return entities.retrieveValues();
    }

    @Override
    public List<Entity> getNearbyEntities(Location location) {
        return getNearbyEntities(location, 10);
    }

    @Override
    public List<Entity> getNearbyEntities(Location location, int distance) {
        return entities.retrieve(location.getBlockX(), location.getBlockY()).stream()
            .filter(e -> e.getLocation().distanceSquared(location) < Math.pow(distance, 2))
            .collect(Collectors.toList());
    }

    public void addLoadedChunk(PocketChunk chunk) {
        this.chunks.put(chunk.getX(), chunk.getZ(), chunk);
    }

    public WorldSettings getSettings() {
        return settings;
    }

    public void sendChunks(PocketPlayer player) {
        Chunk chunk = player.getLocation().getChunk();
        /*
        for (int x = chunk.getX(); x < chunk.getX() + 5; x++) {
            for (int z = chunk.getZ(); z < chunk.getZ() + 5; z++) {
                PacketPlayChunkData packet = new PacketPlayChunkData(getChunk(x, z, true));
                player.unsafe().send(packet, true);
            }
        }
        */
        //player.unsafe().send(new PacketPlayChunkData(chunk), true);  //Comment this and it won't freeze
        //new Timer().schedule(new TimerTask() {
        //    @Override
        //    public void run() {
        //        player.unsafe().send(new PacketPlayChunkData(chunk), true);
        //    }
        //}, 100);
        player.unsafe().send(new PacketHandshakePlayerStatus(3), true);
        //new Timer().schedule(new TimerTask() {
        //    @Override
        //    public void run() {
        //    }
        //}, 10000);
    }

    public Location getSpawn() {
        return new Location(this, 0, 0, 0);
    }

    @Override
    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public long getTime() {
        return time;
    }
}

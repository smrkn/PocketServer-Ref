package org.pocketserver.world.generator;

public enum GeneratorType {
    DEFAULT,
    FLAT,
    LARGE_BIOMES,
    AMPLIFIED,
    CUSTOMIZED,
    DEBUG_ALL_BLOCK_STATES;

    public static GeneratorType fromString(String value) {
        value = value.toUpperCase().replaceAll(" ", "_");
        return valueOf(value);
    }

    public int getId() {
        return 2; //TODO: Figure out the ids for generator
    }
}

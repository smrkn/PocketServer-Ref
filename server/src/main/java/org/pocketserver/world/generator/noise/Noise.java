package org.pocketserver.world.generator.noise;

import java.util.Random;

public class Noise {
    private final ThreadLocal<Random> random;

    public Noise(Random random, long seed) {
        this.random = new ThreadLocal<>();
        random.setSeed(seed);
        this.random.set(random);
    }

    //TODO
}

package org.pocketserver.world.generator.populators;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.world.biome.Biome;
import org.pocketserver.world.biome.DefaultBiomes;
import org.pocketserver.world.generator.ChunkPopulator;
import org.pocketserver.world.generator.structures.BedrockStructure;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class DesertPopulator extends ChunkPopulator {

    public DesertPopulator() {
        super(DefaultBiomes.DESERT, new BedrockStructure());
    }

    @Override
    public void generateChunk(Chunk chunk) {
        Random random = ThreadLocalRandom.current();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = 0; y < 50; y++) {
                    this.checkPopulators(chunk, x, y, z, random);
                    chunk.queueUpdate(x, y, z, Material.SAND);
                }
            }
        }
        chunk.proposeUpdates();
    }
}

package org.pocketserver.entity.vehicle;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.vehicle.Boat;

import java.lang.ref.WeakReference;

public class PocketBoat extends PocketVehicle implements Boat {

    private volatile WeakReference<Entity> primary;
    private volatile WeakReference<Entity> secondary;

    public PocketBoat() {
        super(EntityType.BOAT);
    }

    @Override
    public Entity getSecondaryPassenger() {
        return secondary != null ? secondary.get() : null;
    }

    @Override
    public void setSecondaryPassenger(Entity entity) {
        if (getPassenger() == null) {
            setPassenger(entity);
        } else {
            secondary = entity != null ? new WeakReference<>(entity) : null;
        }
    }

    @Override
    public Entity getPassenger() {
        return primary != null ? primary.get() : null;
    }

    @Override
    public void setPassenger(Entity entity) {
        primary = entity != null ? new WeakReference<>(entity) : null;
    }

}

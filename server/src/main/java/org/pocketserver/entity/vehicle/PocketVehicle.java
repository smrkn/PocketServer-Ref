package org.pocketserver.entity.vehicle;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.vehicle.Vehicle;
import org.pocketserver.entity.PocketEntity;

public abstract class PocketVehicle extends PocketEntity implements Vehicle {

    public PocketVehicle(EntityType type) {
        super(type);
    }

}

package org.pocketserver.entity.vehicle;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.vehicle.Minecart;

import java.lang.ref.WeakReference;

public class PocketMinecart extends PocketVehicle implements Minecart {

    private volatile WeakReference<Entity> primary;

    public PocketMinecart() {
        super(EntityType.MINECART);
    }

    @Override
    public Entity getPassenger() {
        return primary != null ? primary.get() : null;
    }

    @Override
    public void setPassenger(Entity entity) {
        primary = entity != null ? new WeakReference<>(entity) : null;
    }

}

package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.neutral.Enderman;
import org.pocketserver.api.inventory.ItemStack;
import org.pocketserver.entity.living.PocketNeutral;

public class PocketEnderman extends PocketNeutral implements Enderman {

    private volatile ItemStack held;

    public PocketEnderman() {
        super(EntityType.ENDERMAN);
    }

    @Override
    public ItemStack getHeldBlock() {
        return held;
    }

    @Override
    public void setHeldBlock(ItemStack held) {
        this.held = held;
    }

}

package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Tameable;

import java.util.UUID;

public class PocketTameable extends GrowingEntity implements Tameable {

    private volatile boolean tamed, sitting;
    private volatile UUID tamer;

    public PocketTameable(EntityType type) {
        super(type);
    }

    @Override
    public boolean isTamed() {
        return tamed;
    }

    @Override
    public void setTamed(boolean tamed) {
        this.tamed = tamed;
    }

    @Override
    public UUID getTamer() {
        return tamer;
    }

    @Override
    public void setTamer(UUID tamer) {
        this.tamer = tamer;
    }

    @Override
    public boolean isSitting() {
        return sitting;
    }

    @Override
    public void setSitting(boolean sitting) {
        this.sitting = sitting;
    }

}

package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.EntityEquipment;
import org.pocketserver.api.entity.living.hostile.Zombie;
import org.pocketserver.api.entity.living.hostile.ZombieType;
import org.pocketserver.entity.living.PocketHostile;

public class PocketZombie extends PocketHostile implements Zombie {

    private volatile EntityEquipment equipment;
    private volatile ZombieType type;

    public PocketZombie() {
        super(EntityType.ZOMBIE);
    }

    @Override
    public EntityEquipment getEquipment() {
        return equipment;
    }

    @Override
    public void setEquipment(EntityEquipment equipment) {
        this.equipment = equipment;
    }

    @Override
    public ZombieType getZombieType() {
        return type;
    }

    @Override
    public void setZombieType(ZombieType type) {
        this.type = type;
    }

    @Override
    public EntityType getType() {
        return this.type.toEntityType();
    }

}

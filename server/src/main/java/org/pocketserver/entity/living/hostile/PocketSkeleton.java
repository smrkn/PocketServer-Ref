package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.Projectile;
import org.pocketserver.api.entity.living.EntityEquipment;
import org.pocketserver.api.entity.living.hostile.Skeleton;
import org.pocketserver.api.entity.living.hostile.SkeletonType;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;
import org.pocketserver.entity.living.PocketHostile;

public class PocketSkeleton extends PocketHostile implements Skeleton {

    private volatile EntityEquipment equipment;
    private volatile SkeletonType type;

    public PocketSkeleton() {
        super(EntityType.SKELETON);
    }

    @Override
    public EntityEquipment getEquipment() {
        return equipment;
    }

    @Override
    public void setEquipment(EntityEquipment equipment) {
        this.equipment = equipment;
    }

    @Override
    public SkeletonType getSkeletonType() {
        return type;
    }

    @Override
    public void setSkeletonType(SkeletonType type) {
        this.type = type;
    }

    @Override
    public <T extends Projectile> T shoot(Class<T> clazz, Location location, Vector velocity) {
        // TODO
        return null;
    }

}

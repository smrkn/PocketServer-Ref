package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.navigation.Navigation;
import org.pocketserver.entity.PocketEntity;

public class PocketLivingEntity extends PocketEntity implements LivingEntity {

    public static final double DEFAULT_NAVIGATION_SPEED = 5.0;
    protected Navigation navigation;
    private double health, maxHealth, originalMaxHealth;

    public PocketLivingEntity(EntityType type) {
        super(type);
        this.health = this.maxHealth = this.originalMaxHealth = 20.00D;
        this.navigation = new Navigation(this, DEFAULT_NAVIGATION_SPEED);
    }

    @Override
    public void destroy() {
        this.health = 0;

        // TODO: Send death packets & remove from memory

        super.destroy();
    }

    @Override
    public double getHealth() {
        return health;
    }

    @Override
    public void setHealth(double health) {
        this.health = Math.max(0, Math.min(health, maxHealth));
    }

    @Override
    public double getMaxHealth() {
        return maxHealth;
    }

    @Override
    public void setMaxHealth(double health) {
        this.maxHealth = health;
    }

    @Override
    public void resetMaxHealth() {
        this.maxHealth = this.originalMaxHealth;
    }

    @Override
    public Navigation getNavigation() {
        return navigation;
    }

}

package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.neutral.CaveSpider;
import org.pocketserver.entity.living.hostile.PocketSpider;

public class PocketCaveSpider extends PocketSpider implements CaveSpider {

    private volatile boolean angered;

    public PocketCaveSpider() {
        super(EntityType.CAVE_SPIDER);
    }

    @Override
    public boolean isAngered() {
        return angered;
    }

    @Override
    public void setAngered(boolean angered) {
        this.angered = angered;
    }

}

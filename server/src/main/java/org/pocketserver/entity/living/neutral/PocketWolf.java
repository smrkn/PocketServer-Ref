package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.DyeColor;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.living.neutral.Wolf;
import org.pocketserver.entity.living.PocketTameable;

import java.lang.ref.WeakReference;

public class PocketWolf extends PocketTameable implements Wolf {

    private volatile WeakReference<LivingEntity> target;
    private volatile boolean angered;
    private volatile DyeColor collar;

    public PocketWolf(EntityType type) {
        super(EntityType.WOLF);
    }

    @Override
    public LivingEntity getHostileTarget() {
        return target != null ? target.get() : null;
    }

    @Override
    public void setHostileTarget(LivingEntity entity) {
        this.target = entity != null ? new WeakReference<>(entity) : null;
    }

    @Override
    public boolean isAngered() {
        return angered;
    }

    @Override
    public void setAngered(boolean angered) {
        this.angered = angered;
    }

    @Override
    public DyeColor getCollarColor() {
        return collar;
    }

    @Override
    public void setCollarColor(DyeColor collar) {
        this.collar = collar;
    }

}

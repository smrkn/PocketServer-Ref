package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Bat;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketBat extends PocketAnimal implements Bat {

    private volatile boolean flying;

    public PocketBat() {
        super(EntityType.BAT);
    }

    @Override
    public boolean isFlying() {
        return flying;
    }

    @Override
    public void setFlying(boolean flying) {
        this.flying = flying;
    }

}

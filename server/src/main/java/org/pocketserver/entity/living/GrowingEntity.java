package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Growing;

public abstract class GrowingEntity extends PocketLivingEntity implements Growing {

    private volatile int age;
    private volatile boolean adult;

    public GrowingEntity(EntityType type) {
        super(type);
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean isAdult() {
        return adult;
    }

    @Override
    public void setAdult(boolean adult) {
        this.adult = adult;
    }

}

package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.neutral.SnowGolem;
import org.pocketserver.entity.living.PocketNeutral;

public class PocketSnowGolem extends PocketNeutral implements SnowGolem {

    public PocketSnowGolem() {
        super(EntityType.SNOW_GOLEM);
    }

}

package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Chicken;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketChicken extends PocketAnimal implements Chicken {

    public PocketChicken() {
        super(EntityType.CHICKEN);
    }

}

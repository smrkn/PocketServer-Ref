package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Neutral;

public abstract class PocketNeutral extends PocketAngerable implements Neutral {

    public PocketNeutral(EntityType type) {
        super(type);
    }

}

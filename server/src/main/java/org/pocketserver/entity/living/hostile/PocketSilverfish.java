package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Silverfish;
import org.pocketserver.entity.living.PocketHostile;

public class PocketSilverfish extends PocketHostile implements Silverfish {

    public PocketSilverfish() {
        super(EntityType.SILVERFISH);
    }

}

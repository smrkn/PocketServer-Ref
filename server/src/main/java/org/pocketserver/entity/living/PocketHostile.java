package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

import java.lang.ref.WeakReference;

public abstract class PocketHostile extends PocketLivingEntity implements Hostile {

    private volatile WeakReference<LivingEntity> target;

    public PocketHostile(EntityType type) {
        super(type);
    }

    @Override
    public LivingEntity getHostileTarget() {
        return target != null ? target.get() : null;
    }

    @Override
    public void setHostileTarget(LivingEntity entity) {
        this.target = entity != null ? new WeakReference<>(entity) : null;
    }

}

package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.neutral.IronGolem;
import org.pocketserver.entity.living.PocketNeutral;

public class PocketIronGolem extends PocketNeutral implements IronGolem {

    public PocketIronGolem() {
        super(EntityType.IRON_GOLEM);
    }

}

package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Cow;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketCow extends PocketAnimal implements Cow {

    public PocketCow() {
        super(EntityType.COW);
    }

    protected PocketCow(EntityType type) {
        super(type);
    }

}

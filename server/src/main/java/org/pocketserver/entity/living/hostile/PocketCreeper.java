package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Creeper;
import org.pocketserver.entity.living.PocketHostile;

public class PocketCreeper extends PocketHostile implements Creeper {

    private volatile boolean charged;

    public PocketCreeper() {
        super(EntityType.CREEPER);
    }

    @Override
    public boolean isCharged() {
        return charged;
    }

    @Override
    public void setCharged(boolean charged) {
        this.charged = charged;
    }

}

package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Ocelot;
import org.pocketserver.api.entity.living.passive.OcelotBreed;
import org.pocketserver.entity.living.PocketTameable;

public class PocketOcelot extends PocketTameable implements Ocelot {

    private volatile OcelotBreed breed;

    public PocketOcelot() {
        super(EntityType.OCELOT);
    }

    @Override
    public OcelotBreed getBreed() {
        return breed;
    }

    @Override
    public void setBreed(OcelotBreed breed) {
        this.breed = breed;
    }

}

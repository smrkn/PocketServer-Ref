package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Villager;
import org.pocketserver.entity.living.GrowingEntity;

public class PocketVillager extends GrowingEntity implements Villager {

    public PocketVillager() {
        super(EntityType.VILLAGER);
    }

}

package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.MagmaCube;

public class PocketMagmaCube extends PocketSlime implements MagmaCube {

    public PocketMagmaCube() {
        super(EntityType.MAGMA_CUBE);
    }

}

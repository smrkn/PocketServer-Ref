package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Mooshroom;

public class PocketMooshroom extends PocketCow implements Mooshroom {

    public PocketMooshroom() {
        super(EntityType.MOOSHROOM);
    }

}

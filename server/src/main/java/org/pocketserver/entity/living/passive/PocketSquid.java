package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Squid;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketSquid extends PocketAnimal implements Squid {

    public PocketSquid() {
        super(EntityType.SQUID);
    }

}

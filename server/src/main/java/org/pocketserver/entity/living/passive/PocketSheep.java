package org.pocketserver.entity.living.passive;

import org.pocketserver.api.DyeColor;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Sheep;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketSheep extends PocketAnimal implements Sheep {

    private volatile DyeColor color;
    private volatile boolean shaved;

    public PocketSheep() {
        super(EntityType.SHEEP);
    }

    @Override
    public DyeColor getColor() {
        return color;
    }

    @Override
    public void setColor(DyeColor color) {
        this.color = color;
    }

    @Override
    public boolean isShaved() {
        return shaved;
    }

    @Override
    public void setShaved(boolean shaved) {
        this.shaved = shaved;
    }

}

package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Angerable;

public abstract class PocketAngerable extends PocketHostile implements Angerable {

    private volatile boolean angered;

    public PocketAngerable(EntityType type) {
        super(type);
    }

    @Override
    public boolean isAngered() {
        return angered;
    }

    @Override
    public void setAngered(boolean angered) {
        this.angered = angered;
    }

}

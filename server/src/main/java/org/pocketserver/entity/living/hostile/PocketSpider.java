package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Spider;
import org.pocketserver.entity.living.PocketHostile;

public class PocketSpider extends PocketHostile implements Spider {

    public PocketSpider() {
        super(EntityType.SPIDER);
    }

    protected PocketSpider(EntityType type) {
        super(type);
    }

}

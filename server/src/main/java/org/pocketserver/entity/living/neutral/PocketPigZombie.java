package org.pocketserver.entity.living.neutral;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.EntityEquipment;
import org.pocketserver.api.entity.living.neutral.PigZombie;
import org.pocketserver.entity.living.PocketNeutral;

public class PocketPigZombie extends PocketNeutral implements PigZombie {

    private volatile EntityEquipment equipment;

    public PocketPigZombie() {
        super(EntityType.ZOMBIE_PIGMAN);
    }

    @Override
    public EntityEquipment getEquipment() {
        return equipment;
    }

    @Override
    public void setEquipment(EntityEquipment equipment) {
        this.equipment = equipment;
    }

}

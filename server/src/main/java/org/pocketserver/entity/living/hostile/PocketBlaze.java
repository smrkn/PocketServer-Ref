package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Blaze;
import org.pocketserver.entity.living.PocketHostile;

public class PocketBlaze extends PocketHostile implements Blaze {

    private volatile boolean flying;

    public PocketBlaze() {
        super(EntityType.BLAZE);
    }

    @Override
    public boolean isFlying() {
        return flying;
    }

    @Override
    public void setFlying(boolean flying) {
        this.flying = flying;
    }

}

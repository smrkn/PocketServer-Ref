package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Pig;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketPig extends PocketAnimal implements Pig {

    public PocketPig() {
        super(EntityType.PIG);
    }

}

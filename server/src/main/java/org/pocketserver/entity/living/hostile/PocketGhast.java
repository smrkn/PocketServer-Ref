package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.Projectile;
import org.pocketserver.api.entity.living.hostile.Ghast;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;
import org.pocketserver.entity.living.PocketHostile;

public class PocketGhast extends PocketHostile implements Ghast {

    private volatile boolean flying;

    public PocketGhast() {
        super(EntityType.GHAST);
    }

    @Override
    public boolean isFlying() {
        return flying;
    }

    @Override
    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    @Override
    public <T extends Projectile> T shoot(Class<T> clazz, Location location, Vector velocity) {
        try {
            T instance = clazz.newInstance();
            instance.setVelocity(velocity);
            return instance;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}

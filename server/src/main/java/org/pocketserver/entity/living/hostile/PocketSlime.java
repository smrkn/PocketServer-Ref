package org.pocketserver.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Slime;
import org.pocketserver.entity.living.PocketHostile;

public class PocketSlime extends PocketHostile implements Slime {

    private volatile int size;

    public PocketSlime() {
        super(EntityType.SLIME);
    }

    protected PocketSlime(EntityType type) {
        super(type);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

}

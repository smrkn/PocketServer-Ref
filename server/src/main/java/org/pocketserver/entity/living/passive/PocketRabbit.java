package org.pocketserver.entity.living.passive;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.passive.Rabbit;
import org.pocketserver.api.entity.living.passive.RabbitBreed;
import org.pocketserver.entity.living.PocketAnimal;

public class PocketRabbit extends PocketAnimal implements Rabbit {

    private volatile RabbitBreed breed;

    public PocketRabbit() {
        super(EntityType.RABBIT);
    }

    @Override
    public RabbitBreed getBreed() {
        return breed;
    }

    @Override
    public void setBreed(RabbitBreed breed) {
        this.breed = breed;
    }

}

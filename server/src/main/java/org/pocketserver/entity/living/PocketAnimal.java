package org.pocketserver.entity.living;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.Animal;

public abstract class PocketAnimal extends GrowingEntity implements Animal {

    public PocketAnimal(EntityType type) {
        super(type);
    }

}

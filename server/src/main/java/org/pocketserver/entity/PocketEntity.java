package org.pocketserver.entity;

import org.pocketserver.PocketServer;
import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.event.entity.EntityTeleportEvent;
import org.pocketserver.api.world.locale.DirectionalLocation;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;

import com.google.common.base.Preconditions;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class PocketEntity implements Entity {

    protected static final Supplier<Integer> RANDOM_ID_SUPPLIER = () -> ThreadLocalRandom.current()
        .nextInt();

    protected static final Supplier<Integer> SEQUENTIAL_ID_SUPPLIER = new Supplier<Integer>() {
        private final AtomicInteger counter = new AtomicInteger(1);

        @Override
        public Integer get() {
            return counter.getAndIncrement();
        }
    };

    private final EntityType type;
    private final int entityId;
    private DirectionalLocation location;
    private Vector velocity;
    private boolean spawned;

    public PocketEntity(EntityType type) {
        this(type, SEQUENTIAL_ID_SUPPLIER);
    }

    protected PocketEntity(EntityType type, Supplier<Integer> idSupplier) {
        Preconditions.checkNotNull(idSupplier, "idSupplier should not be null");
        this.type = type.bound();
        this.entityId = idSupplier.get();
        this.location = new DirectionalLocation(new Location(null, 0, 72, 0), 0,0,0);
    }

    @Override
    public boolean isSpawned() {
        return spawned;
    }

    @Override
    public void destroy() {
        // TODO: Remove entity from game and memory.
    }

    @Override
    public long getEntityId() {
        return entityId;
    }

    @Override
    public EntityType getType() {
        return type;
    }

    @Override
    public Vector getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }

    @Override
    public void teleport(Location location) {
        EntityTeleportEvent event = new EntityTeleportEvent(this, this.location, location);
        PocketServer.getServer().getPluginManager().post(event);

        if (event.isCancelled()) {
            return;
        }

        this.location = event.getTo().toDirectionalLocation();
        // TODO: Send teleport packets etc
    }

    @Override
    public int getFireTicks() {
        return 0;
    }

    @Override
    public void setFireTicks(int fireTicks) {

    }

    @Override
    public DirectionalLocation getLocation() {
        return location;
    }

    @Override
    public void setLocation(Location location) {
        Preconditions.checkNotNull(location, "location should not be null");
        this.location = location.toDirectionalLocation();
        if (isSpawned()) {
            teleport(location);
        }
    }

}

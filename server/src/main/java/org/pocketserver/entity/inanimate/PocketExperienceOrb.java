package org.pocketserver.entity.inanimate;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.inanimate.ExperienceOrb;
import org.pocketserver.entity.PocketEntity;

public class PocketExperienceOrb extends PocketEntity implements ExperienceOrb {

    private volatile double experience = 0;

    public PocketExperienceOrb() {
        super(EntityType.EXPERIENCE_ORB);
    }

    @Override
    public double getExperience() {
        return experience;
    }

    @Override
    public void setExperience(double experience) {
        this.experience = experience;
    }

}

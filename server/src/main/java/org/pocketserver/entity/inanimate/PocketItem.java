package org.pocketserver.entity.inanimate;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.inanimate.Item;
import org.pocketserver.api.inventory.ItemStack;
import org.pocketserver.entity.PocketEntity;

public class PocketItem extends PocketEntity implements Item {

    private volatile ItemStack item;

    public PocketItem() {
        super(EntityType.DROPPED_ITEM);
    }

    @Override
    public ItemStack getItemStack() {
        return item;
    }

    @Override
    public void setItemStack(ItemStack item) {
        this.item = item != null ? item : new ItemStack(Material.AIR);
    }

}

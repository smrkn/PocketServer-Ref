package org.pocketserver.entity.inanimate;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.inanimate.Motif;
import org.pocketserver.api.entity.inanimate.Painting;
import org.pocketserver.api.world.locale.Orientation;
import org.pocketserver.entity.PocketEntity;

public class PocketPainting extends PocketEntity implements Painting {

    private volatile Orientation orientation;
    private volatile Motif motif;

    public PocketPainting() {
        super(EntityType.PAINTING);
    }

    @Override
    public Orientation getOrientation() {
        return orientation;
    }

    @Override
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public Motif getMotif() {
        return motif;
    }

    @Override
    public void setMotif(Motif motif) {
        this.motif = motif;
    }

}

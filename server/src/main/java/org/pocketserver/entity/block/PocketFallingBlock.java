package org.pocketserver.entity.block;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.block.FallingBlock;
import org.pocketserver.entity.PocketEntity;

public class PocketFallingBlock extends PocketEntity implements FallingBlock {

    private volatile Block block;

    public PocketFallingBlock() {
        super(EntityType.FALLING_BLOCK);
    }

    @Override
    public Block getBlock() {
        return block;
    }

    @Override
    public void setBlock(Block block) {
        this.block = block;
    }

}

package org.pocketserver.entity.block;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.block.PrimedTNT;
import org.pocketserver.entity.PocketEntity;

public class PocketPrimedTNT extends PocketEntity implements PrimedTNT {

    private volatile int explodeTicks;

    public PocketPrimedTNT() {
        super(EntityType.PRIMED_TNT);
    }

    @Override
    public int getExplodeTicks() {
        return explodeTicks;
    }

    @Override
    public void setExplodeTicks(int explodeTicks) {
        this.explodeTicks = explodeTicks;
    }

}

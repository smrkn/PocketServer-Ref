package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.projectile.FishingHook;

public class PocketFishingHook extends PocketProjectile implements FishingHook {

    public PocketFishingHook() {
        this(null);
    }

    public PocketFishingHook(ProjectileShooter shooter) {
        super(EntityType.FISHING_ROD_HOOK, shooter);
    }

}

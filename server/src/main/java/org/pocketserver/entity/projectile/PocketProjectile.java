package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.Projectile;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.entity.PocketEntity;

public abstract class PocketProjectile extends PocketEntity implements Projectile {

    private final ProjectileShooter shooter;

    public PocketProjectile(EntityType type, ProjectileShooter shooter) {
        super(type);
        this.shooter = shooter;
    }

    @Override
    public ProjectileShooter getShooter() {
        return shooter;
    }

}

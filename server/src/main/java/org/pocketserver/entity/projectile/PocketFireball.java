package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.projectile.Fireball;

public class PocketFireball extends PocketProjectile implements Fireball {

    public PocketFireball() {
        this(null);
    }

    public PocketFireball(ProjectileShooter shooter) {
        super(EntityType.FIREBALL, shooter);
    }

}

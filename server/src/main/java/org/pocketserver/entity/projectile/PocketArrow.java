package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.projectile.Arrow;

public class PocketArrow extends PocketProjectile implements Arrow {

    public PocketArrow() {
        this(null);
    }

    public PocketArrow(ProjectileShooter shooter) {
        super(EntityType.ARROW, shooter);
    }

}

package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.projectile.Snowball;

public class PocketSnowball extends PocketProjectile implements Snowball {

    public PocketSnowball() {
        this(null);
    }

    public PocketSnowball(ProjectileShooter shooter) {
        super(EntityType.SNOWBALL, shooter);
    }

}

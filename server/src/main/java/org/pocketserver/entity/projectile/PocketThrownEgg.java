package org.pocketserver.entity.projectile;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.projectile.ThrownEgg;

public class PocketThrownEgg extends PocketProjectile implements ThrownEgg {

    public PocketThrownEgg() {
        this(null);
    }

    public PocketThrownEgg(ProjectileShooter shooter) {
        super(EntityType.EGG, shooter);
    }

}

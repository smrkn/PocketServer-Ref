package org.pocketserver;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.pocketserver.api.Server;
import org.pocketserver.api.command.ConsoleCommandExecutor;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.entity.living.hostile.Zombie;
import org.pocketserver.api.entity.living.hostile.ZombieType;
import org.pocketserver.api.event.world.WorldLoadEvent;
import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PluginManager;
import org.pocketserver.api.util.Pipeline;
import org.pocketserver.api.util.PocketLogging;
import org.pocketserver.api.world.World;
import org.pocketserver.command.CommandPlugins;
import org.pocketserver.command.CommandShutdown;
import org.pocketserver.command.CommandVersion;
import org.pocketserver.entity.PocketEntity;
import org.pocketserver.entity.block.PocketFallingBlock;
import org.pocketserver.entity.block.PocketPrimedTNT;
import org.pocketserver.entity.inanimate.PocketExperienceOrb;
import org.pocketserver.entity.inanimate.PocketItem;
import org.pocketserver.entity.inanimate.PocketPainting;
import org.pocketserver.entity.living.hostile.PocketBlaze;
import org.pocketserver.entity.living.hostile.PocketCreeper;
import org.pocketserver.entity.living.hostile.PocketGhast;
import org.pocketserver.entity.living.hostile.PocketMagmaCube;
import org.pocketserver.entity.living.hostile.PocketSilverfish;
import org.pocketserver.entity.living.hostile.PocketSkeleton;
import org.pocketserver.entity.living.hostile.PocketSlime;
import org.pocketserver.entity.living.hostile.PocketSpider;
import org.pocketserver.entity.living.hostile.PocketZombie;
import org.pocketserver.entity.living.neutral.PocketCaveSpider;
import org.pocketserver.entity.living.neutral.PocketEnderman;
import org.pocketserver.entity.living.neutral.PocketIronGolem;
import org.pocketserver.entity.living.neutral.PocketPigZombie;
import org.pocketserver.entity.living.neutral.PocketSnowGolem;
import org.pocketserver.entity.living.neutral.PocketWolf;
import org.pocketserver.entity.living.passive.PocketBat;
import org.pocketserver.entity.living.passive.PocketChicken;
import org.pocketserver.entity.living.passive.PocketCow;
import org.pocketserver.entity.living.passive.PocketMooshroom;
import org.pocketserver.entity.living.passive.PocketOcelot;
import org.pocketserver.entity.living.passive.PocketPig;
import org.pocketserver.entity.living.passive.PocketRabbit;
import org.pocketserver.entity.living.passive.PocketSheep;
import org.pocketserver.entity.living.passive.PocketSquid;
import org.pocketserver.entity.living.passive.PocketVillager;
import org.pocketserver.entity.projectile.PocketArrow;
import org.pocketserver.entity.projectile.PocketFireball;
import org.pocketserver.entity.projectile.PocketFishingHook;
import org.pocketserver.entity.projectile.PocketSnowball;
import org.pocketserver.entity.projectile.PocketThrownEgg;
import org.pocketserver.entity.vehicle.PocketBoat;
import org.pocketserver.entity.vehicle.PocketMinecart;
import org.pocketserver.net.Packet;
import org.pocketserver.net.PipelineUtils;
import org.pocketserver.net.Protocol;
import org.pocketserver.net.packet.play.PacketPlayDisconnect;
import org.pocketserver.net.packet.play.PacketPlayText;
import org.pocketserver.permissions.PocketPermissionResolver;
import org.pocketserver.player.PocketPlayer;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.pocketserver.world.PocketWorld;
import org.pocketserver.world.storage.Providers;
import org.pocketserver.api.world.provider.WorldProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.FixedRecvByteBufAllocator;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PocketServer extends Server {

    private static final int PORT = 19132;
    private static PocketServer SERVER;

    public static PocketServer getServer() {
        return SERVER;
    }

    private final Pipeline<PermissionResolver> permissionPipeline;
    private final Multimap<String, PocketPlayer> connectionMap;
    private final ConsoleCommandExecutor consoleExecutor;
    private final EventLoopGroup eventLoopGroup;
    private final ReadWriteLock connectionLock;
    private final PluginManager pluginManager;
    private final List<PocketWorld> worldList;
    private final File directory;
    private final Logger logger;
    private volatile boolean running;
    private Channel channel;

    PocketServer() throws Exception {
        this.directory = new File(".").toPath().toAbsolutePath().toFile();
        Preconditions.checkState(directory.getAbsolutePath().indexOf('!') == -1, "PocketServer cannot be run from " +
            "inside " +
            "an archive");

        SERVER = this;
        Server.setServer(SERVER);
        this.consoleExecutor = new ConsoleCommandExecutor(this);
        this.logger = LoggerFactory.getLogger("PocketServer");
        this.eventLoopGroup = PipelineUtils.newEventLoop(6);
        this.connectionLock = new ReentrantReadWriteLock();
        this.pluginManager = new PluginManager(this);
        this.worldList = new CopyOnWriteArrayList<>();
        this.permissionPipeline = Pipeline.of();
        this.connectionMap = ArrayListMultimap.create();

        getLogger().debug("Directory: {}", directory.toString());
        getLogger().debug("Server ID: {}", Protocol.SERVER_ID);

        if (new File(directory, "plugins").mkdirs()) {
            getLogger().info(PocketLogging.Server.STARTUP, "Created \"plugins\" directory");
        }

        startListener();
        registerEntities();
        loadWorld();

        getPermissionPipeline().addFirst(new PocketPermissionResolver());
        getPluginManager().registerCommand(null, new CommandShutdown(this));
        getPluginManager().registerCommand(null, new CommandVersion());
        getPluginManager().registerCommand(null, new CommandPlugins(this.pluginManager));
        this.pluginManager.loadPlugins();
    }

    private void startListener() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);

        // TODO: Add configuration stuff
        ChannelFutureListener listener = future -> {
            if (future.isSuccess()) {
                getLogger().info(PocketLogging.Server.STARTUP, "Listening on port {}", PORT);
                channel = future.channel();
                latch.countDown();
                running = true;
            } else {
                getLogger().error(PocketLogging.Server.STARTUP, "Could not bind to {}", PORT, future
                    .cause());
                shutdown();
            }
        };

        new Bootstrap()
            .group(eventLoopGroup)
            .handler(PipelineUtils.INITIALIZER)
            .channel(PipelineUtils.getChannelClass())
            .option(ChannelOption.SO_BROADCAST, true)
            .option(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(Short.MAX_VALUE)) // BLAME CONNOR
            .bind(PORT)
            .addListener(listener);

        latch.await();
    }

    @Override
    public void shutdown() {
        running = false;

        if (channel != null && channel.isOpen()) {
            channel.close().addListener(future -> {
                if (future.isSuccess()) {
                    getLogger().info(PocketLogging.Server.SHUTDOWN, "Closing listener {}", channel);
                } else {
                    getLogger().error(PocketLogging.Server.SHUTDOWN, "Failed to close listener", future
                        .cause());
                }
            }).syncUninterruptibly();
        }

        Packet packet = new PacketPlayDisconnect("Server is shutting down!");
        broadcast(packet);

        for (Iterator<PermissionResolver> resolvers = permissionPipeline.iterator(); resolvers
            .hasNext(); ) {
            PermissionResolver resolver = resolvers.next();
            try {
                resolver.close();
            } catch (Throwable cause) {
                getLogger().error(PocketLogging.Server.SHUTDOWN, "Failed to close " +
                        "PermissionResolver[type={}]",
                    new Object[]{
                        resolver.getClass().getCanonicalName(),
                        cause
                    });
            } finally {
                resolvers.remove();
            }
        }

        getLogger().info(PocketLogging.Server.SHUTDOWN, "Disabling plugins");
        Lists.reverse(getPluginManager().getPlugins()).stream().filter(Plugin::isEnabled)
            .forEachOrdered(plugin -> getPluginManager().setEnabled(plugin, false));

        getLogger().info(PocketLogging.Server.SHUTDOWN, "Closing IO threads");
        eventLoopGroup.shutdownGracefully();
        try {
            // Don't bother if it already shut down
            if (!eventLoopGroup.isShutdown() && eventLoopGroup.isShuttingDown()) {
                eventLoopGroup.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            }
        } catch (Exception e) {
            // NOP
        }

        getLogger().info("Thanks for using PocketServer!");
        System.exit(0);
    }

    @Override
    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    @Override
    public Logger getLogger() {
        return this.logger;
    }

    @Override
    public File getDirectory() {
        return directory;
    }

    @Override
    public Pipeline<PermissionResolver> getPermissionPipeline() {
        return permissionPipeline;
    }

    @Override
    public Collection<PocketPlayer> getPlayer(String username) {
        connectionLock.readLock().lock();
        try {
            return connectionMap.get(username.toLowerCase());
        } finally {
            connectionLock.readLock().unlock();
        }
    }

    @Override
    public List<PocketPlayer> getPlayers(Predicate<Player> filter) {
        return connectionMap.values().stream().filter(filter).collect(Collectors.toList());
    }

    @Override
    public Collection<PocketPlayer> getOnlinePlayers() {
        connectionLock.readLock().lock();
        try {
            return ImmutableList.copyOf(connectionMap.values());
        } finally {
            connectionLock.readLock().unlock();
        }
    }

    @Override
    public ConsoleCommandExecutor getConsole() {
        return consoleExecutor;
    }

    @Override
    public void broadcastMessage(String message) {
        broadcastMessage(message, p -> true);
    }

    @Override
    public void broadcastMessage(String message, Predicate<Player> filter) {
        broadcast(new PacketPlayText(PacketPlayText.TextType.SYSTEM, message, Optional.empty()), filter::test);
    }

    @Override
    public PocketWorld getDefaultWorld() {
        return worldList.get(0);
    }

    public void broadcast(Packet packet) {
        broadcast(packet, player -> true);
    }

    public void broadcast(Packet packet, Predicate<PocketPlayer> predicate) {
        connectionLock.writeLock().lock();
        try {
            connectionMap.values().stream().filter(predicate::test).forEach(player -> player.unsafe()
                .send(packet, true)); //TODO: Don't send these as instant
        } finally {
            connectionLock.writeLock().unlock();
        }
    }

    private void loadWorld() {
        WorldProvider provider = Providers.getInstance().getDefaultProvider();
        WorldLoadEvent event = new WorldLoadEvent(provider);
        getPluginManager().post(event);
        if (event.isCancelled()) {
            throw new RuntimeException("The world load was cancelled. There must be at least one world loaded.");
        }
        long time = System.currentTimeMillis();
        Future<World> future = event.getProvider().loadWorld("world", false);//TODO: Have default world name in file
        try {
            World world = future.get(30, TimeUnit.SECONDS);
            this.worldList.add((PocketWorld) world);
            double secs = (System.currentTimeMillis() - time) / 1000.00;
            getLogger().info(PocketLogging.Server.WORLD, "Loaded world, " + world.getName() + ", in " + secs + " secs.");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void registerEntities() {
        Map<EntityType, Class<? extends PocketEntity>> entities = new HashMap<>();
        entities.put(EntityType.ARROW, PocketArrow.class);
        entities.put(EntityType.BAT, PocketBat.class);
        entities.put(EntityType.BLAZE, PocketBlaze.class);
        entities.put(EntityType.BOAT, PocketBoat.class);
        entities.put(EntityType.CAVE_SPIDER, PocketCaveSpider.class);
        entities.put(EntityType.CHICKEN, PocketChicken.class);
        entities.put(EntityType.COW, PocketCow.class);
        entities.put(EntityType.CREEPER, PocketCreeper.class);
        entities.put(EntityType.DROPPED_ITEM, PocketItem.class);
        entities.put(EntityType.EGG, PocketThrownEgg.class);
        entities.put(EntityType.ENDERMAN, PocketEnderman.class);
        entities.put(EntityType.EXPERIENCE_ORB, PocketExperienceOrb.class);
        entities.put(EntityType.FALLING_BLOCK, PocketFallingBlock.class);
        entities.put(EntityType.FIREBALL, PocketFireball.class);
        entities.put(EntityType.FISHING_ROD_HOOK, PocketFishingHook.class);
        entities.put(EntityType.GHAST, PocketGhast.class);
        entities.put(EntityType.IRON_GOLEM, PocketIronGolem.class);
        entities.put(EntityType.MAGMA_CUBE, PocketMagmaCube.class);
        entities.put(EntityType.MINECART, PocketMinecart.class);
        entities.put(EntityType.MOOSHROOM, PocketMooshroom.class);
        entities.put(EntityType.OCELOT, PocketOcelot.class);
        entities.put(EntityType.PAINTING, PocketPainting.class);
        entities.put(EntityType.PIG, PocketPig.class);
        entities.put(EntityType.PRIMED_TNT, PocketPrimedTNT.class);
        entities.put(EntityType.RABBIT, PocketRabbit.class);
        entities.put(EntityType.SHEEP, PocketSheep.class);
        entities.put(EntityType.SILVERFISH, PocketSilverfish.class);
        entities.put(EntityType.SKELETON, PocketSkeleton.class);
        entities.put(EntityType.SLIME, PocketSlime.class);
        entities.put(EntityType.SNOW_GOLEM, PocketSnowGolem.class);
        entities.put(EntityType.SNOWBALL, PocketSnowball.class);
        entities.put(EntityType.SPIDER, PocketSpider.class);
        entities.put(EntityType.SQUID, PocketSquid.class);
        entities.put(EntityType.VILLAGER, PocketVillager.class);
        entities.put(EntityType.WOLF, PocketWolf.class);
        entities.put(EntityType.ZOMBIE, PocketZombie.class);
        entities.put(EntityType.ZOMBIE_PIGMAN, PocketPigZombie.class);
        entities.put(EntityType.ZOMBIE_VILLAGER, PocketZombie.class);
        entities.forEach((t, c) -> {
            try {
                t.setImplementation(c, (e, l) -> {
                    PocketEntity ent = null;
                    try {
                        Constructor<? extends PocketEntity> constr = c.getConstructor();
                        constr.setAccessible(true);
                        ent = constr.newInstance();
                        if (ent instanceof Zombie) {
                            ((Zombie) ent).setZombieType(ZombieType.VILLAGER);
                        }
                    } catch (ReflectiveOperationException ex) {
                        ex.printStackTrace();
                    }
                    return ent;
                });
            } catch (IllegalStateException ignored) {
            }
        });
    }

    public void addPlayer(PocketPlayer player) {
        Preconditions.checkNotNull(player, "player should not be null!");
        connectionLock.writeLock().lock();
        try {
            connectionMap.put(player.getName().toLowerCase(), player);
        } finally {
            connectionLock.writeLock().unlock();
        }
        player.setLocation(getDefaultWorld().getSpawn());
        getDefaultWorld().spawnPlayer(player);
    }

    public void removePlayer(PocketPlayer player) {
        Preconditions.checkNotNull(player, "player should not be null!");
        connectionLock.writeLock().lock();
        try {
            connectionMap.remove(player.getName().toLowerCase(), player);
        } finally {
            connectionLock.writeLock().unlock();
        }
    }

    public Optional<PocketPlayer> getPlayer(InetSocketAddress address) {
        connectionLock.readLock().lock();
        try {
            return connectionMap.values().stream()
                .filter(player -> address.equals(player.getAddress()))
                .findFirst();
        } finally {
            connectionLock.readLock().unlock();
        }
    }

    public boolean isRunning() {
        return this.running;
    }
}

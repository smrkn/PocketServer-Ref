package org.pocketserver.permissions;

import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.player.Player;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class PocketPermissionResolver implements PermissionResolver {

    private static final boolean OPS_ALL_PERMISSIONS = Boolean.getBoolean("pocket.ops-have-all");

    private final Set<String> operatorPermissions;
    private final Set<String> defaultPermissions;

    public PocketPermissionResolver() {
        this.operatorPermissions = ImmutableSet.of(
            "pocket.command.plugins",
            "pocket.command.whois",
            "pocket.command.kick",
            "pocket.command.stop",
            "pocket.command.ban"
        );

        this.defaultPermissions = ImmutableSet.of(
            "pocket.command.version",
            "pocket.command.help"
        );
    }

    @Override
    public Result checkPermission(Player player, String permission) {
        if ((OPS_ALL_PERMISSIONS && player.isOp()) || (player.isOp() && operatorPermissions.contains
            (permission)) || defaultPermissions.contains(permission)) {
            return Result.ALLOW;
        }
        return Result.UNSET;
    }
}

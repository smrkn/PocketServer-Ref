#This is the old version of the project. We decided to start fresh in a seperate repo. We will no longer be taking contributions to this version. You can find the new version the project located [here](https://github.com/PocketServer/PocketServer)

PocketServer
============

[![Join the chat at https://gitter.im/PocketServer/PocketServer](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/PocketServer/PocketServer?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Build Status](https://travis-ci.org/PocketServer/PocketServer.svg)](https://travis-ci.org/PocketServer/PocketServer)

A W.I.P Server software for MCPE.

## Table of Contents
- [How can I help?](#how-can-i-help)
- [Project ETA](#project-eta)

## How can I help?

We're attempting to tag as many incomplete files as possible with `TODO` comments so that we can easily find them. If you'd like to help reduce the number of things we have to do then we'd be more than happy to accept your contributions! Find out what you can help with [here](https://github.com/PocketServer/PocketServer/search?utf8=%E2%9C%93&q=TODO)!

**Note**: If you're looking to make changes in a file that's actively being updated you may wish to open an issue before doing any work, just to be on the safe side!

## Project ETA

While we currently have no ETA, we are working actively on it and hope to be finished as soon as possible.

## Captain's Log

We're aware the API and packet system need a little work. It's on the todo list.

## Acknowledgements
[![YourKit Logo](https://www.yourkit.com/images/yklogo.png)](https://yourkit.com)

We use YourKit for profiling our code and helping monitor performance. Check it out at https://yourkit.com!

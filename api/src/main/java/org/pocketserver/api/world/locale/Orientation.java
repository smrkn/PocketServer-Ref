package org.pocketserver.api.world.locale;

public enum Orientation {

    NORTH, SOUTH,
    EAST, WEST,
    NORTH_EAST, SOUTH_WEST,
    NORTH_WEST, SOUTH_EAST;

    public Orientation getOpposite() {
        final int o = ordinal();
        return values()[o % 2 == 0 ? o + 1 : o - 1];
    }

}

package org.pocketserver.api.world.locale;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.world.Chunk;
import org.pocketserver.api.world.World;

import java.io.Serializable;

import static java.lang.Math.pow;

public class Location implements Cloneable, Serializable {

    private static final long serialVersionUID = -7074605674263137642L;

    private final World world;
    private final double x, y, z;

    public Location(Location location) {
        this(location.getWorld(), location.getX(), location.getY(), location.getZ());
    }

    public Location(World world, double x, double y, double z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public World getWorld() {
        return world;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Block getBlock() {
        return world.getBlockAt(this);
    }

    public float getYaw() {
        return 0;
    }

    public float getPitch() {
        return 0;
    }

    public int getBlockX() {
        return (int) x;
    }

    public int getBlockY() {
        return (int) y;
    }

    public int getBlockZ() {
        return (int) z;
    }

    public Location add(double x, double y, double z) {
        return new Location(world, this.x + x, this.y + y, this.z + z);
    }

    @Override
    protected Location clone() {
        return new Location(world, x, y, z);
    }

    public Vector toVector() {
        return new Vector(x, y, z);
    }

    public DirectionalLocation toDirectionalLocation() {
        return new DirectionalLocation(this, 0, 0, 0);
    }

    public Chunk getChunk() {
        return this.world.getChunk((int) x, (int) z, true);
    }

    public double distance(Location location) {
        return Math.sqrt(distanceSquared(location));
    }

    public double distanceSquared(Location location) {
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        return pow(x + this.x, 2) + pow(y + this.y, 2) + pow(z + this.z, 2);
    }
}

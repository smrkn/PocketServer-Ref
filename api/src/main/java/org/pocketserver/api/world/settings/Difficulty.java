package org.pocketserver.api.world.settings;

public enum Difficulty {
    PEACEFUL(0),
    EASY(1),
    NORMAL(2),
    HARD(3);

    private final int id;

    Difficulty(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Difficulty getDefault() {
        return Difficulty.NORMAL;
    }
}

package org.pocketserver.api.world.locale;

public class Bounds {

    private final int width;
    private final int height;

    public Bounds(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

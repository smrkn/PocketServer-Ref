package org.pocketserver.api.world.locale;

import com.google.common.base.Preconditions;
import org.pocketserver.api.world.World;

public class Vector implements Cloneable {

    public static final Vector ZERO = new Vector(0, 0, 0);

    private double x, y, z;
    private Double len, lensq;

    public Vector() {
        this(0, 0, 0);
    }

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double lengthSquared() {
        return lensq != null ? lensq : (lensq = distanceSquared(ZERO));
    }

    public double distanceSquared(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return Math.pow(x - vec.x, 2) + Math.pow(y - vec.y, 2) + Math.pow(z - vec.z, 2);
    }

    public Vector subtract(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return add(vec.multiply(-1));
    }

    public Vector add(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return add(vec.x, vec.y, vec.z);
    }

    public Vector add(double x, double y, double z) {
        return new Vector(this.x + x, this.y + y, this.z + z);
    }

    public Vector multiply(double multiple) {
        return new Vector(x * multiple, y * multiple, z * multiple);
    }

    public double dot(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return x * vec.x + y * vec.y + z * vec.z;
    }

    public Vector cross(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return new Vector(y * vec.z - z * vec.y, z * vec.x - x * vec.z, x * vec.y - y * vec.x);
    }

    public Vector normalize() {
        double l = length();
        return new Vector(x / l, y / l, z / l);
    }

    public double length() {
        return len != null ? len : (len = distance(ZERO));
    }

    public double distance(Vector vec) {
        Preconditions.checkNotNull(vec, "vector is null");
        return Math.sqrt(distanceSquared(vec));
    }

    @Override
    public Vector clone() {
        return new Vector(x, y, z);
    }

    public Location toLocation(World world) {
        return new Location(world, x, y, z);
    }

    public DirectionalLocation toLocation(World world, float yaw, float bodyYaw, float pitch) {
        return new DirectionalLocation(world, x, y, z, yaw, bodyYaw, pitch);
    }

}

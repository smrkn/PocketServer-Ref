package org.pocketserver.api.world;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.block.Material;
import org.pocketserver.api.world.locale.Location;

import java.io.IOException;

public interface Chunk {

    World getWorld();

    int getX();

    int getZ();

    Block getBlock(Location loc);

    Block getBlock(int dx, int dy, int dz);

    boolean isInChunk(Location location);

    boolean isInChunk(int x, int y, int z);

    void queueUpdate(int x, int y, int z, Material material);

    void proposeUpdates();

    byte[] getBytes() throws IOException, Exception;
}
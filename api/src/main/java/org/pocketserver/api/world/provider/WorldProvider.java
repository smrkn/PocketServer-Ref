package org.pocketserver.api.world.provider;

import org.pocketserver.api.world.World;

import java.util.concurrent.Future;

public interface WorldProvider {
    Future<World> loadWorld(String name, boolean async);

    void saveWorld(World world, boolean async);

    void loadChunk(World world, int x, int z, boolean async);
}

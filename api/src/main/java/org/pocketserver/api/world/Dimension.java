package org.pocketserver.api.world;

public enum Dimension {
    OVERWORLD(0),
    NETHER(1),
    THE_END(2);

    private final byte id;

    Dimension(byte id) {
        this.id = id;
    }

    Dimension(int i) {
        this((byte) i);
    }

    public byte getId() {
        return id;
    }
}

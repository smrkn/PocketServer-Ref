package org.pocketserver.api.world.locale;

import org.pocketserver.api.world.World;

public class DirectionalLocation extends Location implements Cloneable {

    private static final long serialVersionUID = 1863753589932919322L;

    private float yaw, bodyYaw, pitch;

    public DirectionalLocation(Location location, float yaw, float bodyYaw, float pitch) {
        this(location.getWorld(), location.getX(), location.getY(), location.getZ(), yaw, bodyYaw,
            pitch);
    }

    public DirectionalLocation(World world, double x, double y, double z, float yaw, float bodyYaw,
                               float pitch) {
        super(world, x, y, z);
        this.yaw = yaw;
        this.bodyYaw = bodyYaw;
        this.pitch = pitch;
    }

    public float getBodyYaw() {
        return bodyYaw;
    }

    public Vector getDirection() {
        double rotX = getYaw();
        double rotY = getPitch();
        double xz = Math.cos(Math.toRadians(rotY));

        double x = -xz * Math.sin(Math.toRadians(rotX));
        double y = -Math.sin(Math.toRadians(rotY));
        double z = xz * Math.cos(Math.toRadians(rotX));

        return new Vector(x, y, z);
    }

    @Override
    public float getYaw() {
        return yaw;
    }

    @Override
    public float getPitch() {
        return pitch;
    }

    @Override
    public DirectionalLocation clone() {
        return new DirectionalLocation(super.clone(), yaw, bodyYaw, pitch);
    }

    @Override
    public DirectionalLocation toDirectionalLocation() {
        return this;
    }

}

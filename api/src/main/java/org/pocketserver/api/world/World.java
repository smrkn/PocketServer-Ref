package org.pocketserver.api.world;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.locale.Bounds;
import org.pocketserver.api.world.locale.Location;

import java.util.List;

public interface World {

    Chunk getChunk(int x, int z);

    Chunk getChunk(int cx, int cz, boolean load);

    Block getBlockAt(int x, int y, int z);

    Block getBlockAt(Location location);

    void spawnPlayer(Player player);

    Entity spawnEntity(EntityType entityType, Location location);

    Bounds getDimensions();

    List<Entity> getEntities();

    List<Entity> getNearbyEntities(Location location);

    List<Entity> getNearbyEntities(Location location, int distance);

    String getName();

    void updateWorld();

    void setTime(long time);

    long getTime();
}

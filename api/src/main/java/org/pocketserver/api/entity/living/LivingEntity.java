package org.pocketserver.api.entity.living;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.navigation.Navigation;

/**
 * Represents a living {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface LivingEntity extends Entity {

    /**
     * Gets whether or not this entity is alive.
     *
     * @return True iff this entity is alive.
     */
    default boolean isAlive() {
        return isSpawned() && getHealth() > 0;
    }

    /**
     * Gets this entity's health.
     *
     * @return The health.
     */
    double getHealth();

    /**
     * Sets this entity's health.
     *
     * @param health The new health.
     */
    void setHealth(double health);

    /**
     * Gets this entity's maximum health.
     *
     * @return The entity's max health.
     */
    double getMaxHealth();

    /**
     * Sets this entity's maximum health.
     *
     * @param health The new max health.
     */
    void setMaxHealth(double health);

    /**
     * Resets the maximum health to the default amount, reducing health if necessary.
     */
    void resetMaxHealth();

    /**
     * Gets this entity's {@link Navigation} controller.
     *
     * @return The navigation controller.
     */
    Navigation getNavigation();

}

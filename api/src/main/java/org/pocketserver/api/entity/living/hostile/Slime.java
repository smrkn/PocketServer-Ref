package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

/**
 * Represents a slime, a {@link Hostile hostile} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Slime extends Hostile {

    /**
     * Gets this Slime's size, on a scale of 0 to 255, inclusive.
     *
     * @return The slime's size.
     */
    int getSize();

    /**
     * Sets this Slime's size. Fails if the size is not in the range of 0 to 255 (inclusive).
     *
     * @param size The new size.
     */
    void setSize(int size);

}

package org.pocketserver.api.entity;

import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.world.locale.DirectionalLocation;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;
import org.pocketserver.api.world.World;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Represents an entity in the game.
 *
 * @author Nick Robson
 */
public interface Entity {

    /**
     * Gets whether or not this entity is valid.
     *
     * @return The entity's validity status.
     */
    boolean isSpawned();

    /**
     * Destroys this entity, killing it if it is a {@link LivingEntity}.
     */
    void destroy();

    /**
     * Gets the internal identification number given to this entity.
     *
     * @return The internal ID.
     */
    long getEntityId();

    /**
     * Gets the entity's type.
     *
     * @return The type.
     */
    EntityType getType();

    /**
     * Gets this entity's current velocity.
     *
     * @return The entity's velocity.
     */
    Vector getVelocity();

    /**
     * Sets this entity's current velocity.
     *
     * @param velocity The new velocity.
     */
    void setVelocity(Vector velocity);

    /**
     * Teleports this entity to a given location.
     *
     * @param location The location to teleport to.
     */
    void teleport(Location location);

    /**
     * Gets the number of ticks for which this entity will be on fire for.
     *
     * @return The number of ticks remaining.
     */
    int getFireTicks();

    /**
     * Sets the number of ticks for which this entity with be on fire for.
     *
     * @param ticks The number of remaining ticks.
     */
    void setFireTicks(int ticks);

    /**
     * Gets the optional nearest entity of a given type to this entity.
     *
     * @param clazz The entity class to search for.
     * @return An optional nearest entity.
     */
    default <T extends Entity> Optional<T> getNearest(Class<T> clazz) {
        Location loc = getLocation();
        World world = loc.getWorld();
        List<Entity> entities = world.getEntities()
            .stream()
            .filter(e -> clazz.isAssignableFrom(e.getClass()))
            .collect(Collectors.toList());
        entities.sort((e1, e2) -> (int) (loc.distanceSquared(e1.getLocation()) * 100 - 100 * loc
            .distanceSquared(e2.getLocation())));
        return entities.isEmpty() ? Optional.empty() : Optional.of(clazz.cast(entities.get(0)));
    }

    /**
     * Gets this entity's current location.
     *
     * @return The entity's location.
     */
    DirectionalLocation getLocation();

    /**
     * Sets this entity's current location.
     *
     * @param location The new location.
     */
    void setLocation(Location location);

}

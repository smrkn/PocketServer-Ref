package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.living.Flying;
import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.projectile.Fireball;

/**
 * Represents a ghast, a {@link Hostile hostile}, {@link Flying flying} {@link LivingEntity entity}
 * capable of {@link ProjectileShooter shooting} {@link Fireball fireball}s.
 *
 * @author Nick Robson
 */
public interface Ghast extends Hostile, Flying, ProjectileShooter {

}

package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;
import org.pocketserver.api.entity.living.Tameable;

/**
 * Represents an ocelot, a {@link Tameable tameable} {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Ocelot extends Animal, Tameable {

    OcelotBreed getBreed();

    void setBreed(OcelotBreed breed);

}

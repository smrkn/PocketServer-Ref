package org.pocketserver.api.entity.living;

/**
 * Represents an {@link LivingEntity entity} that can fly.
 *
 * @author Nick Robson
 */
public interface Flying extends LivingEntity {

    /**
     * Gets whether or not this entity is currently flying.
     *
     * @return True iff this entity is flying.
     */
    boolean isFlying();

    /**
     * Sets whether this entity is flying.
     *
     * @param flying Its new state as flying or not.
     */
    void setFlying(boolean flying);

}

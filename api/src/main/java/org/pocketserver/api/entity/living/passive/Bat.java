package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Flying;
import org.pocketserver.api.entity.living.Passive;

/**
 * Represents a bat, a {@link Passive passive}, {@link Flying flying} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Bat extends Passive, Flying {

}

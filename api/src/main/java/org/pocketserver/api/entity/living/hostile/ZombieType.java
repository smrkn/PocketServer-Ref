package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.EntityType;

/**
 * Represents the different types of Zombies.
 *
 * @author Nick Robson
 */
public enum ZombieType {

    /**
     * Represents the normal Zombie type.
     */
    NORMAL(EntityType.ZOMBIE),

    /**
     * Represents the villager Zombie type.
     */
    VILLAGER(EntityType.ZOMBIE_VILLAGER);

    private final EntityType entityType;

    private ZombieType(EntityType entityType) {
        this.entityType = entityType;
    }

    public EntityType toEntityType() {
        return entityType;
    }

}

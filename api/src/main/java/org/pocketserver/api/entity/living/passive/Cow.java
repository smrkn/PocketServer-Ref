package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;

/**
 * Represents a cow, an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Cow extends Animal {

}

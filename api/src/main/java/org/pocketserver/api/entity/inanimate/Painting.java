package org.pocketserver.api.entity.inanimate;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.world.locale.Orientation;

/**
 * Represents a painting, an {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface Painting extends Entity {

    /**
     * Gets this painting's orientation.
     *
     * @return The orientation.
     */
    Orientation getOrientation();

    /**
     * Sets this painting's orientation.
     *
     * @param orientation The new orientation.
     */
    void setOrientation(Orientation orientation);

    /**
     * Gets the motif expressed by this painting.
     *
     * @return The motif.
     */
    Motif getMotif();

    /**
     * Sets the motif expressed by this painting.
     *
     * @param motif The new motif.
     */
    void setMotif(Motif motif);

}

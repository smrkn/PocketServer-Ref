package org.pocketserver.api.entity.living;

/**
 * Represents an {@link LivingEntity entity} that can be a child or an adult.
 *
 * @author Nick Robson
 */
public interface Growing extends LivingEntity {

    /**
     * Gets this entity's age.
     *
     * @return The age.
     */
    int getAge();

    /**
     * Sets this entity's age.
     *
     * @param age The new age.
     */
    void setAge(int age);

    /**
     * Gets whether or not this entity is an adult.
     *
     * @return True iff it's an adult.
     */
    boolean isAdult();

    /**
     * Sets whether or not this entity is an adult.
     *
     * @param adult True if this entity will be an adult; false otherwise.
     */
    void setAdult(boolean adult);

}

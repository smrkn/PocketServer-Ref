package org.pocketserver.api.entity.navigation;

import org.pocketserver.api.world.locale.Location;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a path on which a {@link Navigation} will navigate entities.
 *
 * @author Nick Robson
 */
public class NavigationPath implements NavigationTarget {

    private final List<Location> waypoints;

    private int wayprg = -1;

    /**
     * Constructs a new navigation path.
     *
     * @param start The starting location.
     * @param end   The ending location.
     */
    public NavigationPath(Location start, Location end) {
        if (start.getWorld() != end.getWorld()) {
            throw new IllegalArgumentException("cannot navigate between worlds");
        }
        this.waypoints = new LinkedList<>(Arrays.asList(start, end));
    }

    /**
     * Gets the starting location of this path.
     *
     * @return The starting location.
     */
    public Location getStart() {
        return waypoints.get(0);
    }

    /**
     * Gets the ending location of this path.
     *
     * @return The ending location.
     */
    public Location getEnd() {
        return waypoints.get(waypoints.size() - 1);
    }

    /**
     * Gets a list containing all waypoints of this path, including start and end points.
     *
     * @return The waypoints.
     */
    public List<Location> getWaypoints() {
        return ImmutableList.copyOf(waypoints);
    }

    /**
     * Adds a waypoint to the waypoints list.
     * Fails if the given waypoint is in a different world.
     *
     * @param loc The waypoint.
     */
    public void addWaypoint(Location loc) {
        if (loc.getWorld() != waypoints.get(0).getWorld()) {
            throw new IllegalArgumentException("cannot navigate between worlds");
        }
        waypoints.add(waypoints.size() - 1, loc);
    }

    @Override
    public Location getTargetLocation() {
        Preconditions.checkNotNull(waypoints, "waypoints");
        if (wayprg >= waypoints.size() - 1) {
            return null;
        }
        return waypoints.get(wayprg + 1);
    }

}

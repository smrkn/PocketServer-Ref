package org.pocketserver.api.entity.projectile;

import org.pocketserver.api.entity.Projectile;
import org.pocketserver.api.entity.living.hostile.Ghast;

/**
 * Represents a fireball (such as those shot by {@link Ghast ghast}s), a {@link Projectile
 * projectile}.
 *
 * @author Nick Robson
 */
public interface Fireball extends Projectile {

}

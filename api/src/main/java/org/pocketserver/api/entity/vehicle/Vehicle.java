package org.pocketserver.api.entity.vehicle;

import org.pocketserver.api.entity.Entity;

/**
 * Represents a vehicle, a {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface Vehicle extends Entity {

}

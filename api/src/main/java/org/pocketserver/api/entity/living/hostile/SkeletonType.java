package org.pocketserver.api.entity.living.hostile;

/**
 * Represents the different types of skeletons.
 *
 * @author Nick Robson
 */
public enum SkeletonType {

    /**
     * Represents the normal Skeleton type.
     */
    NORMAL,

    /**
     * Represents the Wither Skeleton type.
     */
    WITHER;

}

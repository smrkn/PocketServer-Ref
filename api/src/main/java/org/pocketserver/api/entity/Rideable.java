package org.pocketserver.api.entity;

/**
 * Represents an {@link Entity entity} that can be ridden.
 *
 * @author Nick Robson
 */
public interface Rideable extends Entity {

    /**
     * Gets the passenger of this entity.
     *
     * @return The passenger.
     */
    Entity getPassenger();

    /**
     * Sets the passenger of this entity, failing if there already exists one.
     *
     * @param entity The new passenger.
     */
    void setPassenger(Entity entity);

}

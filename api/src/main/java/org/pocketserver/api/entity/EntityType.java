package org.pocketserver.api.entity;

import org.pocketserver.api.world.locale.Location;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * Represents the different types of {@link Entity entities}.
 *
 * @author Nick Robson
 */
public class EntityType {

    /**
     * Represents a chicken.
     */
    public static final EntityType CHICKEN = new EntityType("Chicken", 10);

    /**
     * Represents a cow.
     */
    public static final EntityType COW = new EntityType("Cow", 11);

    /**
     * Represents a pig.
     */
    public static final EntityType PIG = new EntityType("Pig", 12);

    /**
     * Represents a sheep.
     */
    public static final EntityType SHEEP = new EntityType("Sheep", 13);

    /**
     * Represents a wolf.
     */
    public static final EntityType WOLF = new EntityType("Wolf", 14);

    /**
     * Represents a villager.
     */
    public static final EntityType VILLAGER = new EntityType("Villager", 15);

    /**
     * Represents a mooshroom.
     */
    public static final EntityType MOOSHROOM = new EntityType("Mooshroom", 16);

    /**
     * Represents a squid.
     */
    public static final EntityType SQUID = new EntityType("Squid", 17);

    /**
     * Represents a rabbit.
     */
    public static final EntityType RABBIT = new EntityType("Rabbit", 18);

    /**
     * Represents a bat.
     */
    public static final EntityType BAT = new EntityType("Bat", 19);

    /**
     * Represents a iron golem.
     */
    public static final EntityType IRON_GOLEM = new EntityType("Iron Golem", 20);

    /**
     * Represents a snow golem.
     */
    public static final EntityType SNOW_GOLEM = new EntityType("Snow Golem", 21);

    /**
     * Represents a ocelot.
     */
    public static final EntityType OCELOT = new EntityType("Ocelot", 22);

    /**
     * Represents a zombie.
     */
    public static final EntityType ZOMBIE = new EntityType("Zombie", 32);

    /**
     * Represents a creeper.
     */
    public static final EntityType CREEPER = new EntityType("Creeper", 33);

    /**
     * Represents a skeleton.
     */
    public static final EntityType SKELETON = new EntityType("Skeleton", 34);

    /**
     * Represents a spider.
     */
    public static final EntityType SPIDER = new EntityType("Spider", 35);

    /**
     * Represents a zombie pigman.
     */
    public static final EntityType ZOMBIE_PIGMAN = new EntityType("Zombie Pigman", 36);

    /**
     * Represents a slime.
     */
    public static final EntityType SLIME = new EntityType("Slime", 37);

    /**
     * Represents a enderman.
     */
    public static final EntityType ENDERMAN = new EntityType("Enderman", 38);

    /**
     * Represents a silverfish.
     */
    public static final EntityType SILVERFISH = new EntityType("Silverfish", 39);

    /**
     * Represents a cave spider.
     */
    public static final EntityType CAVE_SPIDER = new EntityType("Cave Spider", 40);

    /**
     * Represents a ghast.
     */
    public static final EntityType GHAST = new EntityType("Ghast", 41);

    /**
     * Represents a magma cube.
     */
    public static final EntityType MAGMA_CUBE = new EntityType("Magma Cube", 42);

    /**
     * Represents a blaze.
     */
    public static final EntityType BLAZE = new EntityType("Blaze", 43);

    /**
     * Represents a zombie villager.
     */
    public static final EntityType ZOMBIE_VILLAGER = new EntityType("Zombie Villager", 44);

    /**
     * Represents a player.
     */
    public static final EntityType PLAYER = new EntityType("Player", 63);

    /**
     * Represents a dropped item.
     */
    public static final EntityType DROPPED_ITEM = new EntityType("Dropped Item", 64);

    /**
     * Represents a primed TNT.
     */
    public static final EntityType PRIMED_TNT = new EntityType("Primed TNT", 65);

    /**
     * Represents falling sand.
     */
    public static final EntityType FALLING_BLOCK = new EntityType("Falling Block", 66);

    /**
     * Represents a fishing rod hook.
     */
    public static final EntityType FISHING_ROD_HOOK = new EntityType("Fishing Rod Hook", 77);

    /**
     * Represents a arrow.
     */
    public static final EntityType ARROW = new EntityType("Arrow", 80);

    /**
     * Represents a snowball.
     */
    public static final EntityType SNOWBALL = new EntityType("Snowball", 81);

    /**
     * Represents a egg.
     */
    public static final EntityType EGG = new EntityType("Egg", 82);

    /**
     * Represents a painting.
     */
    public static final EntityType PAINTING = new EntityType("Painting", 83);

    /**
     * Represents a minecart.
     */
    public static final EntityType MINECART = new EntityType("Minecart", 84);

    /**
     * Represents a fireball.
     */
    public static final EntityType FIREBALL = new EntityType("Fireball", 85);

    /**
     * Represents a boat.
     */
    public static final EntityType BOAT = new EntityType("Boat", 90);

    /**
     * Represents an experience orb.
     */
    public static final EntityType EXPERIENCE_ORB = new EntityType("Experience Orb", -1);

    static Map<String, EntityType> typesByName = new HashMap<>();
    static Map<Integer, EntityType> typesById = new HashMap<>();
    static Set<EntityType> types = new HashSet<>();

    static {
        Arrays.asList(EntityType.class.getFields()).stream().filter(f -> f.getType() == EntityType
            .class).forEach(f -> {
            try {
                f.setAccessible(true);
                register((EntityType) f.get(null));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Gets the entity type bound to the given ID.
     *
     * @param id The ID to check against.
     * @return The bound type, or null if none exist.
     */
    public static EntityType getById(int id) {
        return typesById.get(id);
    }

    /**
     * Returns all registered entity types.
     *
     * @return The registered types.
     */
    public static Collection<EntityType> values() {
        return ImmutableSet.copyOf(types);
    }

    private final String name;
    private final int entityId;
    private Class<?> cls;
    private BiFunction<EntityType, Location, ? extends Entity> spawner;

    public EntityType(String name, int entityId) {
        this.name = Preconditions.checkNotNull(name, "name").toLowerCase();
        this.entityId = entityId;
    }

    /**
     * Gets the name of this entity type.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the internal implementation class of this entity type.
     *
     * @return The implementation class.
     */
    public Class<?> getImplementationClass() {
        return cls;
    }

    /**
     * Attempts to spawn an entity of this type at the given location. Fails if
     * no implementation has been set.
     *
     * @param location The location to spawn at.
     * @return The spawned entity if spawning succeeds.
     *
     * @see #setImplementation(Class, BiFunction)
     */
    public Entity spawn(Location location) {
        Preconditions.checkNotNull(location, "location");
        if (spawner == null) {
            throw new IllegalStateException("No spawner set for " + toString());
        }
        return spawner.apply(this, location);
    }

    @Override
    public String toString() {
        return "EntityType(name=" + Objects.toString(name) + ", id=" + Integer.toString(entityId)
            + ")";
    }

    /**
     * Sets the implementation of this type. Fails if an implementation has
     * already been set.
     *
     * @param cls     The implementation class.
     * @param spawner The spawning function.
     */
    public void setImplementation(Class<?> cls, BiFunction<EntityType, Location, ? extends Entity>
        spawner) {
        if (this.cls == null) {
            this.cls = Preconditions.checkNotNull(cls, "cls");
        } else {
            throw new IllegalStateException("implementation class is already set");
        }
        if (this.spawner == null) {
            this.spawner = Preconditions.checkNotNull(spawner, "spawner");
        } else {
            throw new IllegalStateException("implementation spawner is already set");
        }
    }

    /**
     * Gets whether or not this type has been registered.
     *
     * @return True iff this type has been registered.
     */
    public boolean isRegistered() {
        return types.contains(this);
    }

    /**
     * Gets whether or not this type is the bound type.
     *
     * @return True iff this type is the bound type.
     */
    public boolean isBoundType() {
        return typesById.get(getId()) == this;
    }

    /**
     * Gets the internal identification number of this entity type.
     *
     * @return The internal ID.
     */
    public int getId() {
        return entityId;
    }

    /**
     * Registers this entity type.
     *
     * @see #register(EntityType)
     */
    public EntityType register() {
        register(this);
        return this;
    }

    /**
     * Registers a new entity type, failing if the name of the given type is
     * already registered.
     *
     * @param type The new type to be registered.
     */
    public static void register(EntityType type) {
        Preconditions.checkNotNull(type, "type");
        if (typesByName.containsKey(type.name)) {
            throw new IllegalArgumentException("There already exists a type registered to name=" + type
                .name);
        }
        typesById.put(type.entityId, type);
        typesByName.put(type.name, type);
        types.add(type);
    }

    /**
     * Gets the bound entity type for this type's ID.
     *
     * @return The bound type.
     */
    public EntityType bound() {
        return get(this);
    }

    /**
     * Gets the entity type bound to the given type's ID. This method is used to
     * find overwritten entity types.
     *
     * @param type The type.
     * @return The bound type.
     */
    public static EntityType get(EntityType type) {
        return typesById.get(type.entityId);
    }

}

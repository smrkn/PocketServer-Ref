package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Equipable;
import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

/**
 * Represents a zombie, a {@link Hostile hostile}, {@link Equipable equipment-bearing} {@link
 * LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Zombie extends Hostile, Equipable {

    /**
     * Gets this zombie's type.
     *
     * @return The zombie's type.
     */
    ZombieType getZombieType();

    /**
     * Sets this zombie's type.
     *
     * @param type The new type.
     */
    void setZombieType(ZombieType type);

}

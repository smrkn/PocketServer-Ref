package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.entity.living.Neutral;

/**
 * Represents an iron golem, a {@link Neutral neutral} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface IronGolem extends Neutral {

}

package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.DyeColor;
import org.pocketserver.api.entity.living.Growing;
import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.living.Neutral;
import org.pocketserver.api.entity.living.Tameable;

/**
 * Represents a wolf, an {@link Growing ageable}, {@link Tameable tameable}, {@link Neutral
 * neutral}
 * {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Wolf extends Neutral, Tameable, Growing {

    /**
     * Gets the color of this wolf's collar, if it has been tamed.
     * <br>
     * Note: If the wolf is untamed, this method returns null.
     *
     * @return The collar color.
     */
    DyeColor getCollarColor();

    /**
     * Sets this wolf's collar color. Does nothing if untamed.
     *
     * @param color The new collar color.
     */
    void setCollarColor(DyeColor color);

}

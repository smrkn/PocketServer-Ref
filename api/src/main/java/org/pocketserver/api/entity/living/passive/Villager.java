package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Growing;
import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.living.Passive;

/**
 * Represents a villager (or a NPC), a {@link Passive passive} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Villager extends Passive, Growing {

}

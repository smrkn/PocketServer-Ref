package org.pocketserver.api.entity.vehicle;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.entity.Rideable;

/**
 * Represents a boat, a {@link Rideable rideable} {@link Vehicle vehicle}.
 *
 * @author Nick Robson
 */
public interface Boat extends Vehicle, Rideable {

    /**
     * Gets the second passenger of the boat, or null.
     *
     * @return The second passenger.
     */
    Entity getSecondaryPassenger();

    /**
     * Sets the second passenger of this boat.
     *
     * If the parameter is {@code null}, an existing passenger will be removed.
     * If a passenger exists, and the parameter is non-null, nothing is done.
     *
     * @param passenger The passenger to be set.
     */
    void setSecondaryPassenger(Entity passenger);

}

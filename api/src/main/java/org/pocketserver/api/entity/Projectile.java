package org.pocketserver.api.entity;

/**
 * Represents a projectile, an {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface Projectile extends Entity {

    /**
     * Gets who, or what, shot this projectile.
     *
     * @return The shooter.
     */
    ProjectileShooter getShooter();

}

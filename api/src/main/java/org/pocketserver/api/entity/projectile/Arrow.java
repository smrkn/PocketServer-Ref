package org.pocketserver.api.entity.projectile;

import org.pocketserver.api.entity.Projectile;

/**
 * Represents an arrow (such as those shot by {@link Skeleton skeleton}s), a {@link Projectile
 * projectile}.
 *
 * @author Nick Robson
 */
public interface Arrow extends Projectile {

}

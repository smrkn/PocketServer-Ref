package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;

/**
 * Represents a chicken, an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Chicken extends Animal {

}

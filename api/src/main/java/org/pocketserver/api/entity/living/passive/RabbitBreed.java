package org.pocketserver.api.entity.living.passive;

/**
 * Represents the different breeds of {@link Rabbit rabbit}s.
 *
 * @author Nick Robson
 */
public enum RabbitBreed {

    /**
     * Represents the brown skin variation.
     */
    BROWN(0x0),

    /**
     * Represents the white skin variation (albino).
     */
    WHITE(0x1),

    /**
     * Represents the black skin variation.
     */
    BLACK(0x2),

    /**
     * Represents the black and white spots skin variation.
     */
    SPOTS(0x3),

    /**
     * Represents the gold/cream skin variation.
     */
    GOLDEN(0x4),

    /**
     * Represents the salt & pepper skin variation.
     */
    SALT_PEPPER(0x5),

    /**
     * Represents the killer bunny rabbit type.
     */
    KILLER_BUNNY(0x63);

    private final int id;

    private RabbitBreed(int id) {
        this.id = id;
    }

    /**
     * Gets the internal identification number for this rabbit breed.
     *
     * @return The internal ID.
     */
    public int getId() {
        return id;
    }

}

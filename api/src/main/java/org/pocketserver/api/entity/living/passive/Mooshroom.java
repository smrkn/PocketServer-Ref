package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;

/**
 * Represents an mooshroom (also known as a mushroom cow), an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Mooshroom extends Cow {

}

package org.pocketserver.api.entity.block;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.entity.Entity;

/**
 * Represents a primed {@link Material#TNT} {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface PrimedTNT extends Entity {

    /**
     * Gets the number of ticks before this entity explodes.
     *
     * @return The remaining ticks.
     */
    int getExplodeTicks();

    /**
     * Sets the number of ticks before this entity explodes.
     *
     * @param ticks The new number of ticks.
     */
    void setExplodeTicks(int ticks);

}

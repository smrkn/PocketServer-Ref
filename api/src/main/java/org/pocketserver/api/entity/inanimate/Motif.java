package org.pocketserver.api.entity.inanimate;

/**
 * Represents the different motives that a {@link Painting painting} can represent.
 *
 * @author Nick Robson
 */
public enum Motif {

    KEBAB,
    AZTEC,
    ALBAN,
    AZTEC2,
    BOMB,
    PLANT,
    WASTELAND,
    WANDERER,
    GRAHAM,
    POOL,
    COURBET,
    SUNSET,
    SEA,
    CREEBET,
    MATCH,
    BUST,
    STAGE,
    VOID,
    SKULL_AND_ROSES,
    WITHER,
    FIGHTERS,
    SKELETON,
    DONKEY_KONG,
    POINTER,
    PIGSCENE,
    BURNING_SKULL,
    EARTH,
    WATER,
    FIRE,
    AIR;

    @Override
    public String toString() {
        String lower = name().charAt(0) + name().substring(1).toLowerCase();
        int under;
        while ((under = lower.indexOf('_')) >= 0) {
            lower = lower.substring(0, under) + Character.toUpperCase(lower.charAt(under + 1)) + lower
                .substring(under + 2);
        }
        return lower;
    }

}

package org.pocketserver.api.entity.inanimate;

import org.pocketserver.api.entity.Entity;

/**
 * Represents an experience orb, an {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface ExperienceOrb extends Entity {

    double getExperience();

    void setExperience(double experience);

}

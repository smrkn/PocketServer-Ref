package org.pocketserver.api.entity.living;

/**
 * Represents a passive {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Passive extends LivingEntity {

}

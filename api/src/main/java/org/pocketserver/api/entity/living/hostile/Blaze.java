package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Flying;
import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

/**
 * Represents a blaze, a {@link Flying flying} {@link Hostile hostile} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Blaze extends Hostile, Flying {

}

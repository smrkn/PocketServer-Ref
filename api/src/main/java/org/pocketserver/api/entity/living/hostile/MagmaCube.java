package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Hostile;

/**
 * Represents a magma cube, a {@link Hostile hostile} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface MagmaCube extends Slime {

}

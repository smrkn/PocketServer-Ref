package org.pocketserver.api.entity.living;

import java.util.UUID;

/**
 * Represents a tameable {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Tameable extends LivingEntity {

    /**
     * Gets whether or not this entity has been tamed.
     *
     * @return True iff this entity has been tamed.
     */
    boolean isTamed();

    /**
     * Sets whether this entity has been tamed.
     *
     * @param tamed Its new status as tamed or not.
     */
    void setTamed(boolean tamed);

    /**
     * Gets this entity's tamer.
     *
     * @return The tamer.
     */
    UUID getTamer();

    /**
     * Sets this entity's tamer.
     *
     * @param uuid The tamer's UUID.
     */
    void setTamer(UUID uuid);

    /**
     * Gets whether or not this entity is currently sitting.
     *
     * @return True iff this entity is sitting.
     */
    boolean isSitting();

    /**
     * Sets whether this entity is sitting.
     *
     * @param sitting Its new status as sitting or not.
     */
    void setSitting(boolean sitting);

}

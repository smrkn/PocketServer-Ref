package org.pocketserver.api.entity;

import org.pocketserver.api.world.locale.DirectionalLocation;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;

/**
 * Represents something that can shoot a {@link Projectile projectile}.
 *
 * @author Nick Robson
 */
public interface ProjectileShooter {

    /**
     * Fires a projectile from a given location at a given speed.
     *
     * @param clazz    The projectile class.
     * @param location The location to shoot from, including the direction.
     * @param speed    The speed to shoot at.
     * @return The projectile that was shot, or null if something went wrong.
     */
    default <T extends Projectile> T shoot(Class<T> clazz, DirectionalLocation location, double
        speed) {
        Vector dir = location.getDirection();
        dir = dir.normalize().multiply(speed);
        return shoot(clazz, location, dir);
    }

    /**
     * Fires a projectile from a given location at a given velocity.
     *
     * @param clazz    The projectile class.
     * @param location The location to shoot from.
     * @param velocity The velocity to shoot at.
     * @return The projectile that was shot, or null if something went wrong.
     */
    <T extends Projectile> T shoot(Class<T> clazz, Location location, Vector velocity);

}

package org.pocketserver.api.entity.block;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.entity.Entity;

/**
 * Represents a falling {@link Block block} {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface FallingBlock extends Entity {

    /**
     * Gets the {@link Block block} contained in this entity.
     *
     * @return The block.
     */
    Block getBlock();

    /**
     * Sets the {@link Block block} contained in this entity.
     *
     * @param block The new block.
     */
    void setBlock(Block block);

}

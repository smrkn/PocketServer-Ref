package org.pocketserver.api.entity.inanimate;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.inventory.ItemStack;

/**
 * Represents a dropped item, an {@link Entity entity}.
 *
 * @author Nick Robson
 */
public interface Item extends Entity {

    /**
     * Gets the {@link ItemStack} contained in this item.
     *
     * @return The contained item stack.
     */
    ItemStack getItemStack();

    /**
     * Sets the {@link ItemStack} contained in this item.
     *
     * @param item The new item stack.
     */
    void setItemStack(ItemStack item);

}

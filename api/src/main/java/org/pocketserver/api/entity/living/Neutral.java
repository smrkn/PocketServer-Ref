package org.pocketserver.api.entity.living;

/**
 * Represents a {@link Hostile hostile} {@link LivingEntity entity} that must be
 * {@link Angerable angered} before it begins attacking.
 *
 * @author Nick Robson
 */
public interface Neutral extends Hostile, Angerable {

}

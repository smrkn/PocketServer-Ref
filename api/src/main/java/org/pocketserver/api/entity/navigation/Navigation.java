package org.pocketserver.api.entity.navigation;

import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.world.locale.DirectionalLocation;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.api.world.locale.Vector;

/**
 * Represents an {@link LivingEntity entity}'s navigational capacity.
 *
 * @author Nick Robson
 */
public class Navigation {

    private LivingEntity entity;
    private NavigationTarget target;
    private double speed;

    public Navigation(LivingEntity entity, double speed) {
        this.entity = entity;
        this.speed = speed;
    }

    /**
     * Gets the current destination for the entity, or null if no destination.
     *
     * @return The destination.
     */
    public Location getDestination() {
        return target != null ? target.getTargetLocation() : null;
    }

    /**
     * Sets the destination for the entity.
     *
     * @param location The destination.
     */
    public void setDestination(Location location) {
        this.target = new NavigationPath(entity.getLocation(), location);
    }

    /**
     * Gets the entity's navigation speed.
     *
     * @return The speed.
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Sets the entity's navigation speed.
     *
     * @param speed The new speed.
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * Computes and moves to the next location in the path.
     *
     * @param travelDistance The maximum distance to travel.
     * @return The location the entity is moving/has moved to.
     */
    public Location move(double travelDistance) {
        Location next = getNextLocation(travelDistance);
        if (next != null) {
            entity.setLocation(next);
        }
        return next;
    }

    /**
     * Computes the next location in the path without moving there.
     *
     * @param travelDistance The maximum distance to travel.
     * @return The next location.
     */
    public Location getNextLocation(double travelDistance) {
        if (travelDistance < 0) {
            throw new IllegalArgumentException("cannot travel negative distance");
        }
        DirectionalLocation current = entity.getLocation();
        // If we have no target, then we shouldn't do anything.
        if (travelDistance == 0 || target == null) {
            return null;
        }
        // We're going to the next waypoint (or the final destination).
        Location to = target.getTargetLocation();
        if (to == null) {
            return null;
        }
        // Cache the vectors for later calculations.
        Vector cvec = current.toVector();
        Vector tvec = to.toVector();
        // Direction to `to` from `current`.
        Vector dir = tvec.subtract(cvec).normalize();
        // Displacement to move from `current`.
        Vector dist = dir.multiply(Math.pow(travelDistance, 1 / 3));
        // The destination after moving.
        Vector newloc = cvec.add(dist);
        // Distance of `newloc` to `to`. If this is -1*dir, we need to stop at the waypoint.
        Vector newdir = tvec.subtract(newloc).normalize();
        // If the difference between the dot products is below zero, we're past a 90 degree angle.
        if (newdir.dot(dir) < 0) {
            newloc = tvec;
        }
        // Vector => Location
        return newloc.toLocation(current.getWorld());
    }

    /**
     * Gets the current navigation target, or null if no path exists.
     *
     * @return The navigation target.
     */
    public NavigationTarget getTarget() {
        return target;
    }

    /**
     * Sets the current navigation target.
     *
     * @param target The new target.
     */
    public void setTarget(NavigationTarget target) {
        this.target = target;
    }

}

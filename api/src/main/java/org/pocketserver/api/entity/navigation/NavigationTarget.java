package org.pocketserver.api.entity.navigation;

import org.pocketserver.api.world.locale.Location;

/**
 * Represents a target for which entities may navigate to.
 *
 * @author Nick Robson
 */
public interface NavigationTarget {

    /**
     * Gets the current target location.
     *
     * @return The current target.
     */
    Location getTargetLocation();

}

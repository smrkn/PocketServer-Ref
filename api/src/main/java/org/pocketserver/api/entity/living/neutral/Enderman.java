package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.living.Neutral;
import org.pocketserver.api.inventory.ItemStack;

/**
 * Represents an enderman, a {@link Neutral neutral} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Enderman extends Neutral {

    /**
     * Gets the enderman's held block.
     *
     * @return The held block.
     */
    ItemStack getHeldBlock();

    /**
     * Sets the enderman's held block.
     *
     * @param item The new held block.
     */
    void setHeldBlock(ItemStack item);

}

package org.pocketserver.api.entity.projectile;

import org.pocketserver.api.entity.Projectile;

/**
 * Represents a fishing hook (as thrown from a fishing rod), a {@link Projectile projectile}.
 *
 * @author Nick Robson
 */
public interface FishingHook extends Projectile {

}

package org.pocketserver.api.entity.living;

/**
 * Represents an animal, a {@link Passive passive}, {@link Growing ageable} {@link LivingEntity
 * entity}.
 *
 * @author Nick Robson
 */
public interface Animal extends Passive, Growing {

}

package org.pocketserver.api.entity.living.passive;

/**
 * Represents the different breeds of {@link Ocelot ocelot}s.
 *
 * @author Nick Robson
 */
public enum OcelotBreed {

    /**
     * Represents an untamed ocelot.
     */
    UNTAMED(0x0),

    /**
     * Represents a tamed ocelot with a tuxedo coloring.
     */
    TUXEDO(0x1),

    /**
     * Represents a tamed ocelot with a tabby coloring.
     */
    TABBY(0x2),

    /**
     * Represents a tamed ocelot with a siamese colouring.
     */
    SIAMESE(0x3);

    private final int id;

    private OcelotBreed(int id) {
        this.id = id;
    }

    /**
     * Gets the internal identification number for this ocelot breed.
     *
     * @return The internal ID.
     */
    public int getId() {
        return id;
    }

}

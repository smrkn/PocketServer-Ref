package org.pocketserver.api.entity.living;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.inventory.ItemStack;

/**
 * Represents an {@link Equipable entity}'s equipment.
 *
 * @author Nick Robson
 */
public class EntityEquipment {

    private ItemStack held;
    private ItemStack[] armor;

    public EntityEquipment(ItemStack held, ItemStack[] armor) {
        setHeldItem(held);
        setArmor(armor);
    }

    /**
     * Gets the entity's held item.
     *
     * @return The held item.
     */
    public ItemStack getHeldItem() {
        return held;
    }

    /**
     * Sets the entity's held item.
     *
     * @param item The new held item.
     */
    public void setHeldItem(ItemStack item) {
        held = item != null ? item : new ItemStack(Material.AIR);
    }

    /**
     * Gets the entity's worn armor.
     *
     * @return The worn armor.
     */
    public ItemStack[] getArmor() {
        return ensure(armor);
    }

    /**
     * Sets the entity's worn armor.
     *
     * @param armor The new worn armor.
     */
    public void setArmor(ItemStack[] armor) {
        this.armor = ensure(armor);
    }

    private ItemStack[] ensure(ItemStack[] a) {
        if (a == null) {
            return new ItemStack[4];
        }
        if (a.length == 4) {
            return a;
        }
        ItemStack[] b = new ItemStack[4];
        for (int i = 0; i < 4; i++) {
            b[i] = i < a.length && a[i] != null ? a[i] : new ItemStack(Material.AIR);
        }
        return b;
    }

    /**
     * Gets the entity's worn helmet.
     *
     * @return The helmet.
     */
    public ItemStack getHelmet() {
        return ensure(armor)[0];
    }

    /**
     * Sets the entity's worn helmet.
     *
     * @param item The new helmet.
     */
    public void setHelmet(ItemStack item) {
        armor = ensure(armor);
        armor[0] = item;
    }

    /**
     * Gets the entity's worn chestplate.
     *
     * @return The chestplate.
     */
    public ItemStack getChestplate() {
        return ensure(armor)[1];
    }

    /**
     * Sets the entity's worn chestplate.
     *
     * @param item The new chestplate.
     */
    public void setChestplate(ItemStack item) {
        armor = ensure(armor);
        armor[1] = item;
    }

    /**
     * Gets the entity's worn leggings.
     *
     * @return The leggings.
     */
    public ItemStack getLeggings() {
        return ensure(armor)[2];
    }

    /**
     * Sets the entity's worn leggings.
     *
     * @param item The new leggings.
     */
    public void setLeggings(ItemStack item) {
        armor = ensure(armor);
        armor[2] = item;
    }

    /**
     * Gets the entity's worn boots.
     *
     * @return The boots.
     */
    public ItemStack getBoots() {
        return ensure(armor)[3];
    }

    /**
     * Sets the entity's worn boots.
     *
     * @param item The new boots.
     */
    public void setBoots(ItemStack item) {
        armor = ensure(armor);
        armor[3] = item;
    }

}

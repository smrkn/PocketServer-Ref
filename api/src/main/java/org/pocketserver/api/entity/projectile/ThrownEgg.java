package org.pocketserver.api.entity.projectile;

import org.pocketserver.api.entity.Projectile;

/**
 * Represents a thrown egg, a {@link Projectile projectile}.
 *
 * @author Nick Robson
 */
public interface ThrownEgg extends Projectile {

}

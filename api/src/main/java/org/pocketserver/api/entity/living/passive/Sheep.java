package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.DyeColor;
import org.pocketserver.api.entity.living.Animal;

/**
 * Represents a squid, an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Sheep extends Animal {

    /**
     * Gets the sheep's current wool color.
     *
     * @return The wool color.
     */
    DyeColor getColor();

    /**
     * Sets the sheep's wool color.
     *
     * @param color The color.
     */
    void setColor(DyeColor color);

    /**
     * Gets whether or not this sheep has been shaved.
     *
     * @return True iff this sheep has been shaved.
     */
    boolean isShaved();

    /**
     * Sets whether this sheep has been shaved.
     *
     * @param shaved Its new state as shaved or not.
     */
    void setShaved(boolean shaved);

}

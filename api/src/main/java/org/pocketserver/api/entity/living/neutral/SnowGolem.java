package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.entity.living.Neutral;

/**
 * Represents a snow golem (also known as a snowman), a {@link Neutral neutral} {@link LivingEntity
 * entity}.
 *
 * @author Nick Robson
 */
public interface SnowGolem extends Neutral {

}

package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.entity.living.Equipable;
import org.pocketserver.api.entity.living.LivingEntity;
import org.pocketserver.api.entity.living.Neutral;

/**
 * Represents a pig zombie, a {@link Neutral hostile}, {@link Equipable equipment-bearing} {@link
 * LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface PigZombie extends Neutral, Equipable {

}

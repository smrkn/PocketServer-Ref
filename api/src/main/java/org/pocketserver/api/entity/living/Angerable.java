package org.pocketserver.api.entity.living;

/**
 * Represents an angerable {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Angerable extends Hostile {

    /**
     * Gets whether or not this entity is currently angered.
     *
     * @return True iff this entity is angered.
     */
    boolean isAngered();

    /**
     * Sets whether this entity is angered.
     *
     * @param angered Its new state as angered or not.
     */
    void setAngered(boolean angered);

}

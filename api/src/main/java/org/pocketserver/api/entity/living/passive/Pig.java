package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;

/**
 * Represents a pig, an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Pig extends Animal {

}

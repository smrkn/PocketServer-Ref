package org.pocketserver.api.entity.living.neutral;

import org.pocketserver.api.entity.living.Angerable;
import org.pocketserver.api.entity.living.Neutral;
import org.pocketserver.api.entity.living.hostile.Spider;

/**
 * Represents a cave spider, a {@link Neutral neutral} {@link Spider spider} that is
 * {@link Angerable angered} by light levels less than 9.
 *
 * @author Nick Robson
 */
public interface CaveSpider extends Spider, Neutral {

}

package org.pocketserver.api.entity.living;

/**
 * Represents a hostile {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Hostile extends LivingEntity {

    /**
     * Gets who, or what, this entity is currently targetting.
     *
     * @return The current target.
     */
    LivingEntity getHostileTarget();

    /**
     * Sets who, or what, this entity is currently targetting.
     *
     * @param entity The new target.
     */
    void setHostileTarget(LivingEntity entity);

}

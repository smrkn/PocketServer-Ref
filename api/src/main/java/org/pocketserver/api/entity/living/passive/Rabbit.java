package org.pocketserver.api.entity.living.passive;

import org.pocketserver.api.entity.living.Animal;

/**
 * Represents a rabbit, an {@link Animal animal}.
 *
 * @author Nick Robson
 */
public interface Rabbit extends Animal {

    /**
     * Gets this rabbit's breed.
     *
     * @return The breed.
     */
    RabbitBreed getBreed();

    /**
     * Sets this rabbit's breed.
     *
     * @param breed The new breed.
     */
    void setBreed(RabbitBreed breed);

}

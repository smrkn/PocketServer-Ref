package org.pocketserver.api.entity.vehicle;

import org.pocketserver.api.entity.Rideable;

/**
 * Represents a minecart, a {@link Rideable rideable} {@link Vehicle vehicle}.
 *
 * @author Nick Robson
 */
public interface Minecart extends Vehicle, Rideable {

}

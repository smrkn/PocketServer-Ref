package org.pocketserver.api.entity.living;

/**
 * Represents a {@link LivingEntity entity} that can be given equipment.
 *
 * @author Nick Robson
 */
public interface Equipable extends LivingEntity {

    /**
     * Gets this entity's equipment.
     *
     * @return The equipment.
     */
    EntityEquipment getEquipment();

    /**
     * Sets this entity's equipment.
     *
     * @param equipment The new equipment.
     */
    void setEquipment(EntityEquipment equipment);

}

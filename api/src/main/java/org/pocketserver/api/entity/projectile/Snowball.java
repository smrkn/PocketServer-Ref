package org.pocketserver.api.entity.projectile;

import org.pocketserver.api.entity.Projectile;

/**
 * Represents a snowball, a {@link Projectile projectile}.
 *
 * @author Nick Robson
 */
public interface Snowball extends Projectile {

}

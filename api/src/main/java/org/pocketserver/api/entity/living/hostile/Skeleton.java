package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.ProjectileShooter;
import org.pocketserver.api.entity.living.Equipable;
import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

/**
 * Represents a skeleton, a {@link Hostile hostile}, {@link Equipable equipment-bearing} {@link
 * LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Skeleton extends Hostile, Equipable, ProjectileShooter {

    /**
     * Gets this skeleton's type.
     *
     * @return The skeleton's type.
     */
    SkeletonType getSkeletonType();

    /**
     * Sets this skeleton's type.
     *
     * @param type The new type.
     */
    void setSkeletonType(SkeletonType type);

}

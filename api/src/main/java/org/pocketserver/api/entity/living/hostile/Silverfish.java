package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Hostile;

/**
 * Represents a silverfish, a {@link Hostile hostile} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Silverfish extends Hostile {

}

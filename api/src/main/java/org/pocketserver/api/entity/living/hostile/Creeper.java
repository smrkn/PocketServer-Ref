package org.pocketserver.api.entity.living.hostile;

import org.pocketserver.api.entity.living.Hostile;
import org.pocketserver.api.entity.living.LivingEntity;

/**
 * Represents a creeper, a {@link Hostile hostile} {@link LivingEntity entity}.
 *
 * @author Nick Robson
 */
public interface Creeper extends Hostile {

    /**
     * Gets whether or not this creeper is charged.
     *
     * @return True iff this creeper is charged.
     */
    boolean isCharged();

    /**
     * Sets whether this creeper is charged.
     *
     * @param charged Its new charged state.
     */
    void setCharged(boolean charged);

}
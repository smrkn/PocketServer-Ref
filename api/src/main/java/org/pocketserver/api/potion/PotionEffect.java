package org.pocketserver.api.potion;

public class PotionEffect {

    private PotionEffectType type;
    private int duration;
    private int amplifier;
    private boolean showParticles;

    public PotionEffect(PotionEffectType type, int duration) {
        this(type, duration, 0);
    }

    public PotionEffect(PotionEffectType type, int duration, int amplifier) {
        this(type, duration, amplifier, true);
    }

    public PotionEffect(PotionEffectType type, int duration, int amplifier, boolean showParticles) {
        this.type = type;
        this.duration = duration;
        this.amplifier = amplifier;
        this.showParticles = showParticles;
    }

    public PotionEffectType getType() {
        return type;
    }

    public void setType(PotionEffectType type) {
        this.type = type;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getAmplifier() {
        return amplifier;
    }

    public void setAmplifier(int amplifier) {
        this.amplifier = amplifier;
    }

    public boolean isShowParticles() {
        return showParticles;
    }

    public void setShowParticles(boolean showParticles) {
        this.showParticles = showParticles;
    }

    public enum PotionEffectType {
        //TODO: Let's assume the ids are the same as in PC
        SPEED(1),
        SLOWNESS(2),
        HASTE(3),
        MINING_FATIGUE(4),
        STRENGTH(5),
        INSTANT_HEALTH(6),
        INSTANT_DAMAGE(7),
        JUMP_BOOST(8),
        NAUSEA(9),
        REGENERATION(10),
        RESISTANCE(11),
        FIRE_RESISTANCE(12),
        WATER_BREATHING(13),
        INVISIBILITY(14),
        BLINDNESS(15),
        NIGHT_VISION(16),
        HUNGER(17),
        WEAKNESS(18),
        POISON(19),
        WITHER(20),
        HEALTH_BOOST(21),
        ABSORPTION(22),
        SATURATION(23);

        private final int id;

        PotionEffectType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}

package org.pocketserver.api.command;

import org.pocketserver.api.ChatColor;
import org.pocketserver.api.player.Player;

import com.google.common.base.Preconditions;
import com.google.common.collect.MapMaker;

import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;

@SuppressWarnings("unchecked")
public abstract class Command {

    private static final Map<Class<?>, BiFunction<CommandInvocationContext, String, ?>>
        mappingFunctions = new MapMaker().makeMap();

    static {
        registerMapping(Player.class, (ctx, s) -> ctx.getExecutor().getServer().getPlayer(s).stream().findAny().get());
        registerMapping(Double.class, (ctx, s) -> Double.parseDouble(s));
        registerMapping(Integer.class, (ctx, s) -> Integer.parseInt(s));
        registerMapping(Short.class, (ctx, s) -> Short.parseShort(s));
        registerMapping(Long.class, (ctx, s) -> Long.parseLong(s));
    }

    public static <T> void registerMapping(Class<T> clazz, BiFunction<CommandInvocationContext,
        String, T> mappingFunction) {
        Preconditions.checkNotNull(mappingFunction, "mappingFunction should not be null");
        Preconditions.checkNotNull(clazz, "clazz should not be null");
        mappingFunctions.putIfAbsent(clazz, mappingFunction);
    }

    public static <T> void unregisterMapping(Class<T> clazz) {
        mappingFunctions.remove(Preconditions.checkNotNull(clazz, "clazz should not be null"));
    }

    public static <T> BiFunction<CommandInvocationContext, String, T> getMappingFunction(Class<T>
                                                                                             clazz) {
        return (BiFunction<CommandInvocationContext, String, T>) mappingFunctions.getOrDefault(clazz,
            (e, s) -> {
                throw new IllegalArgumentException(
                    String
                        .format(
                            "mapping function has not been "
                                +
                                "registered "
                                +
                                "for %s",
                            clazz
                                .getName()));
            });
    }

    private final String[] aliases;
    private final String permission;
    private final String name;

    protected Command(String name) {
        this(name, "");
    }

    protected Command(String name, String permission, String... aliases) {
        Preconditions.checkNotNull(name, "name should not be null");
        Preconditions.checkArgument(name.length() > 0, "name should not be empty");
        this.aliases = Arrays.stream(aliases).filter(a -> a != null && !a.isEmpty()).toArray
            (String[]::new);
        this.permission = permission == null ? "" : permission.replace(" ", "");
        this.name = name.toLowerCase();
    }

    /**
     * @return immutable collection of aliases that may be used to invoke the command
     */
    public final String[] getAliases() {
        return Arrays.copyOf(aliases, aliases.length);
    }

    /**
     * Method that processes execution of the command.
     *
     * @param ctx object that describes the command execution.
     */
    public abstract void execute(CommandInvocationContext ctx);

    /**
     * @return name used to register the command.
     */
    public final String getName() {
        return name;
    }

    /**
     * Convenience method for verbosely testing if a {@link CommandExecutor} has a permission or
     * not.
     * Effectively the same as {@code testPermission(executor, getPermission())}. This method is
     * called prior to command execution so you do <b>not</b> need to run it first.
     *
     * @param executor executor to test for permission
     * @return {@code true} if thet executor has the permission node returned by {@link
     * Command#getPermission()}
     *
     * @see Command#testPermission(CommandExecutor, String)
     */
    protected final boolean testPermission(CommandExecutor executor) {
        return testPermission(executor, getPermission());
    }

    /**
     * Convenience method for verbosely testing if a {@link CommandExecutor} has a permission or
     * not.
     *
     * @param executor   executor to test for permission
     * @param permission permission node to test for
     * @return {@code true} if the executor has the given permission node
     */
    protected final boolean testPermission(CommandExecutor executor, String permission) {
        if (executor.hasPermission(permission)) {
            return true;
        } else {
            executor.sendMessage(ChatColor.RED + "You do not have permission to do that!");
            return false;
        }
    }

    /**
     * @return permission node required to execute the command
     */
    public final String getPermission() {
        return this.permission;
    }
}

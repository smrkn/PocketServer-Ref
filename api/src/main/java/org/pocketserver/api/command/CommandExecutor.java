package org.pocketserver.api.command;

import org.pocketserver.api.Server;
import org.pocketserver.api.permissions.Permissible;

import java.text.MessageFormat;

/**
 * The interface for all entities that are able to execute commands. They extend
 * {@link Permissible} due to Permissible being closely related with permissions for commands
 * and other things that only humans or console would have access to.
 *
 * @author TheLightMC
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public interface CommandExecutor extends Permissible {

    /**
     * Retrieve the name of the executor instance.
     */
    String getName();

    /**
     * Send a message to an executor with formatting.
     *
     * @param format {@link MessageFormat} compliant format string.
     * @param params array of parameters.
     */
    default void sendMessage(String format, Object... params) {
        sendMessage(MessageFormat.format(format, params));
    }

    /**
     * Sends a message to an executor.
     *
     * @param message message to send.
     */
    void sendMessage(String message);

    /**
     * Retrieve the server instance associated with this {@link CommandExecutor}.
     */
    Server getServer();
}

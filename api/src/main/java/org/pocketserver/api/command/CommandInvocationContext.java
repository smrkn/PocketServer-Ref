package org.pocketserver.api.command;

public interface CommandInvocationContext {

    /**
     * Retrieve the {@link Command} that this context was created from.
     */
    Command getCommand();

    /**
     * Retrieve the {@link CommandExecutor} that triggered execution.
     */
    CommandExecutor getExecutor();

    /**
     * Retrieve the number of arguments.
     */
    default int size() {
        return getArguments().length;
    }

    /**
     * Retrieve the array of strings passed to the command.
     */
    String[] getArguments();

    /**
     * Retrieve the n-th argument and advance the cursor by one.
     * <p>
     * Equivalent of invoking {@link CommandInvocationContext#getArgument(Class)} where the
     * parameter is equal to {@code String.class}.
     */
    default String getArgument() {
        return getArgument(String.class);
    }

    /**
     * Retrieve the n-th argument, attempt to map the argument to the requested type and advance
     * the cursor by one.
     *
     * @param clazz type to map the argument to.
     * @param <T>   type to map the argument to.
     * @return an instance of {@code T}.
     */
    <T> T getArgument(Class<? extends T> clazz);

    /**
     * Retrieve the n-th argument at a given index.
     *
     * @param index index to read from
     */
    default String getArgument(int index) {
        return getArgument(index, String.class);
    }

    /**
     * Retrieve the argument at the n-th index and attempt to map it to an instance of {@code T}.
     *
     * @param index index to read from.
     * @param clazz type to map the argument to.
     * @param <T>   type to map the argument to.
     * @return an instance of {@code T}.
     *
     * @throws IllegalArgumentException thrown when one of the following conditions are met:
     *                                  <ul>
     *                                  <li>the given {@code index} is out of bounds</li>
     *                                  <li>the mapping function returned an object of the wrong
     *                                  type</li>
     *                                  <li>the mapping function threw an unhandled exception</li>
     *                                  </ul>
     */
    <T> T getArgument(int index, Class<? extends T> clazz);
}

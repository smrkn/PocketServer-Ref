package org.pocketserver.api;

import org.pocketserver.api.command.ConsoleCommandExecutor;
import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.plugin.PluginManager;
import org.pocketserver.api.util.Pipeline;

import com.google.common.base.Preconditions;

import org.pocketserver.api.world.World;
import org.slf4j.Logger;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class Server {

    private static Server server;

    public static Server getServer() {
        Preconditions.checkState(Server.server != null, "Server singleton has not been defined - " +
            "improper API usage?");
        return server;
    }

    public static void setServer(Server server) {
        Preconditions.checkState(Server.server == null, "cannot redefine Server singleton!");
        Server.server = server;
    }

    public abstract void shutdown();

    public abstract PluginManager getPluginManager();

    public abstract Logger getLogger();

    public abstract File getDirectory();

    public abstract Pipeline<PermissionResolver> getPermissionPipeline();

    // TODO: Implement Settings interface

    public abstract Collection<? extends Player> getPlayer(String username);

    public abstract List<? extends Player> getPlayers(Predicate<Player> filter);

    public abstract Collection<? extends Player> getOnlinePlayers();

    public abstract ConsoleCommandExecutor getConsole();

    public abstract void broadcastMessage(String message);

    public abstract void broadcastMessage(String message, Predicate<Player> filter);

    public abstract World getDefaultWorld();
}

package org.pocketserver.api.event.entity.vehicle;

import org.pocketserver.api.entity.vehicle.Vehicle;

/**
 * This event is called when an entity gets mounted onto a vehicle.
 */
public class VehicleMountEvent extends VehicleEvent {

    public VehicleMountEvent(Vehicle vehicle) {
        super(vehicle);
    }
}

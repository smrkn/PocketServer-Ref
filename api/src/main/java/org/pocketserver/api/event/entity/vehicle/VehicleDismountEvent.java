package org.pocketserver.api.event.entity.vehicle;

import org.pocketserver.api.entity.vehicle.Vehicle;

/**
 * This event is called when an entity dismounts from a vehicle.
 */
public class VehicleDismountEvent extends VehicleEvent {

    public VehicleDismountEvent(Vehicle vehicle) {
        super(vehicle);
    }
}

package org.pocketserver.api.event.server;

import com.google.common.base.Preconditions;
import org.pocketserver.api.event.Event;

import java.util.regex.Pattern;

/**
 * This event is called when the server gets pinged.
 */
public class ServerPingEvent extends Event {
    private static final Pattern VERSION_STRING = Pattern.compile("[0-9.]+");

    private String motd;
    private String versionNumber;
    private int protocolVersion;
    private int playerCount;
    private int maxPlayers;

    public ServerPingEvent(String motd, String versionNumber, int protocolVersion,
                           int playerCount, int maxPlayers) {
        this.motd = motd;
        this.versionNumber = versionNumber;
        this.protocolVersion = protocolVersion;
        this.playerCount = playerCount;
        this.maxPlayers = maxPlayers;
    }

    public String getMotd() {
        return motd;
    }

    public void setMotd(String motd) {
        this.motd = motd == null ? "" : motd;
    }

    public int getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(int protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        Preconditions.checkArgument(VERSION_STRING.matcher(versionNumber).matches());
        this.versionNumber = versionNumber;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }
}

package org.pocketserver.api.event.entity;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.world.locale.Location;

/**
 * This event is called when an entity teleports.
 */
public class EntityTeleportEvent extends EntityEvent implements Cancellable {

    private final Location from;
    private boolean cancelled;
    private Location to;

    public EntityTeleportEvent(Entity entity, Location from, Location to) {
        super(entity);
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }
}

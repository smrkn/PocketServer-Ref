package org.pocketserver.api.event.entity;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.event.Event;

public class EntityEvent extends Event {

    private final Entity entity;

    public EntityEvent(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}

package org.pocketserver.api.event;

import org.pocketserver.api.plugin.PluginManager;

import com.google.common.base.Preconditions;

/**
 * Utility class for ensuring events are handled properly.
 *
 * @deprecated unless you are writing a {@link PluginManager}
 * implementation
 * then you probably shouldn't be touching this.
 */
public final class Events {

    /**
     * Utility method for triggering possible callbacks.
     */
    public static void done(Event event) {
        Preconditions.checkNotNull(event, "event");
        event.done();
    }

    public static void prepare(PluginManager pluginManager, Event event) {
        if (event.getLeak() != null) {
            event.getLeak().record(pluginManager);
        }
    }

    public static void record(Event event, Object hint) {
        Preconditions.checkNotNull(event, "event").getLeak().record(hint);
    }

    private Events() {
        throw new UnsupportedOperationException("Events cannot be instantiated!");
    }
}

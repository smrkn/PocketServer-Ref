package org.pocketserver.api.event.player;

import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.player.Player;

import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Set;

/**
 * This event is called when a player sends a chat message.
 */
public class PlayerChatEvent extends PlayerEvent implements Cancellable {

    private final Set<Player> recipients;
    private boolean cancelled;
    private String message;

    public PlayerChatEvent(Player player, String message) {
        super(player);
        this.recipients = Sets.newHashSet(player.getServer().getOnlinePlayers());
        this.message = message;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Collection<Player> getRecipients() {
        return recipients;
    }
}

package org.pocketserver.api.event.entity;

import org.pocketserver.api.entity.Entity;
import org.pocketserver.api.world.locale.Location;

/**
 * This event is called when an entity moves.
 */
public class EntityMoveEvent extends EntityEvent {

    private final Location from;
    private Location to;

    public EntityMoveEvent(Entity entity, Location to, Location from) {
        super(entity);
        this.to = to;
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }

    public Location getFrom() {
        return from;
    }
}

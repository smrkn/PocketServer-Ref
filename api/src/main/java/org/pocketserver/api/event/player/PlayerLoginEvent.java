package org.pocketserver.api.event.player;

import org.pocketserver.api.ChatColor;
import org.pocketserver.api.event.AsyncEvent;
import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.util.Callback;

/**
 * This event is called when a player attempts to connect with the server.
 */
public class PlayerLoginEvent extends AsyncEvent<PlayerLoginEvent> implements Cancellable {

    private String kickMessage;
    private boolean cancelled;

    public PlayerLoginEvent(Callback<PlayerLoginEvent> callback) {
        super(callback);
        this.kickMessage = ChatColor.RED + "You are not allowed to join this server!";
    }

    public String getKickMessage() {
        return kickMessage;
    }

    public void setKickMessage(String kickMessage) {
        if ((kickMessage == null || kickMessage.length() < 1) && isCancelled()) {
            setCancelled(false);
            return;
        }
        this.kickMessage = kickMessage;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

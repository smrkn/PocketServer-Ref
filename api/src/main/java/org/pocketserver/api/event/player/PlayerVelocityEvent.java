package org.pocketserver.api.event.player;

import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.locale.Vector;

/**
 * This event is called when the velocity from a player changes.
 */
public class PlayerVelocityEvent extends PlayerEvent implements Cancellable {

    private boolean cancelled;
    private Vector velocity;

    public PlayerVelocityEvent(Player player, Vector vector) {
        super(player);
        this.velocity = vector;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }
}

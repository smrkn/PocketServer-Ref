package org.pocketserver.api.event.player;

import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.player.GameMode;
import org.pocketserver.api.player.Player;

/**
 * This event is called when the gamemode from a player changes.
 */
public class PlayerGameModeChangeEvent extends PlayerEvent implements Cancellable {

    private boolean cancelled;
    private GameMode gameMode;

    public PlayerGameModeChangeEvent(Player player, GameMode gameMode) {
        super(player);
        this.gameMode = gameMode;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public GameMode getGameMode() {
        return gameMode;
    }
}

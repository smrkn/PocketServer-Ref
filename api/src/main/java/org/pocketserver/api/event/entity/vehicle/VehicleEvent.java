package org.pocketserver.api.event.entity.vehicle;

import org.pocketserver.api.entity.vehicle.Vehicle;
import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.event.Event;

public class VehicleEvent extends Event implements Cancellable {

    private final Vehicle vehicle;
    private boolean cancelled;

    public VehicleEvent(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }
}

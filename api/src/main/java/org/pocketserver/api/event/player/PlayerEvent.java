package org.pocketserver.api.event.player;

import org.pocketserver.api.event.entity.EntityEvent;
import org.pocketserver.api.player.Player;

public class PlayerEvent extends EntityEvent {

    public PlayerEvent(Player player) {
        super(player);
    }

    public Player getPlayer() {
        return (Player) getEntity();
    }
}

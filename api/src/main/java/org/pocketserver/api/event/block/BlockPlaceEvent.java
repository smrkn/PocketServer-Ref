package org.pocketserver.api.event.block;

import org.pocketserver.api.block.Block;
import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.event.player.PlayerEvent;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.locale.Location;

/**
 * This event is called when a block gets placed by a player.
 */
public class BlockPlaceEvent extends PlayerEvent implements Cancellable {

    private final Block block;
    private boolean cancelled;

    public BlockPlaceEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

    public Location getLocation() {
        return block.getLocation();
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

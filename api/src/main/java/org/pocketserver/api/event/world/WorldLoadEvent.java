package org.pocketserver.api.event.world;

import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.event.Event;
import org.pocketserver.api.world.provider.WorldProvider;

public class WorldLoadEvent extends Event implements Cancellable {
    private WorldProvider provider;
    private boolean cancelled;

    public WorldLoadEvent(WorldProvider provider) {
        this.provider = provider;
    }

    public void setProvider(WorldProvider provider) {
        this.provider = provider;
    }

    public WorldProvider getProvider() {
        return provider;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

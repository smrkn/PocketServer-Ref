package org.pocketserver.api.event.player;

import org.pocketserver.api.event.Cancellable;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.locale.Location;

/**
 * This event is called when a player moves.
 */
public class PlayerMoveEvent extends PlayerEvent implements Cancellable {

    private final Location from;
    private boolean cancelled;
    private Location to;

    public PlayerMoveEvent(Player player, Location from, Location to) {
        super(player);
        this.from = from;
        this.to = to;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }

    public Location getFrom() {
        return from;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

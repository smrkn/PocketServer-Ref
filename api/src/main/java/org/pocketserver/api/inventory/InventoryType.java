package org.pocketserver.api.inventory;

public enum InventoryType {
    //TODO: Make sure these values are correct
    CHEST(0, 27),
    DOUBLE_CHEST(1, 54),
    PLAYER(2, 36),
    FURNACE(3, 3),
    CRAFTING(4, 4),
    WORKBENCH(5, 9),
    STONECUTTER(6, 4),
    BREWING_STAND(7, 4),
    ANVIL(8, 3),
    ENCHANTMENT_TABLE(9, 2);

    private final int id;
    private final int size;

    InventoryType(int id, int size) {
        this.id = id;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public int getSize() {
        return size;
    }
}

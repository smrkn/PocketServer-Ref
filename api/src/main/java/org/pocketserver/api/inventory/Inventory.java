package org.pocketserver.api.inventory;

/**
 * An inventory holds ItemStacks
 *
 * @author j0ach1mmall3
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public interface Inventory {

    InventoryType getType();

    int getSize();

    ItemStack[] getContents();

    void setContents(ItemStack... contents);

    ItemStack getItem(int position);

    void setItem(int position, ItemStack item);
}

package org.pocketserver.api.inventory.meta;

import org.pocketserver.api.block.Material;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Collections;

/**
 * All the Enchantments
 *
 * @author j0ach1mmall3
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public enum Enchantment {
    PROTECTION(0, 4, Material.Armor.ALL_ARMOR),
    FIRE_PROTECTION(1, 4, Material.Armor.ALL_ARMOR),
    FEATHER_FALLING(2, 4, Material.Armor.BOOTS),
    BLAST_PROTECTION(3, 4, Material.Armor.ALL_ARMOR),
    PROJECTILE_PROTECTION(4, 4, Material.Armor.ALL_ARMOR),
    RESPIRATION(5, 3, Material.Armor.HELMETS),
    AQUA_AFFINITY(6, 1, Material.Armor.HELMETS),
    THORNS(7, 3, Material.Armor.ALL_ARMOR),
    DEPTH_STRIDER(8, 3, Material.Armor.BOOTS),

    SHARPNESS(16, 5, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS, Material.Tools
        .AXES))),
    SMITE(17, 5, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS, Material.Tools.AXES))),
    BANE_OF_THE_ARTHROPODS(18, 5, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS,
        Material.Tools.AXES))),
    KNOCKBACK(19, 2, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS, Material.Tools
        .AXES))),
    FIRE_ASPECT(20, 2, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS, Material.Tools
        .AXES))),
    LOOTING(21, 3, Sets.immutableEnumSet(Iterables.concat(Material.Tools.SWORDS, Material.Tools
        .AXES))),

    EFFICIENCY(32, 5, Material.Tools.YIELDING),
    SILK_TOUCH(33, 1, Material.Tools.YIELDING),
    UNBREAKING(34, 3, Sets.immutableEnumSet(Iterables.concat(Material.Tools.ALL_TOOLS, Material
        .Armor.ALL_ARMOR))),
    FORTUNE(35, 3, Sets.immutableEnumSet(Iterables.concat(Material.Tools.PICKAXES, Material.Tools
        .AXES, Material.Tools.SHOVELS))),

    POWER(48, 5, Material.BOW),
    PUNCH(49, 2, Material.BOW),
    FLAME(50, 1, Material.BOW),
    INFINITY(51, 1, Material.BOW),

    LUCK_OF_THE_SEA(61, 3, Material.FISHING_ROD),
    LURE(62, 3, Material.FISHING_ROD);

    private final int id;
    private final int maxLevel;
    private final Collection<Material> targets;

    Enchantment(int id, int maxLevel, Material target) {
        this(id, maxLevel, Collections.singleton(target));
    }

    Enchantment(int id, int maxLevel, Collection<Material> targets) {
        this.id = id;
        this.maxLevel = maxLevel;
        this.targets = targets;
    }

    public int getId() {
        return id;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public Collection<Material> getTargets() {
        return targets;
    }
}
package org.pocketserver.api.inventory;

import org.pocketserver.api.block.Material;
import org.pocketserver.api.inventory.meta.ItemMeta;

import com.google.common.base.Preconditions;

/**
 * An ItemStack is a stack of items with a specified Material, durability, amount and ItemMeta
 *
 * @author j0ach1mmall3
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public class ItemStack {

    private Material material;
    private int durability, amount;
    private ItemMeta meta;

    public ItemStack(Material material) {
        this(material, 1);
    }

    public ItemStack(Material material, int amount) {
        this(material, amount, 0);
    }

    public ItemStack(Material material, int amount, int durability) {
        this.material = material;
        this.amount = amount;
        this.durability = durability;
        this.meta = null; // TODO: ItemMeta
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public ItemMeta getItemMeta() {
        return meta;
    }

    public void setItemMeta(ItemMeta meta) {
        this.meta = Preconditions.checkNotNull(meta, "meta");
    }
}

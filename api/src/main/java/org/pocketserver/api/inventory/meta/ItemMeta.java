package org.pocketserver.api.inventory.meta;

import java.util.List;
import java.util.Optional;

/**
 * ItemMeta specifies additional properties of an ItemStack.
 *
 * @author j0ach1mmall3
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public interface ItemMeta {

    Optional<String> getDisplayName();

    void setDisplayName(String name);

    Optional<List<String>> getLore();

    void setLore(List<String> lore);

    void addEnchantment(Enchantment enchantment, int level);

    void setEnchantmentLevel(Enchantment enchantment, int level);

    int getEnchantmentLevel(Enchantment enchantment);

    void removeEnchantment(Enchantment enchantment);

    boolean hasEnchantment(Enchantment enchantment);

    void addUnsafeEnchantment(Enchantment enchantment, int level);

    void setUnsafeEnchantmentLevel(Enchantment enchantment, int level);
}

package org.pocketserver.api.inventory;

/**
 * An InventoryHolder holds an Inventory
 *
 * @author j0ach1mmall3
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */
public interface InventoryHolder {

    Inventory getInventory();
}

package org.pocketserver.api.player;

import org.pocketserver.api.command.CommandExecutor;
import org.pocketserver.api.entity.living.Equipable;
import org.pocketserver.api.entity.living.Flying;
import org.pocketserver.api.inventory.InventoryHolder;

import java.net.InetSocketAddress;

public interface Player extends Flying, Equipable, InventoryHolder, CommandExecutor {

    void chat(String message);

    GameMode getGamemode();

    void setGamemode(GameMode mode);

    InetSocketAddress getAddress();

    void sendTip(String message);

    void sendPopup(String message);
}

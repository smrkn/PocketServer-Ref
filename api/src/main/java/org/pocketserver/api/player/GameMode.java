package org.pocketserver.api.player;

public enum GameMode {
    CREATIVE(0),
    SURVIVAL(1);

    private final int id;

    GameMode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
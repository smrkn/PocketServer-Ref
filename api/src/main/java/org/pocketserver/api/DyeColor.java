package org.pocketserver.api;

public enum DyeColor {

    BLACK(0x0),
    RED(0x1),
    GREEN(0x2),
    BROWN(0x3),
    DARK_BLUE(0x4),
    PURPLE(0x5),
    CYAN(0x6),
    LIGHT_GRAY(0x7),
    DARK_GRAY(0x8),
    PINK(0x9),
    LIME(0xA),
    YELLOW(0xB),
    LIGHT_BLUE(0xC),
    MAGENTA(0xD),
    ORANGE(0xE),
    WHITE(0xF);

    private final int id;

    private DyeColor(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}

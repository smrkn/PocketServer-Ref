package org.pocketserver.api.plugin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Erik Rosemberg
 * @since 25.12.2015
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PocketPlugin {

    /**
     * Retrieve the name of the plugin.
     */
    String name();

    /**
     * Retrieve the plugin version.
     */
    String version() default "1.0.0";

    /**
     * Retrieve the plugin author.
     */
    String author() default "";

    /**
     * Retrieve the array of plugins this plugin depends on.
     */
    String[] dependencies() default {};
}

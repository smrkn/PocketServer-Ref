package org.pocketserver.api.plugin;

import org.pocketserver.api.Server;
import org.pocketserver.api.util.PocketLogging;

import com.google.common.base.Preconditions;

import org.slf4j.Logger;

public abstract class Plugin {

    private PocketPlugin pocketPlugin;
    private boolean initialised;
    private Logger logger;
    private Server server;
    private boolean enabled;

    public void onEnable() {

    }

    public void onDisable() {

    }

    public final Logger getLogger() {
        return this.logger;
    }

    public final String getName() {
        return this.getPocketPlugin().name();
    }

    public PocketPlugin getPocketPlugin() {
        return pocketPlugin;
    }

    public final boolean isEnabled() {
        return this.enabled;
    }

    protected final void setEnabled(boolean enabled) {
        if (this.enabled == enabled) {
            String what = enabled ? "enable" : "disable";
            logger.debug("Attempted to {} but the plugin is already {}d", what, what);
            return;
        }

        this.enabled = enabled;
        if (this.enabled) {
            logger.info(PocketLogging.Plugin.INIT, "Enabling {} v{} by {}", this.pocketPlugin.name(),
                this.pocketPlugin.version(), this.pocketPlugin.author());
            benchmarkMethod("onEnable", this::onEnable);
        } else {
            logger.info(PocketLogging.Plugin.INIT, "Disabling {}", this.pocketPlugin.name());
            benchmarkMethod("onDisable", this::onDisable);
            getServer().getPluginManager().unregisterListeners(this);
        }
    }

    private void benchmarkMethod(String methodName, Runnable action) {
        long start = System.currentTimeMillis();
        try {
            action.run();
        } finally {
            logger.trace(PocketLogging.Plugin.BENCHMARK, "Executing {} took {} ms", methodName, System
                .currentTimeMillis()
                - start);
        }
    }

    public final Server getServer() {
        return server;
    }

    final void init(Logger logger, Server server, PocketPlugin pocketPlugin) {
        Preconditions.checkArgument(!initialised, "plugin has already been initialised!");
        this.pocketPlugin = pocketPlugin;
        this.logger = logger;
        this.server = server;

        this.initialised = true;
    }
}

package org.pocketserver.api.metadata;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.HashMap;
import java.util.function.Supplier;

public final class Metadatables {

    // TODO: Test thread safety
    public static Metadatable synchronizedMetadatable(Metadatable metadatable) {
        Preconditions.checkNotNull(metadatable, "delegate should not be null");
        return new SynchronizedMetadatable(metadatable);
    }

    public static Metadatable immutableMetadatable(Metadatable metadatable) {
        Preconditions.checkNotNull(metadatable, "metadatable should not be null");
        return new Metadatable() {
            private final Metadatable delegate = new CopiedMetadatable(metadatable);

            @Override
            public <T> boolean has(MetadataKey<T> key) {
                return delegate.has(key);
            }

            @Override
            public <T> T unset(MetadataKey<T> key) {
                throw new UnsupportedOperationException("operation not supported");
            }

            @Override
            public <T> void set(MetadataKey<T> key, T value) {
                throw new UnsupportedOperationException("operation not supported");
            }

            @Override
            public <T> T get(MetadataKey<T> key, Supplier<? extends T> initialiser) {
                if (initialiser == null) {
                    return delegate.get(key, null);
                }
                throw new UnsupportedOperationException("operation not supported");
            }

            @Override
            public Collection<MetadataKey<?>> getKeys() {
                return ImmutableSet.copyOf(delegate.getKeys());
            }
        };
    }

    public static Metadatable copy(Metadatable metadatable) {
        return new CopiedMetadatable(metadatable);
    }

    public static Metadatable create() {
        return new DefaultMetadatable();
    }

    private Metadatables() {
        throw new UnsupportedOperationException("Metadatables cannot be instantiated!");
    }

    private static class CopiedMetadatable extends AbstractMetadatable {

        private CopiedMetadatable(Metadatable metadatable) {
            for (MetadataKey key : metadatable.getKeys()) {
                set(key, metadatable.get(key));
            }
        }
    }

    private static class DefaultMetadatable extends AbstractMetadatable {

        private DefaultMetadatable() {
            super(new HashMap<>());
        }
    }

    private static class SynchronizedMetadatable implements Metadatable {

        private final Metadatable delegate;

        private SynchronizedMetadatable(Metadatable delegate) {
            this.delegate = delegate;
        }

        @Override
        public synchronized <T> boolean has(MetadataKey<T> key) {
            return delegate.has(key);
        }

        @Override
        public synchronized <T> T unset(MetadataKey<T> key) {
            return delegate.unset(key);
        }

        @Override
        public synchronized <T> void set(MetadataKey<T> key, T value) {
            delegate.set(key, value);
        }

        @Override
        public synchronized Collection<MetadataKey<?>> getKeys() {
            return delegate.getKeys();
        }

        @Override
        public synchronized <T> T get(MetadataKey<T> key, Supplier<? extends T> initialiser) {
            return delegate.get(key, initialiser);
        }
    }
}

package org.pocketserver.api.metadata;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

public abstract class AbstractMetadatable implements Metadatable {

    final Map<MetadataKey<?>, Object> metadataMap;

    protected AbstractMetadatable() {
        this(Maps.newHashMap());
    }

    protected AbstractMetadatable(Map<MetadataKey<?>, Object> metadataMap) {
        this.metadataMap = Preconditions.checkNotNull(metadataMap, "backing map should not be null");
    }

    @Override
    public final <T> boolean has(MetadataKey<T> key) {
        return metadataMap.containsKey(key);
    }

    @Override
    public <T> T unset(MetadataKey<T> key) {
        Preconditions.checkNotNull(key, "key should not be null");
        return key.clazz.cast(metadataMap.remove(key));
    }

    @Override
    public final <T> void set(MetadataKey<T> key, T value) {
        Preconditions.checkNotNull(key, "key should not be null");
        metadataMap.put(key, value);
    }

    @Override
    public final <T> T get(MetadataKey<T> key, Supplier<? extends T> initialiser) {
        Preconditions.checkNotNull(key, "key should not be null");
        return key.clazz.cast(metadataMap.computeIfAbsent(key, k -> {
            if (initialiser == null) {
                return null;
            }
            return initialiser.get();
        }));
    }

    @Override
    public Collection<MetadataKey<?>> getKeys() {
        return metadataMap.keySet();
    }

    @Override
    public int hashCode() {
        return metadataMap.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !Metadatable.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        AbstractMetadatable that = (AbstractMetadatable) obj;
        return metadataMap.equals(that.metadataMap);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append(" {").append(System.lineSeparator());
        for (Map.Entry<MetadataKey<?>, Object> entry : metadataMap.entrySet()) {
            builder.append("\t")
                .append(entry.getKey())
                .append(" -> ")
                .append(entry.getValue())
                .append(System.lineSeparator());
        }
        return builder.append("}").toString();
    }
}

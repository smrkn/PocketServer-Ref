package org.pocketserver.api.metadata;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public final class MetadataKey<T> {

    public static <T> MetadataKey<T> valueOf(String name, Class<? extends T> clazz) {
        Preconditions.checkNotNull(clazz, "clazz should not be null");
        Preconditions.checkNotNull(name, "name should not be null");
        return new MetadataKey<>(name, clazz);
    }

    final Class<? extends T> clazz;
    final String name;

    MetadataKey(String name, Class<? extends T> clazz) {
        this.clazz = clazz;
        this.name = name;
    }

    public Class<? extends T> getType() {
        return clazz;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(clazz, name);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }
        MetadataKey<?> that = (MetadataKey<?>) o;
        return Objects.equal(clazz, that.clazz) && Objects.equal(name, that.name);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(MetadataKey.class)
            .add("class", clazz.getSimpleName())
            .add("name", name)
            .toString();
    }
}

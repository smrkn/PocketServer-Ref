package org.pocketserver.api.metadata;

import java.util.Collection;
import java.util.function.Supplier;

public interface Metadatable {

    <T> boolean has(MetadataKey<T> key);

    <T> T unset(MetadataKey<T> key);

    <T> void set(MetadataKey<T> key, T value);

    default <T> T get(MetadataKey<T> key) {
        return get(key, null);
    }

    <T> T get(MetadataKey<T> key, Supplier<? extends T> initialiser);

    Collection<MetadataKey<?>> getKeys();
}

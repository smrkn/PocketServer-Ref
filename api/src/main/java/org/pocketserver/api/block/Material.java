package org.pocketserver.api.block;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.function.Predicate;

public enum Material {
    AIR(0),
    STONE(1),
    GRASS(2),
    DIRT(3),
    COBBLESTONE(4),
    WOOD(5),
    SAPLING(6),
    BEDROCK(7),
    WATER(8),
    STATIONARY_WATER(9),
    LAVA(10),
    STATIONARY_LAVA(11),
    SAND(12),
    GRAVEL(13),
    GOLD_ORE(14),
    IRON_ORE(15),
    COAL_ORE(16),
    WOOD_LOG(17),
    LEAVES(18),
    SPONGE(19),
    GLASS(20),
    LAPIS_LAZULI_ORE(21),
    LAPIS_LAZULI_BLOCK(22),
    SANDSTONE(24),
    NOTE_BLOCK(25),
    BED_BLOCK(26),
    POWERED_RAIL(27),
    DETECTOR_RAIL(28),
    COBWEB(30),
    TALL_GRASS(31),
    DEAD_BUSH(32),
    WOOL(35),
    DANDELION(37),
    FLOWER(38),
    BROWN_MUSHROOM(39),
    RED_MUSHROOM(40),
    GOLD_BLOCK(41),
    IRON_BLOCK(42),
    STONE_DOUBLE_SLAB(43),
    STONE_SLAB(44),
    BRICKS(45),
    TNT(46),
    BOOKSHELF(47),
    MOSS_STONE(48),
    OBSIDIAN(49),
    TORCH(50),
    FIRE(51),
    SPAWNER(52),
    WOOD_STAIRS(53),
    CHEST(54),
    REDSTONE_WIRE(55),
    DIAMOND_ORE(56),
    DIAMOND_BLOCK(57),
    CRAFTING_TABLE(58),
    CROPS(59),
    FARMLAND(60),
    FURNACE(61),
    BURNING_FURNACE(62),
    SIGN_BLOCK(63),
    WOOD_DOOR_BLOCK(64),
    LADDER(65),
    RAIL(66),
    COBBLESTONE_STAIRS(67),
    WALL_SIGN(68),
    LEVER(69),
    STONE_PRESSURE_PLATE(70),
    IRON_DOOR_BLOCK(71),
    WOOD_PRESSURE_PLATE(72),
    REDSTONE_ORE(73),
    GLOWING_REDSTONE_ORE(74),
    REDSTONE_TORCH_OFF(75),
    REDSTONE_TORCH_ON(76),
    STONE_BUTTON(77),
    SNOW(78),
    ICE(79),
    SNOW_BLOCK(80),
    CACTUS(81),
    CLAY_BLOCK(82),
    SUGAR_CANE_BLOCK(83),
    FENCE(85),
    PUMPKIN(86),
    NETHERRACK(87),
    SOUL_SAND(88),
    GLOWSTONE(89),
    PORTAL(90),
    JACK_O_LANTERN(91),
    CAKE_BLOCK(92),
    INVISIBLE_BEDROCK(95),
    WOOD_TRAPDOOR(96),
    MONSTER_EGG(97),
    STONE_BRICK(98),
    BROWN_MUSHROOM_BLOCK(99),
    RED_MUSHROOM_BLOCK(100),
    IRON_BARS(101),
    GLASS_PANE(102),
    MELON_BLOCK(103),
    PUMPKIN_STEM(104),
    MELON_STEM(105),
    VINES(106),
    FENCE_GATE(107),
    BRICK_STAIRS(108),
    STONE_BRICK_STAIRS(109),
    MYCELIUM(110),
    LILY_PAD(111),
    NETHER_BRICKS(112),
    NETHER_BRICK_FENCE(113),
    NETHER_BRICK_STAIRS(114),
    NETHER_WART_BLOCK(115),
    ENCHANTMENT_TABLE(116),
    BREWING_STAND_BLOCK(117),
    END_PORTAL_FRAME(120),
    END_STONE(121),
    REDSTONE_LAMP_OFF(122),
    REDSTONE_LAMP_ON(123),
    ACTIVATOR_RAIL(126),
    COCOA_BEANS_BLOCK(127),
    SANDSTONE_STAIRS(128),
    EMERALD_ORE(129),
    TRIPWIRE_HOOK(131),
    TRIPWIRE(132),
    EMERALD_BLOCK(133),
    SPRUCE_WOOD_STAIRS(134),
    BIRCH_WOOD_STAIRS(135),
    JUNGLE_WOOD_STAIRS(136),
    COBBLESTONE_WALL(139),
    FLOWER_POT_BLOCK(140),
    CARROTS_BLOCK(141),
    POTATOES_BLOCK(142),
    WOOD_BUTTON(143),
    HEAD_BLOCK(144),
    ANVIL(145),
    TRAPPED_CHEST(146),
    GOLD_PRESSURE_PLATE(147),
    IRON_PRESSURE_PLATE(148),
    DAYLIGHT_SENSOR(151),
    REDSTONE_BLOCK(152),
    QUARTZ_ORE(153),
    QUARTZ_BLOCK(155),
    QUARTZ_STAIRS(156),
    WOOD_DOUBLE_SLAB(157),
    WOOD_SLAB(158),
    STAINED_CLAY(159),
    ACACIA_LEAVES(161),
    ACACIA_WOOD_LOG(162),
    ACACIA_STAIRS(163),
    DARK_OAK_STAIRS(164),
    IRON_TRAPDOOR(167),
    HAY_BALE(170),
    CARPET(171),
    HARDENED_CLAY(172),
    COAL_BLOCK(173),
    PACKED_ICE(174),
    SUNFLOWER(175),
    INVERTED_DAYLIGHT_SENSOR(178),
    SPRUCE_FENCE_GATE(183),
    BIRCH_FENCE_GATE(184),
    JUNGLE_FENCE_GATE(185),
    DARK_OAK_FENCE_GATE(186),
    ACACIA_FENCE_GATE(187),
    GRASS_PATH(198),
    PODZOL(243),
    BEETROOT_BLOCK(244),
    STONECUTTER(245),
    GLOWING_OBSIDIAN(246),
    NETHER_REACTOR_CORE(247),
    UPDATE_BLOCK_1(248),
    UPDATE_BLOCK_2(249),
    RESERVED(255),

    IRON_SHOVEL(256),
    IRON_PICKAXE(257),
    IRON_AXE(258),
    FLINT_AND_STEEL(259),
    APPLE(260),
    BOW(261),
    ARROW(262),
    COAL(263),
    DIAMOND(264),
    IRON(265),
    GOLD(266),
    IRON_SWORD(267),
    WOOD_SWORD(268),
    WOOD_SHOVEL(269),
    WOOD_PICKAXE(270),
    WOOD_AXE(271),
    STONE_SWORD(272),
    STONE_SHOVEL(273),
    STONE_PICKAXE(274),
    STONE_AXE(275),
    DIAMOND_SWORD(276),
    DIAMOND_SHOVEL(277),
    DIAMOND_PICKAXE(278),
    DIAMOND_AXE(279),
    STICK(280),
    BOWL(281),
    MUSHROOM_STEW(282),
    GOLD_SWORD(283),
    GOLD_SHOVEL(284),
    GOLD_PICKAXE(285),
    GOLD_AXE(286),
    STRING(287),
    FEATHER(288),
    GUNPOWDER(289),
    WOOD_HOE(290),
    STONE_HOE(291),
    IRON_HOE(292),
    DIAMOND_HOE(293),
    GOLD_HOE(294),
    SEEDS(295),
    WHEAT(296),
    BREAD(297),
    LEATHER_HELMET(298),
    LEATHER_CHESTPLATE(299),
    LEATHER_LEGGINGS(300),
    LEATHER_BOOTS(301),
    CHAIN_HELMET(302),
    CHAIN_CHESTPLATE(303),
    CHAIN_LEGGINGS(304),
    CHAIN_BOOTS(305),
    IRON_HELMET(306),
    IRON_CHESTPLATE(307),
    IRON_LEGGINGS(308),
    IRON_BOOTS(309),
    DIAMOND_HELMET(310),
    DIAMOND_CHESTPLATE(311),
    DIAMOND_LEGGINGS(312),
    DIAMOND_BOOTS(313),
    GOLD_HELMET(314),
    GOLD_CHESTPLATE(315),
    GOLD_LEGGINGS(316),
    GOLD_BOOTS(317),
    FLINT(318),
    RAW_PORKCHOP(319),
    COOKED_PORKCHOP(320),
    PAINTING(321),
    GOLD_APPLE(322),
    SIGN(323),
    WOOD_DOOR(324),
    BUCKET(325),
    MINECART(328),
    SADDLE(329),
    IRON_DOOR(330),
    REDSTONE(331),
    SNOWBALL(332),
    BOAT(333),
    LEATHER(334),
    BRICK(336),
    CLAY(337),
    SUGAR_CANE(338),
    PAPER(339),
    BOOK(340),
    SLIMEBALL(341),
    EGG(344),
    COMPASS(345),
    FISHING_ROD(346),
    CLOCK(347),
    GLOWSTONE_DUST(348),
    RAW_FISH(349),
    COOKED_FISH(350),
    DYE(351),
    BONE(352),
    SUGAR(353),
    CAKE(354),
    BED(355),
    COOKIE(357),
    SHEARS(359),
    MELON(360),
    PUMPKIN_SEEDS(361),
    MELON_SEEDS(362),
    RAW_BEEF(363),
    STEAK(364),
    RAW_CHICKEN(365),
    COOKED_CHICKEN(366),
    ROTTEN_FLESH(367),
    BLAZE_ROD(369),
    GHAST_TEAR(370),
    GOLD_NUGGET(371),
    NETHER_WART(372),
    POTION(373),
    GLASS_BOTTLE(374),
    SPIDER_EYE(375),
    FERMENTED_SPIDER_EYE(376),
    BLAZE_POWDER(377),
    MAGMA_CREAM(378),
    BREWING_STAND(379),
    GLISTERING_MELON(382),
    SPAWN_EGG(383),
    BOTTLE_O_ENCHANTING(384),
    EMERALD(388),
    FLOWER_POT(390),
    CARROT(391),
    POTATO(392),
    BAKED_POTATO(393),
    POISONOUS_POTATO(394),
    GOLDEN_CARROT(396),
    HEAD(397),
    PUMPKIN_PIE(400),
    ENCHANTED_BOOK(403),
    NETHER_BRICK(405),
    NETHER_QUARTZ(406),
    RAW_RABBIT(411),
    COOKED_RABBIT(412),
    RABBIT_STEW(413),
    RABBIT_FOOT(414),
    RABBIT_HIDE(415),
    SPLASH_POTION(438),
    BEETROOT(457),
    BEETROOT_SEEDS(458),
    BEETROOT_SOUP(459);

    private static final Map<Integer, Material> ID_MAP = Collections.unmodifiableMap(new HashMap<Integer, Material>() {{
        Arrays.asList(Material.values()).forEach(material -> put(material.getId(), material));
    }});

    public static Optional<Material> getMaterial(int id) {
        return Optional.ofNullable(ID_MAP.get(id));
    }

    public int getId() {
        return id;
    }

    private final int id;

    Material(int id) {
        this.id = id;
    }

    public boolean isBlock() {
        return !isItem();
    }

    public boolean isItem() {
        return this.id >= 256;
    }

    public byte getByte() {
        return (byte) getId();
    }

    public interface Armor {

        Collection<Material> HELMETS = Sets.immutableEnumSet(
            LEATHER_HELMET,
            CHAIN_HELMET,
            IRON_HELMET,
            GOLD_HELMET,
            DIAMOND_HELMET
        );

        Collection<Material> CHESTPLATES = Sets.immutableEnumSet(
            LEATHER_CHESTPLATE,
            CHAIN_CHESTPLATE,
            IRON_CHESTPLATE,
            GOLD_CHESTPLATE,
            DIAMOND_CHESTPLATE
        );

        Collection<Material> LEGGINGS = Sets.immutableEnumSet(
            LEATHER_LEGGINGS,
            CHAIN_LEGGINGS,
            IRON_LEGGINGS,
            GOLD_LEGGINGS,
            DIAMOND_LEGGINGS
        );

        Collection<Material> BOOTS = Sets.immutableEnumSet(
            LEATHER_BOOTS,
            CHAIN_BOOTS,
            IRON_BOOTS,
            GOLD_BOOTS,
            DIAMOND_BOOTS
        );

        Collection<Material> ALL_ARMOR = Sets.immutableEnumSet(Iterables.concat(HELMETS, CHESTPLATES,
            LEGGINGS, BOOTS));

        static Predicate<Material> isArmor() {
            return ALL_ARMOR::contains;
        }
    }

    public interface Tools {

        Collection<Material> AXES = Sets.immutableEnumSet(
            WOOD_AXE,
            STONE_AXE,
            IRON_AXE,
            GOLD_AXE,
            DIAMOND_AXE
        );

        Collection<Material> SWORDS = Sets.immutableEnumSet(
            WOOD_SWORD,
            STONE_SWORD,
            IRON_SWORD,
            GOLD_SWORD,
            DIAMOND_SWORD
        );

        Collection<Material> HOES = Sets.immutableEnumSet(
            WOOD_HOE,
            STONE_HOE,
            IRON_HOE,
            GOLD_HOE,
            DIAMOND_HOE
        );

        Collection<Material> SHOVELS = Sets.immutableEnumSet(
            WOOD_SHOVEL,
            STONE_SHOVEL,
            IRON_SHOVEL,
            GOLD_SHOVEL,
            DIAMOND_SHOVEL
        );

        Collection<Material> PICKAXES = Sets.immutableEnumSet(
            WOOD_PICKAXE,
            STONE_PICKAXE,
            IRON_PICKAXE,
            GOLD_PICKAXE,
            DIAMOND_PICKAXE
        );

        Collection<Material> YIELDING = Sets.immutableEnumSet(new HashSet<Material>() {{
            addAll(AXES);
            addAll(SHOVELS);
            addAll(PICKAXES);

            add(SHEARS);
        }});

        Collection<Material> WEAPONS = Sets.immutableEnumSet(new HashSet<Material>() {{
            addAll(AXES);
            addAll(SWORDS);

            add(BOW);
        }});

        Collection<Material> ALL_TOOLS = Sets.immutableEnumSet(new HashSet<Material>() {{
            addAll(AXES);
            addAll(HOES);
            addAll(SWORDS);
            addAll(SHOVELS);
            addAll(PICKAXES);

            add(BOW);
            add(SHEARS);
            add(FISHING_ROD);
            add(FLINT_AND_STEEL);
        }});

        static Predicate<Material> isTool() {
            return ALL_TOOLS::contains;
        }

        static Predicate<Material> isWeapon() {
            return WEAPONS::contains;
        }
    }
}
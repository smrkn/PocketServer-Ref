package org.pocketserver.api.plugin;

import org.pocketserver.api.Server;
import org.pocketserver.api.TestLogger;

import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;

public class MockPlugin extends Plugin {

    public MockPlugin(Server server) {
        TestLogger.init();
        PocketPlugin pocketPlugin = new PocketPlugin() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return PocketPlugin.class;
            }

            @Override
            public String name() {
                return "MockPlugin";
            }

            @Override
            public String version() {
                return "1.0";
            }

            @Override
            public String author() {
                return "PocketServer Team";
            }

            @Override
            public String[] dependencies() {
                return new String[0];
            }
        };
        init(LoggerFactory.getLogger(MockPlugin.class), server, pocketPlugin);
    }
}

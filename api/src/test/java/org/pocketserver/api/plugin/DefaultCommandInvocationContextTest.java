package org.pocketserver.api.plugin;

import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DefaultCommandInvocationContextTest {

    @Test
    public void testGetArgument() throws Exception {
        class Person {

            String name;

            Person(String name) {
                this.name = name;
            }
        }

        Command.registerMapping(Person.class, (ctx, s) -> new Person(s));
        String[] args = new String[]{
            "Bob"
        };
        CommandInvocationContext ctx = new PluginManager.DefaultCommandInvocationContext(null, null,
            args);
        assertEquals(args[0], ctx.getArgument(Person.class).name);
    }
}
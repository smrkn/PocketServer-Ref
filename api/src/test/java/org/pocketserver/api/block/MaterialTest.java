package org.pocketserver.api.block;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MaterialTest {

    @Test
    public void testMaterialLookup() {
        for (Material material : Material.values()) {
            Optional<Material> optional = Material.getMaterial(material.getId());
            if (!optional.isPresent()) {
                fail();
            }
            assertEquals(material, optional.get());
        }
    }
}

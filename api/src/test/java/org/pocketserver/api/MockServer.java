package org.pocketserver.api;

import org.pocketserver.api.command.ConsoleCommandExecutor;
import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.player.Player;
import org.pocketserver.api.plugin.PluginManager;
import org.pocketserver.api.util.Pipeline;

import com.google.common.collect.ImmutableList;

import org.pocketserver.api.world.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class MockServer extends Server {

    private final Logger logger;

    public MockServer() {
        TestLogger.init();
        this.logger = LoggerFactory.getLogger(MockServer.class);
    }

    @Override
    public void shutdown() {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public PluginManager getPluginManager() {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public File getDirectory() {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public Pipeline<PermissionResolver> getPermissionPipeline() {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public Collection<Player> getPlayer(String username) {
        return Collections.emptyList();
    }

    @Override
    public List<? extends Player> getPlayers(Predicate<Player> filter) {
        return Collections.emptyList();
    }

    @Override
    public Collection<Player> getOnlinePlayers() {
        return ImmutableList.of();
    }

    @Override
    public ConsoleCommandExecutor getConsole() {
        return null;
    }

    @Override
    public void broadcastMessage(String message) {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public void broadcastMessage(String message, Predicate<Player> filter) {
        throw new UnsupportedOperationException("not supported by mocked servers");
    }

    @Override
    public World getDefaultWorld() {
        return null;
    }
}

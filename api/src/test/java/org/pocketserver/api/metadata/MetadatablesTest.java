package org.pocketserver.api.metadata;

import org.junit.Assume;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MetadatablesTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableMetadatable() throws Exception {
        String name = "JUnit";

        MetadataKey<String> nameKey = MetadataKey.valueOf("name", String.class);
        Metadatable metadatable = Metadatables.create();
        metadatable.set(nameKey, name);

        Assume.assumeTrue(name.equals(metadatable.get(nameKey)));

        metadatable = Metadatables.immutableMetadatable(metadatable);
        metadatable.unset(nameKey);
    }

    @Test
    public void testCopy() throws Exception {
        MetadataKey<String> nameKey = MetadataKey.valueOf("name", String.class);
        Metadatable one = Metadatables.create();
        one.set(nameKey, "JUnit");
        Metadatable two = Metadatables.copy(one);
        assertEquals("metadatables should be equal", one, two);
    }
}
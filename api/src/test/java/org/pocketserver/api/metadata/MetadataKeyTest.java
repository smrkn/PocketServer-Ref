package org.pocketserver.api.metadata;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author Connor Spencer Harries
 */
public class MetadataKeyTest {

    @Test
    public void testHashCode() throws Exception {
        MetadataKey one = MetadataKey.valueOf("foo", Float.class);
        MetadataKey two = MetadataKey.valueOf("foo", Short.class);
        assertNotEquals("hashCode should not return the same value", one.hashCode(), two.hashCode());
    }

    @Test
    public void testEquals() throws Exception {
        MetadataKey one = MetadataKey.valueOf("foo", Float.class);
        MetadataKey two = MetadataKey.valueOf("foo", Short.class);
        assertNotEquals("objects should not be considered equal", one, two);

        one = MetadataKey.valueOf("foo", Short.class);
        assertEquals(one, two);
    }
}
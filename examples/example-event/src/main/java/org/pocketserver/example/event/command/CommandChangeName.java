package org.pocketserver.example.event.command;

import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;
import org.pocketserver.example.event.event.ServerListener;

public class CommandChangeName extends Command {

    private final ServerListener listener;

    public CommandChangeName(ServerListener listener) {
        super("change-name");
        this.listener = listener;
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        if (ctx.size() > 0) {
            listener.setServerName(String.join(" ", ctx.getArguments()));
        } else {
            ctx.getExecutor().sendMessage("You must name the server!");
        }
    }
}

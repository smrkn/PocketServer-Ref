package org.pocketserver.example.event;

import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PocketPlugin;
import org.pocketserver.example.event.command.CommandChangeName;
import org.pocketserver.example.event.event.ServerListener;

@PocketPlugin(name = "EventExample", author = "PocketServer Team")
public final class EventExamplePlugin extends Plugin {

    @Override
    public void onEnable() {
        ServerListener listener = new ServerListener();
        getServer().getPluginManager().registerListener(this, listener);
        getServer().getPluginManager().registerCommand(this, new CommandChangeName(listener));
    }
}

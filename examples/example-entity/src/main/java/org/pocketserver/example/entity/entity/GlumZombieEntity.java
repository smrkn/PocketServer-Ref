package org.pocketserver.example.entity.entity;

import org.pocketserver.api.player.Player;
import org.pocketserver.api.world.locale.Location;
import org.pocketserver.entity.living.PocketLivingEntity;
import org.pocketserver.example.entity.EntityExamplePlugin;

public class GlumZombieEntity extends PocketLivingEntity {

    public GlumZombieEntity(Location location) {
        super(EntityExamplePlugin.GLUM_ZOMBIE_TYPE);
        setLocation(location);
        getNavigation().setTarget(() -> {
            Player player = getNearest(Player.class).orElse(null);
            return player != null ? player.getLocation() : null;
        });
    }
}

package org.pocketserver.example.entity;

import org.pocketserver.api.entity.EntityType;
import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PocketPlugin;
import org.pocketserver.example.entity.entity.GlumZombieEntity;

@PocketPlugin(name = "EntityExample", author = "PocketServer Team")
public class EntityExamplePlugin extends Plugin {

    public static final EntityType GLUM_ZOMBIE_TYPE;

    static {
        GLUM_ZOMBIE_TYPE = new EntityType("Glum Zombie", EntityType.ZOMBIE.getId());
        GLUM_ZOMBIE_TYPE.setImplementation(GlumZombieEntity.class, (e, l) -> new GlumZombieEntity(l));
        GLUM_ZOMBIE_TYPE.register();
    }

    @Override
    public void onEnable() {

    }

}

package org.pocketserver.example.permissions.command;

import org.pocketserver.api.ChatColor;
import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;
import org.pocketserver.api.player.Player;

public class CommandTestPermission extends Command {

    public CommandTestPermission() {
        super("test-permission");
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        if (ctx.size() < 2) {
            ctx.getExecutor().sendMessage("Usage: /test-permission <username> <permission>");
        } else {
            Player player = ctx.getArgument(Player.class);
            if (player != null) {
                String permission = ctx.getArgument();
                if (player.hasPermission(permission)) {
                    ctx.getExecutor()
                        .sendMessage(ChatColor.GREEN + "{0} has {1}", player.getName(), permission);
                } else {
                    ctx.getExecutor()
                        .sendMessage(ChatColor.RED + "{0} does not have {1}", player.getName(), permission);
                }
            } else {
                ctx.getExecutor().sendMessage(ChatColor.RED + "Sorry, you can only test online players!");
            }
        }
    }
}

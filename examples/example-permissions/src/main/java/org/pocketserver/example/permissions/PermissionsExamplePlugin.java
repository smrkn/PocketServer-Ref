package org.pocketserver.example.permissions;

import org.pocketserver.api.permissions.PermissionResolver;
import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PocketPlugin;
import org.pocketserver.example.permissions.command.CommandTestPermission;
import org.pocketserver.example.permissions.storage.PermissionLoader;
import org.pocketserver.example.permissions.storage.impl.FilePermissionLoader;

@PocketPlugin(name = "PermissionExample", author = "PocketServer Team")
public final class PermissionsExamplePlugin extends Plugin {

    private PermissionLoader permissionLoader;

    @Override
    public void onEnable() {
        permissionLoader = new FilePermissionLoader(this);
        getServer().getPluginManager().registerCommand(this, new CommandTestPermission());
        getServer().getPermissionPipeline().addFirst((player, permission) -> {
            if (permissionLoader.get(player.getName()).contains(permission)) {
                return PermissionResolver.Result.ALLOW;
            }
            return PermissionResolver.Result.UNSET;
        });
    }
}

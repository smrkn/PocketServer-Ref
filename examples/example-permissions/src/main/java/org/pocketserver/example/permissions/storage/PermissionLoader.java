package org.pocketserver.example.permissions.storage;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Collection;

public interface PermissionLoader {

    Marker PERMISSION_IO_MARKER = MarkerFactory.getMarker("PERMISSION_IO");

    Collection<String> get(String username);
}

package org.pocketserver.example.permissions.storage.impl;

import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.example.permissions.storage.PermissionLoader;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class FilePermissionLoader implements PermissionLoader {

    private final Map<String, Set<String>> cache;
    private final File storageDirectory;
    private final Logger logger;

    public FilePermissionLoader(Plugin plugin) {
        this.storageDirectory =
            new File(plugin.getServer().getDirectory(), "plugins/permission-example");
        this.cache = Maps.newHashMap();
        this.logger = plugin.getLogger();
        if (storageDirectory.mkdirs()) {
            plugin.getLogger().info(PermissionLoader.PERMISSION_IO_MARKER, "Created \"{}\" directory",
                storageDirectory.toPath());
        }
    }

    @Override
    public Collection<String> get(String username) {
        Preconditions.checkNotNull(username, "uniqueId should not be null!");
        Preconditions.checkArgument(username.length() > 0, "username should not be empty!");

        return cache.computeIfAbsent(username, n -> {
            Set<String> permissions = Sets.newLinkedHashSet();
            File userFile = new File(storageDirectory, username.concat(".permissions"));
            try {
                if (userFile.createNewFile()) {
                    logger.info(PermissionLoader.PERMISSION_IO_MARKER, "Created \"{}\"", userFile.toPath());
                } else {
                    permissions.addAll(Files.readAllLines(userFile.toPath(), Charsets.UTF_8));
                }
            } catch (IOException cause) {
                logger.error(PermissionLoader.PERMISSION_IO_MARKER,
                    "An unknown error occured during method execution", cause);
            }
            return permissions;
        });
    }
}

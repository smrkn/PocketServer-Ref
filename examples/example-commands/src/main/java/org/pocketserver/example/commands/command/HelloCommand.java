package org.pocketserver.example.commands.command;

import org.pocketserver.api.command.Command;
import org.pocketserver.api.command.CommandInvocationContext;

public class HelloCommand extends Command {

    public HelloCommand() {
        super("hello");
    }

    @Override
    public void execute(CommandInvocationContext ctx) {
        ctx.getExecutor().sendMessage("Hello {0}!", ctx.getExecutor().getName());
    }
}

package org.pocketserver.example.commands;

import org.pocketserver.api.plugin.Plugin;
import org.pocketserver.api.plugin.PocketPlugin;
import org.pocketserver.example.commands.command.HelloCommand;

@PocketPlugin(name = "CommandExample", author = "PocketServer Team")
public class CommandExamplePlugin extends Plugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerCommand(this, new HelloCommand());
    }
}
